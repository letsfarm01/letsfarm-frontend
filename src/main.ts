import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import 'jquery';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
  console.log = function() {}
}

/*$(window).on('load', function() {
  $('#page-loader').fadeOut('fast', function() {
    $(this).remove();
  });
});*/

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
