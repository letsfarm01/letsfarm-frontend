import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import {Upload} from '../../models/upload';
import {NotificationService} from "../notificationServices/notification.service";

@Injectable({
  providedIn: 'root'
})
export class UploadService {
  private basePath = '/letsfarm_images';
  constructor(private alertService: NotificationService) { }
  public uploadB64 (upload, dataObject: object, callback: any, key: string) {
    const storageRef = firebase.storage().ref();
    const itemForUpload = upload;
    const name = 'image' + Math.random() * 100;
    const uploadBase = itemForUpload.split(';base64,')[1];
    const uploadTask = storageRef.child(`${this.basePath}/${name}`).putString(uploadBase, 'base64');
    // split out the image/png or jpg out of string
    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot) =>  {
        // upload in progress
        // upload.progress = (uploadTask.snapshot.bytesTransferred / uploadTask.snapshot.totalBytes) * 100;
      },
      (error) => {
        // upload failed
        console.log(error);
      },
      () => {
        // upload success
        uploadTask.snapshot.ref.getDownloadURL().then(function (downloadURL) {
          console.log('File available at', downloadURL);
          // upload success
          // upload.url = uploadTask.snapshot.downloadURL;
          dataObject[key] = downloadURL;
          // upload.url = downloadURL;
          callback(dataObject);
          // upload.name = upload.file.name;
        });
      }
    );
  }
  public uploadBlob (upload: Upload, dataObject: object, callback: any, key: string) {
    const storageRef = firebase.storage().ref();
    const uploadTask = storageRef.child(`${this.basePath}/${upload.file.name}`).put(upload.file);
    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot) =>  {
        // upload in progress
        upload.progress = (uploadTask.snapshot.bytesTransferred / uploadTask.snapshot.totalBytes) * 100;
      },
      (error) => {
        // upload failed
        console.log(error);
      },
      () => {
        // upload success
        uploadTask.snapshot.ref.getDownloadURL().then(function (downloadURL) {
          console.log('File available at', downloadURL);
          // upload success
          // upload.url = uploadTask.snapshot.downloadURL;
          dataObject[key] = downloadURL;
          upload.url = downloadURL;
          callback(dataObject);
          // upload.name = upload.file.name;
        });
      }
    );
  }
  public uploadMultiple(upload: Upload, path) {
    return new Promise( (resolve, reject) => {
      const storageRef = firebase.storage().ref();
      const uploadTask = storageRef.child(`${this.basePath}/${path}/${upload.file.name}`).put(upload.file);
      uploadTask.on('state_changed',
        function progress(snapshot) {        },
        function error(err) {
          reject({});
        },
        function complete() {
          uploadTask.snapshot.ref.getDownloadURL().then(function (downloadURL) {
            console.log('File available at', downloadURL);
            upload.url = downloadURL;
            resolve({name: upload.file.name, url: upload.url});
          });
        }
      );
    });
  }
  uploadOnEditor(upload, uploadPath, callback) {
    const storageRef = firebase.storage().ref();
    const itemForUpload = upload;
    console.log('File : ', upload);
    const uploadTask = storageRef.child(`${this.basePath}/${uploadPath}/${upload.name}`).put(itemForUpload);
    // split out the image/png or jpg out of string
    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot) => {
        // upload in progress
        // upload.progress = (uploadTask.snapshot.bytesTransferred / uploadTask.snapshot.totalBytes) * 100;
      },
      (error) => {
        // upload failed
        this.alertService.error('Unable to upload to file, check internet connection!')
        console.log(error);
      },
      () => {
        uploadTask.snapshot.ref.getDownloadURL().then(function (downloadURL) {
          console.log("File available at", downloadURL);
          // upload success
          // upload.url = uploadTask.snapshot.downloadURL;
          upload.url = downloadURL;
          // upload.name = upload.file.name;
          console.log('Upload', upload);
          callback(upload.url);
        });

      }
    );
  }

}
