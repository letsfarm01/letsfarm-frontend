import { Injectable } from '@angular/core';
import * as JWT_DECODE from 'jwt-decode';
import {CacheService} from '../cacheService/cache.service';
import {environment as ENV} from '../../../environments/environment';
import {IResponse} from '../../interfaces/iresponse';
import swal from 'sweetalert2';
import {EventsService} from '../eventServices/event.service';
import {ScrollToConfigOptions, ScrollToService} from '@nicky-lenaers/ngx-scroll-to';
import {BootstrapNotifyService} from '../bootstrap-notify/bootstrap-notify.service';
@Injectable({
  providedIn: 'root'
})
export class UtilService {
  dataTableInstance: any;
  Roles: any[] = [];
  // public CASHIER_ROUTE: any[] = [];
  constructor(private cacheService: CacheService, private eventService: EventsService,
              private alertService: BootstrapNotifyService,
              private _scrollToService: ScrollToService) {  }

  public getAuthUser() {
    return JSON.parse(sessionStorage.getItem(ENV.USERTOKEN));
  }
  public setAuthUser(user) {
    sessionStorage.setItem(ENV.USERTOKEN, user);
  }
  public getAuthToken() {
    const token = this.cacheService.getSession(ENV.TOKEN);
    const decodedToken = JWT_DECODE(token);
    return decodedToken['data']['public'];
  }
  public setRoles() {
    this.Roles = [];
    const user = this.getAuthUser();
    if (user && user.roles) {
      user.roles.forEach((role) => {
        this.Roles.push(role.name);
      });
      return this.Roles;
    } else {
      return this.Roles;
    }
  }
  public triggerScrollTo() {

    const config: ScrollToConfigOptions = {
      target: 'intro-hero'
    };

    this._scrollToService.scrollTo(config);
  }

  public processCommonJs2() {
    $('body').css('background-color', '#fafafa');
  }
  public openModal(id) {
    setTimeout(() => {
      (<any>$('#' + id)).modal({show: true, backdrop: 'static', keyboard: false});
    }, 20);
  }
  public closeModal(id) {
    (<any>$('#' + id)).modal('hide');
  }
  startDatatable(id) {
    setTimeout(() => {
      this.initDataTable(id);
    }, 1000);
  }

  neutralDatatable(id, res, destroy='destroy') {
    setTimeout(() => {
      this.initDataTable(id, res, destroy);
    }, 1000);
  }

  public initDataTable(id, responsive = true, destroy = 'destroy') {
    console.log('Is Destroy ', destroy);
    if (this.dataTableInstance && destroy === 'destroy') {
      console.log('DESTROYER ', this.dataTableInstance);
      this.dataTableInstance.destroy();
    }
    const buttons = ['pdf', 'print', 'excel', 'csv', 'copy'];
    setTimeout(() => {
      this.dataTableInstance = ($('#' + id)as any).DataTable({
        pagingType: 'full_numbers',
        dom: 'Blfrtip',
        keys: !0,
        buttons: buttons,
        // order: [[1, 'asc']],
        language: {
          search: '_INPUT_',
          searchPlaceholder: 'Search...',
          paginate: {
            previous: '<i class=\'fa fa-angle-left\'>',
            next: '<i class=\'fa fa-angle-right\'>'
          }
        },
        select: {
          // style: 'multi'
        },
        columnDefs: [ {
          targets: 'no-sort',
          orderable: false,
        },
          { responsivePriority: 1, targets: 0 },
          { responsivePriority: 2, targets: -1 }
          ],
        'lengthMenu': [
          [50, 100, 150, -1],
          [50, 100, 150, 'All']
        ],
        responsive: responsive,
      });
      $('.dt-buttons .btn').removeClass('btn-secondary').addClass('btn-sm btn-primary');
      // Add event listener for opening and closing details
      $(`#${id} tbody`).on('click', 'td.details-control', (e) => {
        const tr = (<any>$(this)).closest('tr');
        const row = this.dataTableInstance.row( tr );
        const target = $(`#${e.target.id}`);
        if ( row.child.isShown() ) {
          this.handleIconSwitch(target);
          tr.removeClass('shown');
          $('.dtr-details').addClass('table-bordered table-hover table-striped');

        } else {
          this.handleIconSwitch(target);
          tr.addClass('shown');
          $('.dtr-details').addClass('table-bordered table-hover table-striped');

        }
      });
    }, 400);

  }
  public handleIconSwitch(target) {
    if (target.hasClass('isShown')) {
      target.removeClass('isShown');
      target.addClass('isNotShown');
    } else {
      target.addClass('isShown');
      target.removeClass('isNotShown');
    }
  }
  public confirmAction(callback) {
    const swalWithBootstrapButtons = swal.mixin({
      confirmButtonClass: 'btn btn-success mx-1',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
    });
    swalWithBootstrapButtons({
      title: 'Are you sure?',
      text: `You won't be able to revert action! `,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        callback();
      } else if (
        // Read more about handling dismissals
        result.dismiss === swal.DismissReason.cancel
      ) {
        console.log('Action not completed!');
      }
    });
  }
  public randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
  }
  public actionRequireConfirmed(callback) {
    const swalWithBootstrapButtons = swal.mixin({
      confirmButtonClass: 'btn btn-success mx-1',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
    });
    swalWithBootstrapButtons({
      title: 'Are you sure?',
      text: `This action is not reversible`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, proceed!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        callback();
      } else if (
        // Read more about handling dismissals
        result.dismiss === swal.DismissReason.cancel
      ) {
        console.log('Action not completed!');
      }
    });
  }
  keepInCache(product, qty = 1) {
    const productsInCart = JSON.parse(this.cacheService.getStorage(ENV.PRODUCTS_TO_CART)) || [];
    console.log('ITEM ALREADY IN CART ', productsInCart);
    const _product = {
      quantity: qty,
      product_name: product.product_name,
      display_img: product.display_img,
      price: product.price,
      _id: product._id,
      total_price: product.price
    };
    let found = false;
    // let done = false;
    let itemInCart = {item: null, index: null} ;
    for (let i = 0; i < productsInCart.length; i++) {
      const selectedProduct = productsInCart[i];
      console.log('Update Cache Loop ', i, selectedProduct._id, product._id, selectedProduct._id === product._id);
      if (selectedProduct._id === product._id) {
        itemInCart = {item: selectedProduct, index: i};
        found = true;
        break;
      } else {
        found = false;
      }
    }
    if (found) {
      console.log('Update Cache 2');
      productsInCart[itemInCart.index].quantity += qty;
      const newQty = productsInCart[itemInCart.index].quantity;
      productsInCart[itemInCart.index].total_price = parseFloat(itemInCart.item.price) * parseInt(newQty, 10);
      this.cacheService.setStorage(ENV.PRODUCTS_TO_CART, JSON.stringify(productsInCart));
    } else {
      console.log('Update Cache');
      productsInCart.push(_product);
      this.cacheService.setStorage(ENV.PRODUCTS_TO_CART, JSON.stringify(productsInCart));
    }
    this.alertService.success('Item added to cart successfully');
    this.eventService.broadcast('CART_MODIFIED');
    console.log('PRODUCTS ', product, productsInCart, found);
  }
  removeItemFromCache(product) {
    const productsInCart = JSON.parse(this.cacheService.getStorage(ENV.PRODUCTS_TO_CART)) || [];
    // let index = 0;
    for (let i = 0; i < productsInCart.length; i++) {
      const selectedProduct = productsInCart[i];
      if (selectedProduct._id === product._id) {
        productsInCart.splice(i, 1);
        break;
      }
    }

  }
  getItemsInCache() {
    const productsInCart = JSON.parse(this.cacheService.getStorage(ENV.PRODUCTS_TO_CART)) || [];
    console.log('PRODUCTS ', productsInCart);
    return productsInCart;
  }
  clearCartItemsInCache() {
    this.cacheService.deleteStorage(ENV.PRODUCTS_TO_CART);
    return true;
  }
  countItemsInCache() {
    const productsInCart = JSON.parse(this.cacheService.getStorage(ENV.PRODUCTS_TO_CART)) || [];
    console.log('PRODUCTS ', productsInCart);
    return productsInCart.length;
  }
}
