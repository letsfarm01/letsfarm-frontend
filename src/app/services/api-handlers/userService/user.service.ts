/**
 * Created by Arokoyu Olalekan Ojo
 */

import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {IResponse} from '../../../interfaces/iresponse';
import {ApiService} from '../../api/api.service.';
import {DecryptService} from '../../decryptService/decrypt.service';
@Injectable()
export class UserService {

  constructor(private api: ApiService,
              private decryptionService: DecryptService) {

  }


  auth(data): Observable<IResponse> {
    return this.api.postRequest('auth', 'login' , data).map((res: IResponse)  => {
      return res ;
    });
  }
  socialLogin(data): Observable<IResponse> {
    return this.api.postRequest('auth', 'social-login' , data).map((res: IResponse)  => {
      return res ;
    });
  }
  authRecaptcha(data): Observable<IResponse> {
    return this.api.postRequest('auth', 'recaptcha' , data).map((res: IResponse)  => {
      return res ;
    });
  }
  accountManager(data): Observable<IResponse> {
    return this.api.postRequest('contact-account-manager', null , data).map((res: IResponse)  => {
      return res ;
    });
  }

  register(data): Observable<IResponse> {
    return this.api.postRequest('auth', 'register' , data).map((res: IResponse)  => {
      return res ;
    });
  }
  socialRegister(data): Observable<IResponse> {
    return this.api.postRequest('auth', 'social-register' , data).map((res: IResponse)  => {
      return res ;
    });
  }
  changePassword(data): Observable<IResponse> {
    return this.api.postRequest('auth', 'change-password' , data).map((res: IResponse)  => {
      return res ;
    });
  }

  public getTeamMembers(): Observable<IResponse> {
    return this.api.getRequest('team-member', null).map( (res: IResponse) => {
      return res;
    });
  }

  public getDashboard(): Observable<IResponse> {
    return this.api.getRequest('dashboard', null).map( (res: IResponse) => {
      return res;
    });
  }
  public getAddressBooks(userId): Observable<IResponse> {
    return this.api.getRequest('address-book', 'user/' + userId).map( (res: IResponse) => {
      return res;
    });
  }
  public saveAddressBook(data): Observable<IResponse> {
    return this.api.postRequest('address-book', null, data).map( (res: IResponse) => {
      return res;
    });
  }
  public updateAddressBook(data, id): Observable<IResponse> {
    return this.api.putRequest('address-book', id, data).map( (res: IResponse) => {
      return res;
    });
  }
  public setDefaultAddressBook(data, userId): Observable<IResponse> {
    return this.api.putRequest('address-book', 'user/' + userId, data).map( (res: IResponse) => {
      return res;
    });
  }
  public getDefaultAddressBook(userId): Observable<IResponse> {
    return this.api.getRequest('address-book', 'user/' + userId + '/default').map( (res: IResponse) => {
      return res;
    });
  }
  public deleteAddressBook(id): Observable<IResponse> {
    return this.api.deleteRequest('address-book', id).map( (res: IResponse) => {
      return res;
    });
  }
  public getTopBlog(): Observable<IResponse> {
    return this.api.getRequest('blog', 'top-post').map( (res: IResponse) => {
      return res;
    });
  }
  public getBlogs(limit, page): Observable<IResponse> {
    return this.api.getRequest('blog',  'normal-posts?limit=' + limit + '&&page=' + page).map( (res: IResponse) => {
      return res;
    });
  }
  public getUserReferrers(userId): Observable<IResponse> {
    return this.api.getRequest('referral/user', userId).map( (res: IResponse) => {
      return res;
    });
  }
  public referUser(data): Observable<IResponse> {
    return this.api.postRequest('referral', null, data).map( (res: IResponse) => {
      return res;
    });
  }
  public getActiveShop(): Observable<IResponse> {
    return this.api.getRequest('farm-shop', null).map( (res: IResponse) => {
      return res;
    });
  }
  public getActiveFarmProducts(): Observable<IResponse> {
    return this.api.getRequest('farm-product', null).map( (res: IResponse) => {
      return res;
    });
  }
  public getDistinctFarmProducts(): Observable<IResponse> {
    return this.api.getRequest('farm-product', 'distinct').map( (res: IResponse) => {
      return res;
    });
  }
  public checkIfFarmIsAvailable(): Observable<IResponse> {
    return this.api.getRequest('dashboard', 'farm-available').map( (res: IResponse) => {
      return res;
    });
  }
  public getShop(id): Observable<IResponse> {
    return this.api.getRequest('farm-shop', id).map( (res: IResponse) => {
      return res;
    });
  }
  public getProduct(id): Observable<IResponse> {
    return this.api.getRequest('farm-product', id).map( (res: IResponse) => {
      return res;
    });
  }
  public filterFarmShop(filter): Observable<IResponse> {
    return this.api.postRequest('farm-shop', 'filter-farm-shop', filter).map( (res: IResponse) => {
      return res;
    });
  }
  public filterFarmProduct(filter): Observable<IResponse> {
    return this.api.postRequest('farm-product', 'filter-farm-product', filter).map( (res: IResponse) => {
      return res;
    });
  }
  public searchFarmShop(search): Observable<IResponse> {
    return this.api.postRequest('farm-shop', 'search-farm-shop', search).map( (res: IResponse) => {
      return res;
    });
  }
  public searchFarmProduct(search): Observable<IResponse> {
    return this.api.postRequest('farm-product', 'search-farm-product', search).map( (res: IResponse) => {
      return res;
    });
  }
  public getActiveType(): Observable<IResponse> {
    return this.api.getRequest('farm-type', 'active-farm-types').map( (res: IResponse) => {
      return res;
    });
  }
  public getActiveProductType(): Observable<IResponse> {
    return this.api.getRequest('product-type', 'active-product-types').map( (res: IResponse) => {
      return res;
    });
  }
  public getActiveStatus(): Observable<IResponse> {
    return this.api.getRequest('farm-status', 'active-farm-status').map( (res: IResponse) => {
      return res;
    });
  }
  public getActiveProductStatus(): Observable<IResponse> {
    return this.api.getRequest('product-status', 'active-product-status').map( (res: IResponse) => {
      return res;
    });
  }
  public logOut(): Observable<IResponse> {
    return this.api.postRequest('auth', 'logout', {}).map((res: IResponse)  => {
      return res ;
    });
  }

  public resetPassword(data): Observable<IResponse> {
    return this.api.postRequest('auth', 'forgot-password', data).map( (res: IResponse) => {
      return res;
    });
  }
  public verifyAccount(data): Observable<IResponse> {
    return this.api.postRequest('auth', 'verify-signup', data).map( (res: IResponse) => {
      return res;
    });
  }
  public verifyPayment(data): Observable<IResponse> {
    return this.api.postRequest('checkout-cart', 'paystack-verify', data).map( (res: IResponse) => {
      return res;
    });
  }
  public verifyWalletFundingPayment(data): Observable<IResponse> {
    return this.api.postRequest('fund-wallet', 'paystack-verify-payment', data).map( (res: IResponse) => {
      return res;
    });
  }
  public forgotPassword(data): Observable<IResponse> {
    return this.api.postRequest('auth', 'reset-password', data).map( (res: IResponse) => {
      return res;
    });
  }
  public resendVerificationLink(data): Observable<IResponse> {
    return this.api.postRequest('auth', 'resend-verify-link', data).map( (res: IResponse) => {
      return res;
    });
  }
  public updateUser(data): Observable<IResponse> {
    return this.api.putRequest('users', data._id, data).map( (res: IResponse) => {
      return res;
    });
  }
  public updateBusinessInfo(data, userId): Observable<IResponse> {
    return this.api.putRequest('profile/business-info', userId, data).map( (res: IResponse) => {
      return res;
    });
  }
  public getTransactionByRef(ref): Observable<IResponse> {
    return this.api.getRequest('transaction/order-reference', ref).map( (res: IResponse) => {
      return res;
    });
  }
  public getOrdersByRef(ref): Observable<IResponse> {
    return this.api.getRequest('orders/order-reference', ref).map( (res: IResponse) => {
      return res;
    });
  }
  public updateBankInfo(data, userId): Observable<IResponse> {
    return this.api.putRequest('profile/bank-setting', userId, data).map( (res: IResponse) => {
      return res;
    });
  }
  public updateNextKin(data, userId): Observable<IResponse> {
    return this.api.putRequest('profile/next-of-kin', userId, data).map( (res: IResponse) => {
      return res;
    });
  }
  public getUser(userId): Observable<IResponse> {
    return this.api.getRequest('users', userId).map( (res: IResponse) => {
      return res;
    });
  }
  public getUpdates(): Observable<IResponse> {
    return this.api.getRequest('updates', 'get-farm-updates-and-notifications').map( (res: IResponse) => {
      return res;
    });
  }
  public getBanks(): Observable<IResponse> {
    return this.api.getRequest('bank', null).map( (res: IResponse) => {
      return res;
    });
  }
  public getBusinessInfo(userId): Observable<IResponse> {
    return this.api.getRequest('profile/business-info', userId).map( (res: IResponse) => {
      return res;
    });
  }
  public getBankInfo(userId): Observable<IResponse> {
    return this.api.getRequest('profile/bank-setting', userId).map( (res: IResponse) => {
      return res;
    });
  }
  public getNextKin(userId): Observable<IResponse> {
    return this.api.getRequest('profile/next-of-kin', userId).map( (res: IResponse) => {
      return res;
    });
  }
  public addNewSubscriber(data): Observable<IResponse> {
    return this.api.postRequest('newsletter', null, data).map( (res: IResponse) => {
      return res;
    });
  }
  public getCarts(userId): Observable<IResponse> {
    return this.api.getRequest('cart', 'user-cart/' + userId).map( (res: IResponse) => {
      return res;
    });
  }
  public getProductCarts(userId): Observable<IResponse> {
    return this.api.getRequest('cart', 'user-product-cart/' + userId).map( (res: IResponse) => {
      return res;
    });
  }

  public getPaymentChannels(can_fund = false): Observable<IResponse> {
    if (can_fund) {
      return this.api.getRequest('payment-channel?can_fund_wallet=true', null).map( (res: IResponse) => {
        return res;
      });
    } else {
      return this.api.getRequest('payment-channel', null).map( (res: IResponse) => {
        return res;
      });
    }
  }
  public getTransactions(userId): Observable<IResponse> {
    return this.api.getRequest('transaction/user', userId).map( (res: IResponse) => {
      return res;
    });
  }
  public getProductOrders(userId): Observable<IResponse> {
    return this.api.getRequest('product-order/user', userId).map( (res: IResponse) => {
      return res;
    });
  }
  public getEducationCategories(): Observable<IResponse> {
    return this.api.getRequest('education-category', null).map( (res: IResponse) => {
      return res;
    });
  }
  public getBlogCategories(): Observable<IResponse> {
    return this.api.getRequest('blog-category', null).map( (res: IResponse) => {
      return res;
    });
  }

  public getEducations(): Observable<IResponse> {
    return this.api.getRequest('education', 'publish').map( (res: IResponse) => {
      return res;
    });
  }
  public saveEducationComment(data): Observable<IResponse> {
    return this.api.postRequest('education-comment', null, data).map( (res: IResponse) => {
      return res;
    });
  }
  public saveBlogComment(data): Observable<IResponse> {
    return this.api.postRequest('blog-comment', null, data).map( (res: IResponse) => {
      return res;
    });
  }
  public saveEducationFeeling(data): Observable<IResponse> {
    return this.api.postRequest('education-like', null, data).map( (res: IResponse) => {
      return res;
    });
  }
  public saveBlogFeeling(data): Observable<IResponse> {
    return this.api.postRequest('blog-like', null, data).map( (res: IResponse) => {
      return res;
    });
  }
  public getMyFeeling(data): Observable<IResponse> {
    return this.api.postRequest('education-like', 'check-user-status', data).map( (res: IResponse) => {
      return res;
    });
  }
  public getMyBlogFeeling(data): Observable<IResponse> {
    return this.api.postRequest('blog-like', 'check-user-status', data).map( (res: IResponse) => {
      return res;
    });
  }
  public getEducationsByCategory(data): Observable<IResponse> {
    return this.api.postRequest('education', 'filter-education', data).map( (res: IResponse) => {
      return res;
    });
  }
  public getBlogsByCategory(data): Observable<IResponse> {
    return this.api.postRequest('blog', 'filter-blog', data).map( (res: IResponse) => {
      return res;
    });
  }
  public searchEducationByTitle(data): Observable<IResponse> {
    return this.api.postRequest('education', 'search-education', data).map( (res: IResponse) => {
      return res;
    });
  }
  public searchBlogByTitle(data): Observable<IResponse> {
    return this.api.postRequest('blog', 'search-blog', data).map( (res: IResponse) => {
      return res;
    });
  }
  public getEducationById(id): Observable<IResponse> {
    return this.api.getRequest('education', id).map( (res: IResponse) => {
      return res;
    });
  }
  public getBlogById(id): Observable<IResponse> {
    return this.api.getRequest('blog', id).map( (res: IResponse) => {
      return res;
    });
  }
  public getPendingTransfers(userId): Observable<IResponse> {
    return this.api.getRequest('transaction/awaiting-payment-proof', userId).map( (res: IResponse) => {
      return res;
    });
  }
  public getWalletTransactions(userId): Observable<IResponse> {
    return this.api.getRequest('wallet-transaction', 'user/' + userId).map( (res: IResponse) => {
      return res;
    });
  }
  public getFarmHistory(userId): Observable<IResponse> {
    return this.api.getRequest('sponsorship', 'history/' + userId ).map( (res: IResponse) => {
      return res;
    });
  }
  public getActiveSponsorship(userId): Observable<IResponse> {
    return this.api.getRequest('sponsorship', 'active/' + userId ).map( (res: IResponse) => {
      return res;
    });
  }
  public getArchiveSponsorship(data): Observable<IResponse> {
    return this.api.postRequest('archive-sponsor', 'user/email', data).map( (res: IResponse) => {
      return res;
    });
  }
  public cancelSponsorship(userId, sponsorId): Observable<IResponse> {
    return this.api.postRequest('sponsorship', 'cancel/' + userId + '/' + sponsorId , {}).map( (res: IResponse) => {
      return res;
    });
  }
  public countCart(userId): Observable<IResponse> {
    return this.api.getRequest('cart', 'user-count/' + userId).map( (res: IResponse) => {
      return res;
    });
  }
  public getCart(cartId): Observable<IResponse> {
    return this.api.getRequest('cart', cartId).map( (res: IResponse) => {
      return res;
    });
  }
  public addShopToCart(data): Observable<IResponse> {
    return this.api.postRequest('cart', null, data).map( (res: IResponse) => {
      return res;
    });
  }
  public addProductToCart(data): Observable<IResponse> {
    return this.api.postRequest('cart', 'product-to-cart', data).map( (res: IResponse) => {
      return res;
    });
  }
  public addProductToCartInBulk(data): Observable<IResponse> {
    return this.api.postRequest('cart', 'product-to-cart-bulk', data).map( (res: IResponse) => {
      return res;
    });
  }
  public updateCart(cartId, data): Observable<IResponse> {
    return this.api.putRequest('cart', cartId, data).map( (res: IResponse) => {
      return res;
    });
  }
  public checkoutFarmShop(data): Observable<IResponse> {
    return this.api.postRequest('checkout-cart', 'farm-shop', data).map( (res: IResponse) => {
      return res;
    });
  }
  public checkoutFarmProduct(data): Observable<IResponse> {
    return this.api.postRequest('product-checkout', 'farm-product', data).map( (res: IResponse) => {
      return res;
    });
  }
  public verifyProductPurchasePayment(data): Observable<IResponse> {
    return this.api.postRequest('product-checkout', 'paystack-verify', data).map( (res: IResponse) => {
      return res;
    });
  }
  public chargeTransaction(data): Observable<IResponse> {
    return this.api.postRequest('checkout-cart', 'calculate-charge', data).map( (res: IResponse) => {
      return res;
    });
  }
  public confirmBankTransfer(data): Observable<IResponse> {
    return this.api.postRequest('bank', 'payment/bank-transfer', data).map( (res: IResponse) => {
      return res;
    });
  }
  public deleteCart(cartId): Observable<IResponse> {
    return this.api.deleteRequest('cart', cartId).map( (res: IResponse) => {
      return res;
    });
  }
  public fundWalletInit(data): Observable<IResponse> {
    return this.api.postRequest('fund-wallet', null, data).map( (res: IResponse) => {
      return res;
    });
  }
  public requestToWithdraw(data): Observable<IResponse> {
    return this.api.postRequest('withdrawal-request', null, data).map( (res: IResponse) => {
      return res;
    });
  }
  public getWallet(userId): Observable<IResponse> {
    return this.api.getRequest('wallet', 'user/' + userId).map( (res: IResponse) => {
      return res;
    });
  }
 }
