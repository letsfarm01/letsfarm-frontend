import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {IResponse} from '../../../interfaces/iresponse';
import {ApiService} from '../../api/api.service.';

@Injectable()
export class SuperAdminService {

  constructor(private api: ApiService) { }
  public getFarmStatus(): Observable<IResponse> {
    return this.api.getRequest('farm-status', null).map( (res: IResponse) => {
      return res;
    });
  }
public getFarmProductStatus(): Observable<IResponse> {
    return this.api.getRequest('product-status', null).map( (res: IResponse) => {
      return res;
    });
  }

  public getUserDashboard(userId): Observable<IResponse> {
    return this.api.getRequest('dashboard', 'user/' + userId).map( (res: IResponse) => {
      return res;
    });
  }
  public getActiveFarmStatus(): Observable<IResponse> {
    return this.api.getRequest('farm-status', 'active-farm-status').map( (res: IResponse) => {
      return res;
    });
  }
  public getActiveProductStatus(): Observable<IResponse> {
    return this.api.getRequest('product-status', 'active-product-status').map( (res: IResponse) => {
      return res;
    });
  }

  public getDashboard(): Observable<IResponse> {
    return this.api.getRequest('dashboard', 'admin').map( (res: IResponse) => {
      return res;
    });
  }
  public createStatus(data): Observable<IResponse> {
    return this.api.postRequest('farm-status', null, data).map( (res: IResponse) => {
      return res;
    });
  }
  public createProductStatus(data): Observable<IResponse> {
    return this.api.postRequest('product-status', null, data).map( (res: IResponse) => {
      return res;
    });
  }
  public createTeamGroup(data): Observable<IResponse> {
    return this.api.postRequest('team-group', null, data).map( (res: IResponse) => {
      return res;
    });
  }
  public createTeamMember(data): Observable<IResponse> {
    return this.api.postRequest('team-member', null, data).map( (res: IResponse) => {
      return res;
    });
  }
  public updateTeamGroup(data, id): Observable<IResponse> {
    return this.api.putRequest('team-group', id, data).map( (res: IResponse) => {
      return res;
    });
  }
  public updateTeamMember(data, id): Observable<IResponse> {
    return this.api.putRequest('team-member', id, data).map( (res: IResponse) => {
      return res;
    });
  }
  public updateStatus(data, id): Observable<IResponse> {
    return this.api.putRequest('farm-status', id, data).map( (res: IResponse) => {
      return res;
    });
  }
  public updateProductStatus(data, id): Observable<IResponse> {
    return this.api.putRequest('product-status', id, data).map( (res: IResponse) => {
      return res;
    });
  }
  public updateUser(data, id): Observable<IResponse> {
    return this.api.putRequest('users', id, data).map( (res: IResponse) => {
      return res;
    });
  }
  public deleteStatus(id): Observable<IResponse> {
    return this.api.deleteRequest('farm-status', id).map( (res: IResponse) => {
      return res;
    });
  }
  public deleteProductStatus(id): Observable<IResponse> {
    return this.api.deleteRequest('product-status', id).map( (res: IResponse) => {
      return res;
    });
  }
  public getFarmType(): Observable<IResponse> {
    return this.api.getRequest('farm-type', null).map( (res: IResponse) => {
      return res;
    });
  }
  public getProductType(): Observable<IResponse> {
    return this.api.getRequest('product-type', null).map( (res: IResponse) => {
      return res;
    });
  }
  public getActiveFarmTypes(): Observable<IResponse> {
    return this.api.getRequest('farm-type', 'active-farm-types').map( (res: IResponse) => {
      return res;
    });
  }
  public getActiveProductTypes(): Observable<IResponse> {
    return this.api.getRequest('product-type', 'active-product-types').map( (res: IResponse) => {
      return res;
    });
  }
  public getEducationCategories(): Observable<IResponse> {
    return this.api.getRequest('education-category', null).map( (res: IResponse) => {
      return res;
    });
  }
  public getBlogCategories(): Observable<IResponse> {
    return this.api.getRequest('blog-category', null).map( (res: IResponse) => {
      return res;
    });
  }
  public createType(data): Observable<IResponse> {
    return this.api.postRequest('farm-type', null, data).map( (res: IResponse) => {
      return res;
    });
  }
  public createProductType(data): Observable<IResponse> {
    return this.api.postRequest('product-type', null, data).map( (res: IResponse) => {
      return res;
    });
  }
  public createEducationCategory(data): Observable<IResponse> {
    return this.api.postRequest('education-category', null, data).map( (res: IResponse) => {
      return res;
    });
  }
  public createBlogCategory(data): Observable<IResponse> {
    return this.api.postRequest('blog-category', null, data).map( (res: IResponse) => {
      return res;
    });
  }
  public updateEducationCategory(data, id): Observable<IResponse> {
    return this.api.putRequest('education-category', id, data).map( (res: IResponse) => {
      return res;
    });
  }
  public updateBlogCategory(data, id): Observable<IResponse> {
    return this.api.putRequest('blog-category', id, data).map( (res: IResponse) => {
      return res;
    });
  }
  public updateType(data, id): Observable<IResponse> {
    return this.api.putRequest('farm-type', id, data).map( (res: IResponse) => {
      return res;
    });
  }
  public updateProductType(data, id): Observable<IResponse> {
    return this.api.putRequest('product-type', id, data).map( (res: IResponse) => {
      return res;
    });
  }
  public deleteType(id): Observable<IResponse> {
    return this.api.deleteRequest('farm-type', id).map( (res: IResponse) => {
      return res;
    });
  }
  public deleteProductType(id): Observable<IResponse> {
    return this.api.deleteRequest('product-type', id).map( (res: IResponse) => {
      return res;
    });
  }
  public deleteEducationType(id): Observable<IResponse> {
    return this.api.deleteRequest('education-category', id).map( (res: IResponse) => {
      return res;
    });
  }
  public deleteBlogType(id): Observable<IResponse> {
    return this.api.deleteRequest('blog-category', id).map( (res: IResponse) => {
      return res;
    });
  }
  public getFarmShops(): Observable<IResponse> {
    return this.api.getRequest('farm-shop', null).map( (res: IResponse) => {
      return res;
    });
  }
  public getUpdates(): Observable<IResponse> {
    return this.api.getRequest('updates', null).map( (res: IResponse) => {
      return res;
    });
  }
  public createUpdate(data): Observable<IResponse> {
    return this.api.postRequest('updates', null, data).map( (res: IResponse) => {
      return res;
    });
  }
  public publishUpdate(data): Observable<IResponse> {
    return this.api.postRequest('updates', 'push-updates', data).map( (res: IResponse) => {
      return res;
    });
  }
  public deleteUpdate(id): Observable<IResponse> {
    return this.api.deleteRequest('updates', id).map( (res: IResponse) => {
      return res;
    });
  }
  public updateUpdate(data, id): Observable<IResponse> {
    return this.api.putRequest('updates', id, data).map( (res: IResponse) => {
      return res;
    });
  }
  public getFarmProducts(): Observable<IResponse> {
    return this.api.getRequest('farm-product', null).map( (res: IResponse) => {
      return res;
    });
  }

  public getProductOrders(status): Observable<IResponse> {
    return this.api.getRequest('product-order?status=' + status, null).map( (res: IResponse) => {
      return res;
    });
  }

  public setProductOrderStatus(data, order_id): Observable<IResponse> {
    return this.api.putRequest('product-order', order_id, data).map( (res: IResponse) => {
      return res;
    });
  }
  public getTeamMembers(): Observable<IResponse> {
    return this.api.getRequest('team-member', null).map( (res: IResponse) => {
      return res;
    });
  }
  public getPrivileges(): Observable<IResponse> {
    return this.api.getRequest('privileges', null).map( (res: IResponse) => {
      return res;
    });
  }
  public getPendingBankPayment(): Observable<IResponse> {
    return this.api.getRequest('bank', 'payment/bank-transfer').map( (res: IResponse) => {
      return res;
    });
  }
  public getConfirmedBankPayment(): Observable<IResponse> {
    return this.api.getRequest('bank', 'payment/bank-transfer/old').map( (res: IResponse) => {
      return res;
    });
  }
  public getNewsletter(): Observable<IResponse> {
    return this.api.getRequest('newsletters', null).map( (res: IResponse) => {
      return res;
    });
  }
  public getAccountManagers(): Observable<IResponse> {
    return this.api.getRequest('contact-account-manager', null).map( (res: IResponse) => {
      return res;
    });
  }
  public resolveMessage(id): Observable<IResponse> {
    return this.api.getRequest('contact-account-manager', id).map( (res: IResponse) => {
      return res;
    });
  }
  public getEducationPosts(): Observable<IResponse> {
    return this.api.getRequest('education', null).map( (res: IResponse) => {
      return res;
    });
  }
  public getBlogPosts(): Observable<IResponse> {
    return this.api.getRequest('blog', null).map( (res: IResponse) => {
      return res;
    });
  }
  public createEducation(data): Observable<IResponse> {
    return this.api.postRequest('education', null, data).map( (res: IResponse) => {
      return res;
    });
  }
  public createBlog(data): Observable<IResponse> {
    return this.api.postRequest('blog', null, data).map( (res: IResponse) => {
      return res;
    });
  }
  public createAdmin(data): Observable<IResponse> {
    return this.api.postRequest('users', null, data).map( (res: IResponse) => {
      return res;
    });
  }
  public makeTopBlog(data): Observable<IResponse> {
    return this.api.postRequest('blog', 'top-post', data).map( (res: IResponse) => {
      return res;
    });
  }
  public updateEducation(data, id): Observable<IResponse> {
    return this.api.putRequest('education', id, data).map( (res: IResponse) => {
      return res;
    });
  }
  public updateBlog(data, id): Observable<IResponse> {
    return this.api.putRequest('blog', id, data).map( (res: IResponse) => {
      return res;
    });
  }
  public deleteBlog(blogId): Observable<IResponse> {
    return this.api.deleteRequest('blog', blogId).map( (res: IResponse) => {
      return res;
    });
  }
  public deleteAdmin(adminId): Observable<IResponse> {
    return this.api.deleteRequest('users', adminId).map( (res: IResponse) => {
      return res;
    });
  }
  public getTransactions(): Observable<IResponse> {
    return this.api.getRequest('transaction', null).map( (res: IResponse) => {
      return res;
    });
  }
  public getVersion(): Observable<IResponse> {
    return this.api.getRequest('dashboard', 'app-version').map( (res: IResponse) => {
      return res;
    });
  }

  public cancelSponsorship(userId, sponsorId): Observable<IResponse> {
    return this.api.postRequest('sponsorship', 'cancel/' + userId + '/' + sponsorId , {}).map( (res: IResponse) => {
      return res;
    });
  }

  public fundUserWalletSingle(data): Observable<IResponse> {
    return this.api.postRequest('completed-sponsor', 'sendToWallet', data).map( (res: IResponse) => {
      return res;
    });
  }
  public acceptRequest(data): Observable<IResponse> {
    return this.api.postRequest('withdrawal-request', 'manual-accept-request', data).map( (res: IResponse) => {
      return res;
    });
  }
  public rejectRequest(data): Observable<IResponse> {
    return this.api.postRequest('withdrawal-request', 'reject-request', data).map( (res: IResponse) => {
      return res;
    });
  }
  public setSponsorCompleted(sponsor): Observable<IResponse> {
    return this.api.postRequest('sponsorship', 'completed', sponsor).map( (res: IResponse) => {
      return res;
    });
  }
  public setArchiveSponsorCompleted(sponsor): Observable<IResponse> {
    return this.api.postRequest('archive-sponsor', 'completed', sponsor).map( (res: IResponse) => {
      return res;
    });
  }
  public saveVersion(data): Observable<IResponse> {
    return this.api.postRequest('dashboard', 'app-version', data).map( (res: IResponse) => {
      return res;
    });
  }
  public getPaystackTransactions(status = null): Observable<IResponse> {
    if (status) {
      return this.api.getRequest('transaction', 'paystack-transaction?status=' + status).map( (res: IResponse) => {
        return res;
      });
    } else {
      return this.api.getRequest('transaction', 'paystack-transaction').map( (res: IResponse) => {
        return res;
      });
    }
  }
  public getBankTransactions(): Observable<IResponse> {
    return this.api.getRequest('transaction', 'bank-transaction').map( (res: IResponse) => {
      return res;
    });
  }
  public getReferrals(): Observable<IResponse> {
    return this.api.getRequest('referral', null).map( (res: IResponse) => {
      return res;
    });
  }
  public getWalletTransactions(): Observable<IResponse> {
    return this.api.getRequest('wallet-transaction', null).map( (res: IResponse) => {
      return res;
    });
  }
  public verifyPaystackManually(data): Observable<IResponse> {
    return this.api.postRequest('checkout-cart', 'paystack-verify-manually', data).map( (res: IResponse) => {
      return res;
    });
  }
  public getOrdersByRef(ref): Observable<IResponse> {
    return this.api.getRequest('orders/order-reference', ref).map( (res: IResponse) => {
      return res;
    });
  }
  public getUserBank(userId): Observable<IResponse> {
    return this.api.getRequest('profile', 'bank-setting/' + userId).map( (res: IResponse) => {
      return res;
    });
  }
  public getPaystackRef(): Observable<IResponse> {
    return this.api.getRequest('paystack-reference', null).map( (res: IResponse) => {
      return res;
    });
  }

  public getSponsorship(filter, query): Observable<IResponse> {
    return this.api.postRequest(`sponsorship${query? '?status='+ query.status: '' }`, null , filter).map( (res: IResponse) => {
      return res;
    });
  }
/*
  public getCompletedSponsorship(status): Observable<IResponse> {
    return this.api.getRequest('completed-sponsor', '?status=' + status ).map( (res: IResponse) => {
      return res;
    });
  }*/

  public getCompletedSponsorship(filter, query): Observable<IResponse> {
    return this.api.postRequest(`completed-sponsor${query? '?status='+ query: '' }`, null , filter).map( (res: IResponse) => {
      return res;
    });
  }
  public getRequestedWithdraw(): Observable<IResponse> {
    return this.api.getRequest('withdrawal-request', null).map( (res: IResponse) => {
      return res;
    });
  }
  public getWithdrawalHistory(): Observable<IResponse> {
    return this.api.getRequest('withdrawal-request?status=APPROVED', null).map( (res: IResponse) => {
      return res;
    });
  }
  public confirmBankTransfer(data): Observable<IResponse> {
    return this.api.postRequest('checkout-cart', 'confirm-bank-transfer', data).map( (res: IResponse) => {
      return res;
    });
  }
  public confirmBankTransferWalletFunding(data): Observable<IResponse> {
    return this.api.postRequest('fund-wallet', 'confirm-bank-transfer', data).map( (res: IResponse) => {
      return res;
    });
  }
  public cancelConfirmationProof(data): Observable<IResponse> {
    return this.api.postRequest('fund-wallet', 'cancel-proof', data).map( (res: IResponse) => {
      return res;
    });
  }
  public reInitCancelProof(data): Observable<IResponse> {
    return this.api.postRequest('fund-wallet', 'uncancel-proof', data).map( (res: IResponse) => {
      return res;
    });
  }
  public getUserGroups(): Observable<IResponse> {
    return this.api.getRequest('user-groups', null).map( (res: IResponse) => {
      return res;
    });
  }
  public getUsers(type): Observable<IResponse> {
    return this.api.getRequest('users?role=' + type.role, null).map( (res: IResponse) => {
      return res;
    });
  }
  public getPayouts(): Observable<IResponse> {
    return this.api.getRequest('repay', 'auto-get-repay').map( (res: IResponse) => {
      return res;
    });
  }
  public updatePayoutRepay(data, id): Observable<IResponse> {
    return this.api.putRequest('repay', id, data).map( (res: IResponse) => {
      return res;
    });
  }
  public generateRepay(data): Observable<IResponse> {
    return this.api.postRequest('repay', 'generate-repay', data).map( (res: IResponse) => {
      return res;
    });
  }
  public getPayoutByYear(year): Observable<IResponse> {
    return this.api.getRequest('repay', 'repay-by-year/'+ year).map( (res: IResponse) => {
      return res;
    });
  }
  public getCurrentYear(): Observable<IResponse> {
    return this.api.getRequest('repay', 'get-current-year').map( (res: IResponse) => {
      return res;
    });
  }
  public getUser(id): Observable<IResponse> {
    return this.api.getRequest('users', id).map( (res: IResponse) => {
      return res;
    });
  }

  public getActiveSponsorship(userId): Observable<IResponse> {
    return this.api.getRequest('sponsorship', 'active/' + userId ).map( (res: IResponse) => {
      return res;
    });
  }
  public getArchiveSponsorship(data): Observable<IResponse> {
    return this.api.postRequest('archive-sponsor', 'user/email', data).map( (res: IResponse) => {
      return res;
    });
  }
  public getAllArchiveSponsorship(filter, query): Observable<IResponse> {
    return this.api.postRequest(`archive-sponsor${query? '?status='+ query.status: '' }`, null, filter).map( (res: IResponse) => {
      return res;
    });
  }
  public createShop(data): Observable<IResponse> {
    return this.api.postRequest('farm-shop', null, data).map( (res: IResponse) => {
      return res;
    });
  }
  public createProduct(data): Observable<IResponse> {
    return this.api.postRequest('farm-product', null, data).map( (res: IResponse) => {
      return res;
    });
  }
  public updateShop(data, id): Observable<IResponse> {
    return this.api.putRequest('farm-shop', id, data).map( (res: IResponse) => {
      return res;
    });
  }
  public updateProduct(data, id): Observable<IResponse> {
    return this.api.putRequest('farm-product', id, data).map( (res: IResponse) => {
      return res;
    });
  }
  public deleteShop(id): Observable<IResponse> {
    return this.api.deleteRequest('farm-shop', id).map( (res: IResponse) => {
      return res;
    });
  }
  public deleteProduct(id): Observable<IResponse> {
    return this.api.deleteRequest('farm-product', id).map( (res: IResponse) => {
      return res;
    });
  }
  public deleteEducation(id): Observable<IResponse> {
    return this.api.deleteRequest('education', id).map( (res: IResponse) => {
      return res;
    });
  }
  public deleteTeamGroup(id): Observable<IResponse> {
    return this.api.deleteRequest('team-group', id).map( (res: IResponse) => {
      return res;
    });
  }
  public deleteTeamMember(id): Observable<IResponse> {
    return this.api.deleteRequest('team-member', id).map( (res: IResponse) => {
      return res;
    });
  }
  public deleteUser(id): Observable<IResponse> {
    return this.api.deleteRequest('users', id).map( (res: IResponse) => {
      return res;
    });
  }
  public createPaymentChannel(data): Observable<IResponse> {
    return this.api.postRequest('payment-channel', null, data).map( (res: IResponse) => {
      return res;
    });
  }
  public updatePaymentChannel(data, id): Observable<IResponse> {
    return this.api.putRequest('payment-channel', id, data).map( (res: IResponse) => {
      return res;
    });
  }
  public deletePaymentChannel(id): Observable<IResponse> {
    return this.api.deleteRequest('payment-channel', id).map( (res: IResponse) => {
      return res;
    });
  }
  public getPaymentChannels(): Observable<IResponse> {
    return this.api.getRequest('payment-channel', null).map( (res: IResponse) => {
      return res;
    });
  }
  public getWallet(userId): Observable<IResponse> {
    return this.api.getRequest('wallet', 'user/' + userId).map( (res: IResponse) => {
      return res;
    });
  }
}
