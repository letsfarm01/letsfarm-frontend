import {Injectable} from '@angular/core';
import * as CryptoJS from 'crypto-js';
import {isNullOrUndefined} from 'util';
import {CacheService} from '../cacheService/cache.service';
import { environment as env } from '../../../environments/environment';
// import * as  JSEncrypt from 'jsencrypt';
import { JSEncrypt } from 'jsencrypt'
@Injectable()
export class EncryptDataService {
key: string;
  constructor(
    private cacheService: CacheService,
  ) {
    this.key = env.PRIVATE_KEY;
  }

  /**
   *
   * @param data
   * @param key
   * @param type
   */
  encryptObject(data: any, key: string, type = 'setStorage') {
    // Encrypt the Password
    const object = JSON.stringify(data);
    const encrypted = CryptoJS.AES.encrypt(object, this.key);
    // Save the Login in to an array
    this.cacheService[type](key, encrypted);
    return true;
  }

  /**
   *
   * @param data
   * @param key
   * @param type
   */
  encryptByKeyString(data: any, key, type = 'setStorage') {
    this.key = env.PRIVATE_KEY;
    const encrypted = CryptoJS.AES.encrypt(data, this.key);
    this.cacheService[type](key, encrypted);
    return true;
  }

  /**
   * USe the jsEncrypt technique
   * @param string
   * @returns {null|*|any|PromiseLike<ArrayBuffer>|CipherParams|PromiseLike<ArrayBuffer>}
   */
  jsEncrypt(string) {
    if (!string) {
      return null
    }
    const encrypt = new JSEncrypt();
    encrypt.setPublicKey(env._PUBLIC_KEY);
    return encrypt.encrypt(string);
  }
  /**
   * Encrypt data and call back
   * @param data
   * @param callback
   * @returns {null}
   */
  jsEncryptWithCallback(data, callback) {
    if (!data) {
      return null
    }
    const stringified = JSON.stringify(data);
    console.log('Daata to encrypt', stringified);
    const encrypt = new JSEncrypt();
    encrypt.setPublicKey(env._PUBLIC_KEY);
    const val = encrypt.encrypt(stringified);
    callback(val);
    // return val;
  }

  /**
   * Use the jsEncrypt technique to decrypt data
   * @param data
   * @returns {*|WordArray|any|PromiseLike<ArrayBuffer>|null}
   */
  jsDecrypt(data) {
    console.log('DATA ', data);
    if (!data) {
      return null
    }
    const decrypt = new JSEncrypt();
    decrypt.setPrivateKey(env._PRIVATE_KEY);
    return JSON.parse(decrypt.decrypt(data));
  }
}
