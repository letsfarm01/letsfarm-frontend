import { Component, OnInit } from '@angular/core';
import {NavigatorService} from '../../services/navigatorService/navigator.service';
import {IResponse} from '../../interfaces/iresponse';
import {UtilService} from '../../services/utilService/util.service';
import {BootstrapNotifyService} from '../../services/bootstrap-notify/bootstrap-notify.service';
import {SuperAdminService} from '../../services/api-handlers/superAdminService/super-admin.service';
import {configs} from '../../configs/config';
import {EncryptDataService} from "../../services/encryption/encrypt-data.service";
import * as moment from 'moment';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-all-sponsors',
  templateUrl: './all-sponsors.component.html',
  styleUrls: ['./all-sponsors.component.css']
})
export class AllSponsorsComponent implements OnInit {
  breadCrumb: any;
  farmHistory: any[] = [];
  activeSponsor: any[] = [];
  // archiveSponsors: any[] = [];
  roiData: any[] = [];
  year = null;
  selected_status = 'ALL';
  showData = true;
  showData2 = true;
  filter = null;
  allMonths = configs.months;
  allYears = configs.years;
  allStatus = configs.sponsorStatus;
  public loadingData = false;
  public loadingData2 = false;
  currentUser = null;
  processing = false;
  // password: null;
  constructor(private navigatorService: NavigatorService,
              private alertService: BootstrapNotifyService,
              private encrypt: EncryptDataService,
              private superAdminService: SuperAdminService, private utilService: UtilService) { }

  ngOnInit() {
    this.currentUser = this.utilService.getAuthUser();
    this.breadCrumb  = {
      name: 'Sponsorships',
      parent: 'Report',
      subLink: null
    };
    // this.getActiveSponsor();
    this.getSponsorships(this.filter);
    // this.getArchive();
  }
  gotoURL(url) {
    this.navigatorService.navigateUrl(url);
  }

  getSponsorships(filter, query = null) {
    this.showData = false;
    this.loadingData = true;
    if(this.selected_status === 'ALL') {
      query = null;// for now, but once we have limit and pages kindly re-modify this.
    }
    this.superAdminService.getSponsorship(filter, query)
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.farmHistory = res.data;
        this.loadingData = false;
        this.showData = true;
        this.utilService.startDatatable('all--history');
        // this.getArchive(filter);
      }, error => {
        console.log('Error ', error);
        this.loadingData = false;
        this.showData = true;
        this.utilService.startDatatable('all--history');
        // this.getArchive(filter);
      });
  }

 /* getActiveSponsor() {
    this.showData2 = false;
    this.loadingData2 = true;
    this.superAdminService.getActiveSponsorship()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.activeSponsor = res.data;
        this.utilService.startDatatable('active-farm-history');
        this.loadingData2 = false;
        this.showData2 = true;
      }, error => {
        console.log('Error ', error);
        this.utilService.startDatatable('active-farm-history');
        this.loadingData2 = false;
        this.showData2 = true;
      });
  }
*/
  viewROI(data) {
    this.activeSponsor = data;
    this.roiData = data.roi_data;
    this.utilService.openModal('foodTrading');
  }
  closeTradingModal() {
    this.utilService.closeModal('foodTrading');
    this.roiData = [];
  }
  filterSponsorByMonth(event) {
    console.log('Evenrt ', event);
    if(!event) {
      this.getSponsorships(null);
    } else {
      this.farmHistory = [];
      // this.archiveSponsors = [];
      this.filter = {month: event.value};
      this.getSponsorships({month: event.value, year: this.year});
    }
  }


   filterOrdersByStatus(event) {
    console.log('EVENT ', event);
    if (!event) { return false; }
    this.selected_status = event.value;
    this.getSponsorships(this.filter, {status: this.selected_status});
  }
  async markCompleted(sponsor, i, pos=null, type=null) {
    this.processing = true;
    $('.processing-').addClass('d-none');
    if(i || i === 0) {
      $('#processing-' + i).removeClass('d-none');
    }
    if(type === 'advance') {
      this.utilService.closeModal('foodTrading');
    }
      const { value: password } = await (<any>Swal).fire({
        title: 'Enter your password to continue!',
        input: 'password',
        inputPlaceholder: 'Enter your password',
        inputAttributes: {
          maxlength: 70,
          autocapitalize: 'off',
          autocorrect: 'off'
        }
      });

      if(password) {
        this.superAdminService.setSponsorCompleted({
          email: this.currentUser.email,
          sponsorId: sponsor._id,
          type: type !== 'advance' ? 'normal' : 'advance',
          pos: pos,
          password: this.encrypt.jsEncrypt(password)}).subscribe((res: IResponse) => {
          this.alertService.success(res.msg);
          this.processing = false;
          $('.processing-').addClass('d-none');
          this.getSponsorships(this.filter);
        }, error => {
          console.log('Error ', error);
          this.processing = false;
          $('.processing-').addClass('d-none');
          this.alertService.error(error.error.msg || 'Unable to mark this sponsor as completed');
        });
      } else {
        this.processing = false;
        $('.processing-').addClass('d-none');
      }

  }
}
