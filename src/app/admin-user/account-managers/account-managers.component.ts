import { Component, OnInit } from '@angular/core';
import {SuperAdminService} from "../../services/api-handlers/superAdminService/super-admin.service";
import {UtilService} from "../../services/utilService/util.service";
import {IResponse} from "../../interfaces/iresponse";

@Component({
  selector: 'app-account-managers',
  templateUrl: './account-managers.component.html',
  styleUrls: ['./account-managers.component.css']
})
export class AccountManagersComponent implements OnInit {
  breadCrumb: any;

  loaders = {
    saving: false
  };
  accountManagers: any [] = [];
  newCustomer =  {
    name: null,
    status: true
  };
  public activeCustomer = null;
  public loadingData = false;
  public modal = {
    title: 'Add New Customer',
    btnText: 'SAVE'
  };
  showData = true;

  constructor(private superAdminService: SuperAdminService,
              private utilService: UtilService) { }

  ngOnInit() {
    this.breadCrumb  = {
      name: 'Messages',
      parent: 'Account Manager',
      subLink: null
    };
    this.getManagers();
  }

  getManagers() {
    this.showData = false;
    this.loadingData = true;
    this.superAdminService.getAccountManagers()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.accountManagers = res.data;
        this.utilService.startDatatable('all-newsletters');
        this.loadingData = false;
        this.showData = true;
      }, error => {
        console.log('Error ', error);
        this.utilService.startDatatable('all-newsletters');
        this.loadingData = false;
        this.showData = true;
      });
  }
  resolveMessage(message) {
    this.superAdminService.resolveMessage(message._id)
      .subscribe((res: IResponse) => {
      this.getManagers();
      }, error => {
      console.log('Unable to resolve complain');
      });
  }
}
