import { Component, OnInit } from '@angular/core';
import {NavigatorService} from '../../services/navigatorService/navigator.service';
import {IResponse} from '../../interfaces/iresponse';
import {UtilService} from '../../services/utilService/util.service';
import {BootstrapNotifyService} from '../../services/bootstrap-notify/bootstrap-notify.service';
import {SuperAdminService} from '../../services/api-handlers/superAdminService/super-admin.service';
import {configs} from '../../configs/config';
import Swal from 'sweetalert2';
import {EncryptDataService} from "../../services/encryption/encrypt-data.service";

@Component({
  selector: 'app-all-archive-sponsors',
  templateUrl: './all-archive-sponsors.component.html',
  styleUrls: ['./all-archive-sponsors.component.css']
})
export class AllArchiveSponsorsComponent implements OnInit {
  breadCrumb: any;
  activeSponsor: any[] = [];
  archiveSponsors: any[] = [];
  showData = true;
  showData2 = true;
  processing = false;
  year = null;
  selected_status = 'ALL';
  allMonths = configs.months;
  allStatus = configs.sponsorStatus;
  allYears = configs.years;
  public loadingData = false;
  public loadingData2 = false;
  currentUser = null;
  filter = null;
  constructor(private navigatorService: NavigatorService,
              private alertService: BootstrapNotifyService,
              private encrypt: EncryptDataService,
              private superAdminService: SuperAdminService, private utilService: UtilService) { }

  ngOnInit() {
    this.currentUser = this.utilService.getAuthUser();
    this.breadCrumb  = {
      name: 'Archive Sponsorships',
      parent: 'Report',
      subLink: null
    };
    // this.getActiveSponsor();
    this.getSponsorships(this.filter);
    // this.getArchive();
  }
  gotoURL(url) {
    this.navigatorService.navigateUrl(url);
  }

   getSponsorships(filter, query=null) {
    this.showData = false;
    this.loadingData = true;
    if(this.selected_status === 'ALL') {
      query = null;// for now, but once we have limit and pages kindly re-modify this.
    }
    this.superAdminService.getAllArchiveSponsorship(filter, query)
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.archiveSponsors = res.data;
        this.loadingData = false;
        this.showData = true;
        this.utilService.startDatatable('all-archive-farm-sponsor');
      }, error => {
        console.log('Error ', error);
        this.loadingData = false;
        this.showData = true;
        this.utilService.startDatatable('all-archive-farm-sponsor');
      });
  }
  filterSponsorByMonth(event) {
    console.log('Evenrt ', event);
    if(!event) {
      this.getSponsorships(null);
    } else {
      this.archiveSponsors = [];
      this.filter = {month: event.value};
      this.getSponsorships({month: event.value, year: this.year});
    }
  }

  filterOrdersByStatus(event) {
    console.log('EVENT ', event);
    if (!event) { return false; }
    this.selected_status = event.value;
    this.getSponsorships(this.filter, {status: this.selected_status});
  }
  async markCompleted(sponsor, i) {
    this.processing = true;
    $('.processing-').addClass('d-none');
    if(i || i === 0) {
      $('#processing-' + i).removeClass('d-none');
    }
    const { value: password } = await (<any>Swal).fire({
      title: 'Enter your password to continue!',
      input: 'password',
      inputPlaceholder: 'Enter your password',
      inputAttributes: {
        maxlength: 70,
        autocapitalize: 'off',
        autocorrect: 'off'
      }
    });

    if(password) {
      this.superAdminService.setArchiveSponsorCompleted({
        email: this.currentUser.email,
        sponsorId: sponsor._id,
        password: this.encrypt.jsEncrypt(password)}).subscribe((res: IResponse) => {
        this.alertService.success(res.msg);
        this.processing = false;
        $('.processing-').addClass('d-none');
        this.getSponsorships(this.filter);
      }, error => {
        console.log('Error ', error);
        this.processing = false;
        $('.processing-').addClass('d-none');
        this.alertService.error(error.error.msg || 'Unable to mark this sponsor as completed');
      });
    } else {
      this.processing = false;
      $('.processing-').addClass('d-none');
    }

  }

}
