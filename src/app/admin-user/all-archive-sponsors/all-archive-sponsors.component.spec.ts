import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllArchiveSponsorsComponent } from './all-archive-sponsors.component';

describe('AllArchiveSponsorsComponent', () => {
  let component: AllArchiveSponsorsComponent;
  let fixture: ComponentFixture<AllArchiveSponsorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllArchiveSponsorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllArchiveSponsorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
