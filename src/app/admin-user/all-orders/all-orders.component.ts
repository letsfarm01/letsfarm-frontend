import { Component, OnInit } from '@angular/core';
import {IResponse} from '../../interfaces/iresponse';
import {SuperAdminService} from '../../services/api-handlers/superAdminService/super-admin.service';
import {UtilService} from '../../services/utilService/util.service';

@Component({
  selector: 'app-all-orders',
  templateUrl: './all-orders.component.html',
  styleUrls: ['./all-orders.component.css']
})
export class AllOrdersComponent implements OnInit {
  breadCrumb: any;

  loaders = {
    saving: false
  };
  productsOrders: any [] = [];
  newCustomer =  {
    name: null,
    status: true
  };
  public activeCustomer = null;
  public loadingData = false;
  public modal = {
    title: 'Add New Customer',
    btnText: 'SAVE'
  };
  showData = true;

  constructor(private superAdminService: SuperAdminService,
              private utilService: UtilService) { }

  ngOnInit() {
    this.breadCrumb  = {
      name: 'Orders',
      parent: 'Farm Shops',
      subLink: null
    };
    this.getProductOrders();
  }

  getProductOrders() {
    this.showData = false;
    this.loadingData = true;
    this.superAdminService.getPendingBankPayment()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.productsOrders = res.data.data;
        this.utilService.startDatatable('all-orders');
        this.loadingData = false;
        this.showData = true;
      }, error => {
        console.log('Error ', error);
        this.utilService.startDatatable('all-orders');
        this.loadingData = false;
        this.showData = true;
      });
  }

}
