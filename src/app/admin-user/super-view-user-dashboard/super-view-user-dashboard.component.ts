import { Component, OnInit } from '@angular/core';
import { Label } from 'ng2-charts';

import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import {UserService} from "../../services/api-handlers/userService/user.service";
import {NavigatorService} from "../../services/navigatorService/navigator.service";
import {UtilService} from "../../services/utilService/util.service";
import {IResponse} from "../../interfaces/iresponse";
import {SuperAdminService} from "../../services/api-handlers/superAdminService/super-admin.service";
import {ActivatedRoute} from "@angular/router";
import {BootstrapNotifyService} from "../../services/bootstrap-notify/bootstrap-notify.service";

@Component({
  selector: 'app-super-view-user-dashboard',
  templateUrl: './super-view-user-dashboard.component.html',
  styleUrls: ['./super-view-user-dashboard.component.css']
})
export class SuperViewUserDashboardComponent implements OnInit {
  breadCrumb: any;
  dashboard = null;
  currentUser = null;
  loaders = {
    loading: true,
    loadingData: false
  };
  activeSponsor = [];
  archiveSponsors = [];
  roiData = [];


  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];

  userId = null;
  user = null;
  public barChartData: ChartDataSets[] = [];
  constructor(private userService: SuperAdminService,
              private navigatorService: NavigatorService,
              private route: ActivatedRoute,
              private alertService: BootstrapNotifyService,
              private utilService: UtilService) {
    this.userId = this.route.snapshot.paramMap.get('userId') || null;
    if (!this.userId) {
      this.navigatorService.navigateUrl('/super/dashboard');
    }
  }

  ngOnInit() {
    this.currentUser = this.utilService.getAuthUser();
    this.getDashboard();
    this.getUser(this.userId);
    this.breadCrumb  = {
      name: 'Dashboard',
      parent: 'Home',
      subLink: null
    };

  }
  getUser(id) {
    this.userService.getUser(id).subscribe((res: IResponse) => {
      this.user = res.data;
      this.getActiveSponsor();
      this.getArchive();
    }, error => {
    });
  }
  getDashboard() {
    this.loaders.loading = true;
    this.userService.getUserDashboard(this.userId).subscribe((res: IResponse) => {
      console.log('Res ', res);
      this.dashboard = res.data;
      let invested, returns, interests, descriptions;
      /*if (this.dashboard.expected_returns.invested.length > 9) {
        descriptions = this.dashboard.expected_returns.description.reverse().slice(0, 9).reverse();
        invested = this.dashboard.expected_returns.invested.reverse().slice(0, 9).reverse();
        returns = this.dashboard.expected_returns.returns.reverse().slice(0, 9).reverse();
        interests = this.dashboard.expected_returns.interests.reverse().slice(0, 9).reverse();
      } else {*/
      descriptions = this.dashboard.expected_returns.description;
      invested = this.dashboard.expected_returns.invested;
      returns = this.dashboard.expected_returns.returns;
      interests = this.dashboard.expected_returns.interests;
      // }
      this.barChartLabels = descriptions;
      console.log('DATA ', invested, returns, interests);
      this.barChartData = [
        { data: invested, label: 'Amount Invested' },
        { data: returns, label: 'Expected Return' },
        { data: interests, label: 'Calculated Interest' }
      ];
      this.loaders.loading = false;
    }, error => {
      console.log('Error ', error );
    });
  }
  gotoSponsor() {
    this.navigatorService.navigateUrl('/user/my-farm');
  }
  gotoFarm() {
    this.navigatorService.navigateUrl('/user/my-farm');
  }

  getActiveSponsor() {
    this.loaders.loadingData = true;
    this.userService.getActiveSponsorship(this.user._id)
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.activeSponsor = res.data;
        this.loaders.loadingData = false;
      }, error => {
        console.log('Error ', error);
        this.loaders.loadingData = false;
      });
  }
  getArchive() {
    this.userService.getArchiveSponsorship({email: this.user.email})
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.archiveSponsors = res.data;
        this.utilService.startDatatable('archive-farm-sponsor');
      }, error => {
        console.log('Error ', error);
        this.utilService.startDatatable('archive-farm-sponsor');
      });
  }
  public handleFullWidth() {
    console.log('BREADCRUMB');
    const body = $('body');
    if (body.hasClass('layout-fullwidth')) {
      body.removeClass('layout-fullwidth');
      $('#fullWithToggler').removeClass('fa-arrow-right').addClass('fa-arrow-left');
    } else {
      body.addClass('layout-fullwidth');
      $('#fullWithToggler').removeClass('fa-arrow-left').addClass('fa-arrow-right');
    }
  }

  cancelSponsor (sponsor) {
    console.log('CUSTOMER ', this.user);
    this.utilService.confirmAction((res: IResponse) => {
      this.userService.cancelSponsorship(this.user._id, sponsor._id).subscribe(() => {
        this.alertService.success(res.msg || 'Sponsorship canceled successfully!');
      }, error => {
        console.log({error});
        this.alertService.error(error.error.msg || 'Unable to cancel sponsorship!');
      });
    });
  }
  refundSponsor(sponsor) {
    this.alertService.info('This is not possible at the moment!');
  }

  viewROI(data) {
    this.roiData = data;
    this.utilService.openModal('foodTrading');
  }
  closeTradingModal() {
    this.utilService.closeModal('foodTrading');
    this.roiData = [];
  }
}
