import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperViewUserDashboardComponent } from './super-view-user-dashboard.component';

describe('SuperViewUserDashboardComponent', () => {
  let component: SuperViewUserDashboardComponent;
  let fixture: ComponentFixture<SuperViewUserDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperViewUserDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperViewUserDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
