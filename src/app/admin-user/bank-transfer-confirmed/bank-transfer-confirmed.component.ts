import { Component, OnInit } from '@angular/core';
import {IResponse} from '../../interfaces/iresponse';
import {SuperAdminService} from '../../services/api-handlers/superAdminService/super-admin.service';
import {UtilService} from '../../services/utilService/util.service';
import {BootstrapNotifyService} from "../../services/bootstrap-notify/bootstrap-notify.service";

@Component({
  selector: 'app-bank-transfer-confirmed',
  templateUrl: './bank-transfer-confirmed.component.html',
  styleUrls: ['./bank-transfer-confirmed.component.css']
})
export class BankTransferConfirmedComponent implements OnInit {
  breadCrumb: any;

  loaders = {
    saving: false
  };
  bankTransfer: any [] = [];
  documents: any [] = [];
  newCustomer =  {
    name: null,
    status: true
  };
  public activeCustomer = null;
  public loadingData = false;
  public modal = {
    title: 'Add New Customer',
    btnText: 'SAVE'
  };
  showData = true;

  constructor(private superAdminService: SuperAdminService,
              private alertService: BootstrapNotifyService,
              private utilService: UtilService) { }

  ngOnInit() {
    this.breadCrumb  = {
      name: 'Old Bank Transfers',
      parent: 'Home',
      subLink: null
    };
    this.getBankPayments();
  }

  getBankPayments() {
    this.showData = false;
    this.loadingData = true;
    this.superAdminService.getConfirmedBankPayment()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.bankTransfer = res.data;
        this.utilService.startDatatable('all-bank-transfer');
        this.loadingData = false;
        this.showData = true;
      }, error => {
        console.log('Error ', error);
        this.utilService.startDatatable('all-bank-transfer');
        this.loadingData = false;
        this.showData = true;
      });
  }
  confirmPayment(payment) {
    this.superAdminService.confirmBankTransfer({transactionId: payment.transactionId._id})
      .subscribe((res: IResponse) => {
        this.alertService.success(res.msg || 'Payment confirmed successfully');
        this.getBankPayments();
        console.log('Res ', res);
      }, error => {
        console.log('Error ', error);
        this.alertService.error(error.error.msg || 'Unable to confirm payment!');
      });
  }
  confirmWalletFunding(payment) {
    this.superAdminService.confirmBankTransferWalletFunding({transactionId: payment.transactionId._id})
      .subscribe((res: IResponse) => {
        this.alertService.success(res.msg || 'Payment for wallet funding confirmed');
        this.getBankPayments();
        console.log('Res ', res);
      }, error => {
        console.log('Error ', error);
        this.alertService.error(error.error.msg || 'Unable to confirm payment!');
      });
  }

  cancelTransaction(payment) {
    this.superAdminService.cancelConfirmationProof({transactionId: payment.transactionId._id})
      .subscribe((res: IResponse) => {
        this.alertService.success(res.msg || 'Transaction canceled!');
        this.getBankPayments();
        console.log('Res ', res);
      }, error => {
        console.log('Error ', error);
        this.alertService.error(error.error.msg || 'Unable to cancel transaction!');
      });
  }

  reInitTransaction(payment) {
    this.superAdminService.reInitCancelProof({transactionId: payment.transactionId._id})
      .subscribe((res: IResponse) => {
        this.alertService.success(res.msg || 'Transaction re-initiated!');
        this.getBankPayments();
        console.log('Res ', res);
      }, error => {
        console.log('Error ', error);
        this.alertService.error(error.error.msg || 'Unable to re-initiate transaction!');
      });
  }

  viewDocuments(payment) {
    this.documents = payment.documents;
    this.utilService.openModal('viewDocument');
  }

}
