import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankTransferConfirmedComponent } from './bank-transfer-confirmed.component';

describe('BankTransferConfirmedComponent', () => {
  let component: BankTransferConfirmedComponent;
  let fixture: ComponentFixture<BankTransferConfirmedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankTransferConfirmedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankTransferConfirmedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
