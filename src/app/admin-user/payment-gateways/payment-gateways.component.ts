import { Component, OnInit } from '@angular/core';
import {UtilService} from '../../services/utilService/util.service';
import {IResponse} from '../../interfaces/iresponse';
import {BootstrapNotifyService} from '../../services/bootstrap-notify/bootstrap-notify.service';
import {SuperAdminService} from '../../services/api-handlers/superAdminService/super-admin.service';

@Component({
  selector: 'app-payment-gateways',
  templateUrl: './payment-gateways.component.html',
  styleUrls: ['./payment-gateways.component.css']
})
export class PaymentGatewaysComponent implements OnInit {
  breadCrumb: any;
  showData = false;
  public paymentChannels: any[] = [];
  loadingData = false;
  loaders = {
    saving: false
  };
  modal = {
    title: 'Create Payment Channel',
    btnText: 'SAVE'
  };
  activeChannel = null;
  channel =  {
    name: null,
    description: null
  };
  constructor(private utilService: UtilService,
              private bootstrapNotify: BootstrapNotifyService,
              private superAdminService: SuperAdminService) { }

  ngOnInit() {
    this.breadCrumb  = {
      name: 'Payment Channel',
      parent: 'Settings',
      subLink: null
    };
    this.getPaymentChannel();
  }

  getPaymentChannel() {
    this.loadingData = true;
    this.showData = false;
    this.paymentChannels = [];
    this.superAdminService.getPaymentChannels()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.paymentChannels = res.data;
        this.utilService.neutralDatatable('all-channels', false);
        this.loadingData = false;
        this.showData = true;
      }, error => {
        console.log('Error ', error);
        this.utilService.startDatatable('all-channels');
        this.loadingData = false;
        this.showData = true;
      });
  }


  openDialog(id) {
    this.modal = {
      title: 'Create Payment Channel',
      btnText: 'SAVE'
    };
    this.activeChannel = null;
    this.resetForm();
    this.utilService.openModal(id);
  }

  public deleteChannel(channel) {
    this.utilService.confirmAction(() => {
      this.superAdminService.deletePaymentChannel(channel._id)
        .subscribe((res: IResponse) => {
          console.log('Res ', res);
          this.getPaymentChannel();
        }, error => {
          console.log('Errro ', error);

        });
    });
  }
  public triggerEdit(channel) {
    this.activeChannel = JSON.parse(JSON.stringify(channel));
    this.resetForm();
    this.utilService.openModal('createChannel');
    this.channel = this.activeChannel;
    this.modal = {
      title: 'Update Payment Channel',
      btnText: 'UPDATE'
    };
  }

  public resetForm() {
    this.channel =  {
      name: null,
      description: null
    };
  }
  public createChannel() {
    console.log('Channel ', this.channel);
    this.loaders.saving = true;
    if (!this.channel.name) {
      this.bootstrapNotify.info('Payment Channel name field is Required');
      this.loaders.saving = false;
    } else if (!this.channel.description) {
      this.bootstrapNotify.info('Payment Channel description field is Required');
      this.loaders.saving = false;
    } else {
      if (this.activeChannel) {
        this.updateChannel();
      } else {
        this.superAdminService.createPaymentChannel(this.channel).subscribe((response: IResponse) => {
          console.log('Response', response);
          this.loaders.saving = false;
          this.resetForm();
          this.getPaymentChannel();
          this.utilService.closeModal('createChannel');
        }, error => {
          this.bootstrapNotify.error(error.error.message || 'Unable to create payment channel!', 'right');
          this.loaders.saving = false;
          console.info('Error => ', error);
        });
      }
    }

  }
  public updateChannel() {
    this.superAdminService.updatePaymentChannel(this.channel, this.activeChannel._id).subscribe((response: IResponse) => {
      console.log('Response', response);
      this.loaders.saving = false;
      this.resetForm();
      this.getPaymentChannel();
      this.utilService.closeModal('createChannel');
    }, error => {
      this.bootstrapNotify.error(error.error.message || 'Unable to update payment channel!', 'right');
      this.loaders.saving = false;
      console.info('Error => ', error);
    });
  }
  public makeChannel(status, channel) {
    this.superAdminService.updatePaymentChannel({is_active: status}, channel._id).subscribe((response: IResponse) => {
      console.log('Response', response);
      this.getPaymentChannel();
    }, error => {
      this.bootstrapNotify.error(error.error.message || 'Unable to update payment channel!', 'right');
      console.info('Error => ', error);
    });
  }
  public toggleCanFund(status, channel) {
    this.superAdminService.updatePaymentChannel({can_fund: !status}, channel._id).subscribe((response: IResponse) => {
      console.log('Response', response);
      this.getPaymentChannel();
    }, error => {
      this.bootstrapNotify.error(error.error.message || 'Unable to update payment channel!', 'right');
      console.info('Error => ', error);
    });
  }
}
