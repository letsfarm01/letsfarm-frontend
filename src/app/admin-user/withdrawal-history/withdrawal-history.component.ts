import { Component, OnInit } from '@angular/core';
import {NavigatorService} from '../../services/navigatorService/navigator.service';
import {IResponse} from '../../interfaces/iresponse';
import {UtilService} from '../../services/utilService/util.service';
import {BootstrapNotifyService} from '../../services/bootstrap-notify/bootstrap-notify.service';
import {SuperAdminService} from '../../services/api-handlers/superAdminService/super-admin.service';

@Component({
  selector: 'app-withdrawal-history',
  templateUrl: './withdrawal-history.component.html',
  styleUrls: ['./withdrawal-history.component.css']
})
export class WithdrawalHistoryComponent implements OnInit {
  breadCrumb: any;
  withdrawalHistory: any[] = [];
  showData = true;
  showData2 = true;
  public loadingData = false;
  currentUser = null;
  constructor(private navigatorService: NavigatorService,
              private alertService: BootstrapNotifyService,
              private superAdminService: SuperAdminService, private utilService: UtilService) { }

  ngOnInit() {
    this.currentUser = this.utilService.getAuthUser();
    this.breadCrumb  = {
      name: 'Withdrawal-History',
      parent: 'Fund',
      subLink: null
    };
    this.getWithdrawHistory();
  }

  getWithdrawHistory() {
    this.showData = false;
    this.loadingData = true;
    this.superAdminService.getWithdrawalHistory()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.withdrawalHistory = res.data;
        this.loadingData = false;
        this.showData = true;
        this.utilService.neutralDatatable('all-withdrawal-history', true);
      }, error => {
        console.log('Error ', error);
        this.loadingData = false;
        this.showData = true;
        this.utilService.neutralDatatable('all-withdrawal-history', true);

      });
  }
}
