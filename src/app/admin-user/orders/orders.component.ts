import { Component, OnInit } from '@angular/core';
import {IResponse} from '../../interfaces/iresponse';
import {SuperAdminService} from '../../services/api-handlers/superAdminService/super-admin.service';
import {UtilService} from '../../services/utilService/util.service';
import {UserService} from '../../services/api-handlers/userService/user.service';
import {NavigatorService} from '../../services/navigatorService/navigator.service';
import {NotificationService} from "../../services/notificationServices/notification.service";

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  breadCrumb: any;
  orders: any[] = [];
  showData = true;
  allStatus = [{name: 'ALL', value: 'ALL'}, {name: 'PROCESSING', value: 'PROCESSING'},
    {name: 'PENDING PAYMENT', value: 'PENDING_PAYMENT'},
    {name: 'PAID', value: 'PAID'}, {name: 'FAILED', value: 'FAILED'},
    {name: 'SHIPPED', value: 'SHIPPED'}, {name: 'DELIVERED', value: 'DELIVERED'}];
  selected_status = 'ALL';
  public loadingData = false;
  currentUser = null;

  constructor(private navigatorService: NavigatorService,
              private superadminService: SuperAdminService,
              private alertService: NotificationService,
              private userService: UserService, private utilService: UtilService) {
  }

  ngOnInit() {
    this.getOrders();
    this.breadCrumb = {
      name: 'Orders',
      parent: 'Farm Products',
      subLink: null
    };
  }

  gotoURL(url) {
    this.navigatorService.navigateUrl(url);
  }

  getOrders(selected_status = 'ALL') {
    this.showData = false;
    this.loadingData = true;
    this.superadminService.getProductOrders(selected_status)
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.orders = res.data.data;
        this.utilService.startDatatable('product-order-list');
        this.loadingData = false;
        this.showData = true;
      }, error => {
        console.log('Error ', error);
        this.utilService.startDatatable('product-order-list');
        this.loadingData = false;
        this.showData = true;
      });
  }
  setOrderStatus(order, action) {
    this.superadminService.setProductOrderStatus({status: action}, order._id)
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.getOrders()
        this.alertService.success('Order updated successfully')
      }, error => {
        this.alertService.error('Unable to update order status!');
        console.log('Error ', error);
      });
  }
  filterOrdersByStatus(event) {
    console.log('EVENT ', event);
    if (!event) { return false; }
    this.selected_status = event.value;
    this.getOrders(this.selected_status);
  }
}
