import { Component, OnInit } from '@angular/core';
import {SuperAdminService} from "../../services/api-handlers/superAdminService/super-admin.service";
import {IResponse} from "../../interfaces/iresponse";
import {BootstrapNotifyService} from "../../services/bootstrap-notify/bootstrap-notify.service";
import * as moment from 'moment';

@Component({
  selector: 'app-manage-payout',
  templateUrl: './manage-payout.component.html',
  styleUrls: ['./manage-payout.component.css']
})
export class ManagePayoutComponent implements OnInit {
  breadCrumb: any;
  years = [];
  repays = [];
  activeYear;
  loading = true;
  pauseUI = false;
  activePayoutRepay = {
    status: null,
    commencement_date: new Date()
  };
  saving = false;
  activePayout = null;
  constructor(private superadminService: SuperAdminService,
              private alertService: BootstrapNotifyService) {
    this.getCurrentYear();
  }

  ngOnInit() {
    this.initPicker();
    this.breadCrumb  = {
      name: 'Repay',
      parent: 'Payout',
      subLink: null
    };
    this.getPayouts();
  }
  public getPayouts() {
    this.loading = true;
    this.superadminService.getPayouts().subscribe((res:IResponse) => {
      console.log('REs ', res);
      this.loading = false;
      this.repays = res.data;
      this.repays.forEach((repay) => {
        repay._commencement_date  = moment(repay.commencement_date).format('MMM DD YYYY')
      });
    }, error => {
      console.log('Error', error);
      this.alertService.error("Unable to list payout for current year");
      this.loading = false;
    })
  }
  public getPayoutsByYear() {
    this.superadminService.getPayoutByYear(this.activeYear).subscribe((res:IResponse) => {
      console.log('REs by year', res);
      this.loading = false;
      this.repays = res.data;
      this.repays.forEach((repay) => {
        repay._commencement_date  = moment(repay.commencement_date).format('MMM DD YYYY')
      });
    }, error => {
      console.log('Error', error);
      this.alertService.error("Unable to list payout for selected year");
      this.loading = false;
    })
  }

  private setYears() {
    const year = this.activeYear;
    let i = 0;
    while(i < 2) {
      this.years.push(year + i);
      i++;
    }
  }
  private getCurrentYear() {
    this.superadminService.getCurrentYear().subscribe((res: IResponse) => {
      this.activeYear = res.data;
      this.setYears();
    }, () => {
      this.activeYear =  new Date().getFullYear();
      this.setYears();
    })
  }
  public switchRepay() {
    console.log('Active ', this.activeYear);
    this.loading = true;
    this.getPayoutsByYear();
  }
  public generateRepay() {
    this.pauseUI = true;
    this.superadminService.generateRepay({year: this.activeYear}).subscribe((res: IResponse) => {
      this.pauseUI = false;
      this.alertService.success(res.msg || 'Repay generated successfully for ' + this.activeYear);
      this.switchRepay();
    }, error => {
      this.pauseUI = false;
      this.alertService.error(error.error.msg || 'Unable to generate repay for ' + this.activeYear)
    });
  }

  initPicker() {
    setTimeout(() => {
      (<any>$('.datepicker')).datetimepicker({
        icons: {
          time: 'fa fa-clock-o',
          date: 'fa fa-calendar',
          up: 'fa fa-chevron-up',
          down: 'fa fa-chevron-down',
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-screenshot',
          clear: 'fa fa-trash',
          close: 'fa fa-remove'
        },
        format: 'YYYY-MM-DD',
        minDate: new Date()
      });
    }, 400);
  }
  public updateField(pos) {
    setTimeout(() => {
      const val: any = $('#commencement_date_' + pos).val();
      this.activePayoutRepay.commencement_date = val;
      console.log('Val ', this.activePayoutRepay.commencement_date, val)
    }, 100);
  }
  public updateRepay(repay, index) {
    this.activePayout = JSON.parse(JSON.stringify(repay));
    this.initPicker();
    console.log('repay ', repay, this.activePayout);
    this.activePayoutRepay = {
      status: this.activePayout.status,
      commencement_date: repay.commencement_date
    };
    setTimeout(() => {
      $('.updater').addClass('d-none');
      $('#payout_repay_' + index).removeClass('d-none');
    }, 100);
  }
  public saveRepayUpdate(repay, i) {
    if(this.activePayoutRepay.status && this.activePayoutRepay.status === 'PENDING' && !this.activePayoutRepay.commencement_date) {
      return this.alertService.info('Commencement date is required for pending payouts.')
    }
    this.saving = true;
    this.superadminService.updatePayoutRepay(this.activePayoutRepay, repay._id).subscribe((res: IResponse) => {
      this.switchRepay();
      console.log('Res ', res);
      this.saving = false;
      this.cancelUpdate(i);
    }, error => {
      this.saving = false;
      this.alertService.error(error.error.msg || 'Unable to update repay!!');
    });
  }
  public cancelUpdate(index) {
    $('#payout_repay_' + index).addClass('d-none');
    this.activePayoutRepay = {
      status: null,
      commencement_date: new Date()
    };
    this.activePayout = null;
    $('.updater').removeClass('d-none');
  }
}
