import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagePayoutComponent } from './manage-payout.component';

describe('ManagePayoutComponent', () => {
  let component: ManagePayoutComponent;
  let fixture: ComponentFixture<ManagePayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagePayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagePayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
