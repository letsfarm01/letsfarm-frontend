import { Component, OnInit } from '@angular/core';
import {IResponse} from '../../interfaces/iresponse';
import {SuperAdminService} from '../../services/api-handlers/superAdminService/super-admin.service';
import {UtilService} from '../../services/utilService/util.service';
import {BootstrapNotifyService} from '../../services/bootstrap-notify/bootstrap-notify.service';


@Component({
  selector: 'app-bank-transaction',
  templateUrl: './bank-transaction.component.html',
  styleUrls: ['./bank-transaction.component.css']
})
export class BankTransactionComponent implements OnInit {
  breadCrumb: any;
  orderDetail = null;
  initiator = null;
  loaders = {
    saving: false,
    loading: false
  };
  transactions: any [] = [];
  newCustomer =  {
    name: null,
    status: true
  };
  public activeCustomer = null;
  public loadingData = false;
  public modal = {
    title: 'Add New Customer',
    btnText: 'SAVE'
  };
  showData = true;

  constructor(private superAdminService: SuperAdminService,
              private alertService: BootstrapNotifyService,
              private utilService: UtilService) { }

  ngOnInit() {
    this.getTransactions();
    this.breadCrumb  = {
      name: 'Bank Transactions',
      parent: 'Report',
      subLink: null
    };
  }

  getTransactions() {
    this.showData = false;
    this.loadingData = true;
    this.superAdminService.getBankTransactions()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.transactions = res.data.data;
        this.utilService.startDatatable('transaction-list');
        this.loadingData = false;
        this.showData = true;
      }, error => {
        console.log('Error ', error);
        this.utilService.startDatatable('transaction-list');
        this.loadingData = false;
        this.showData = true;
      });
  }

  public viewDetails(transaction, i) {
    this.loaders.loading = false;
    $(`#btn-${i}`).addClass('d-none');
    $('.loaders-btn').addClass('d-none');
    $(`#load-${i}`).removeClass('d-none');
    this.alertService.info('Loading transaction details, please wait!');
    this.superAdminService.getOrdersByRef(transaction.orderReference).subscribe((res: IResponse) => {
      console.log('Response ', res);
      this.loaders.loading = true;
      this.orderDetail = res.data;
      $(`#btn-${i}`).removeClass('d-none');
      $(`#load-${i}`).addClass('d-none');
      this.utilService.openModal('transactionDetails');
    }, error => {
      console.log('Error ', error);
      this.alertService.error('Unable to fetch transaction detail');
      $(`#btn-${i}`).removeClass('d-none');
      $(`#load-${i}`).addClass('d-none');
    });
  }
  viewInitiator(transaction) {
    this.initiator = transaction.userId;
    this.utilService.openModal('initiatorModal');
    this.superAdminService.getUserBank(transaction.userId._id).subscribe((res: IResponse) => {
      this.initiator.bank = res.data || null;
      console.log('this.initiator ', this.initiator );
    }, e => {
      this.initiator.bank = null;
    });
  }
  verifyManually(transaction) {
    this.alertService.info('Verification in progress');
    this.superAdminService.verifyPaystackManually({transactionId: transaction._id}).subscribe((res: IResponse) => {
      this.getTransactions();
      this.alertService.success(res.msg);
    }, error => {
      console.log('Erro ', error);
      this.alertService.error(error.error.msg);
    });
  }
}
