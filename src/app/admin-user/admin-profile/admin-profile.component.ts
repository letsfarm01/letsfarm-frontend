import { Component, OnInit } from '@angular/core';
import {UtilService} from '../../services/utilService/util.service';

@Component({
  selector: 'app-admin-profile',
  templateUrl: './admin-profile.component.html',
  styleUrls: ['./admin-profile.component.css']
})
export class AdminProfileComponent implements OnInit {
  currentUser = null;
  breadCrumb: any;
  basic_info: any;
  password = {
    current_password: null,
    new_password: null,
    confirm_new_password: null
  };
  business_info = {
    company_name: null,
    company_address: null,
    company_phone_number: null,
    company_email: null
  };
  bank_info = {
    account_name: null,
    account_number: null,
    bvn: null,
    bank_name: null,
  };
  kin_info = {
    full_name: null,
    address: null,
    email: null,
    relationship: null,
    phone_number: null
  };
  constructor(private utilService: UtilService) { }

  ngOnInit() {
    this.currentUser = this.utilService.getAuthUser();
    this.basic_info = this.currentUser;
    this.breadCrumb  = {
      name: 'Profile',
      parent: 'Settings',
      subLink: null
    };
    console.log('currentUser ', this.currentUser);
  }

}
