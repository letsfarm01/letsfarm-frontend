import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaystackPaymentsComponent } from './paystack-payments.component';

describe('PaystackPaymentsComponent', () => {
  let component: PaystackPaymentsComponent;
  let fixture: ComponentFixture<PaystackPaymentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaystackPaymentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaystackPaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
