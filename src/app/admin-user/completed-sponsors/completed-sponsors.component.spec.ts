import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompletedSponsorsComponent } from './completed-sponsors.component';

describe('CompletedSponsorsComponent', () => {
  let component: CompletedSponsorsComponent;
  let fixture: ComponentFixture<CompletedSponsorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompletedSponsorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletedSponsorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
