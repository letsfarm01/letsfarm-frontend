import { Component, OnInit } from '@angular/core';
import {NavigatorService} from '../../services/navigatorService/navigator.service';
import {IResponse} from '../../interfaces/iresponse';
import {UtilService} from '../../services/utilService/util.service';
import {BootstrapNotifyService} from '../../services/bootstrap-notify/bootstrap-notify.service';
import {SuperAdminService} from '../../services/api-handlers/superAdminService/super-admin.service';
import {configs} from '../../configs/config';
import {EncryptDataService} from "../../services/encryption/encrypt-data.service";

@Component({
  selector: 'app-completed-sponsors',
  templateUrl: './completed-sponsors.component.html',
  styleUrls: ['./completed-sponsors.component.css']
})
export class CompletedSponsorsComponent implements OnInit {
  breadCrumb: any;
  completedSponsorship: any[] = [];
  activeSponsor: any[] = [];
  archiveSponsors: any[] = [];
  roiData: any[] = [];
  showData = true;
  showData2 = true;
  processing = false;
  totalCompleted = 0.0;

  filter = null;

  allMonths = configs.months;
  allYears = configs.years;
  selected_status = 'ALL';
  year = null;
  allStatus = [{name: 'ALL', value: 'ALL'}, {name: 'PENDING', value: 'PENDING'}, {name: 'PAID', value: 'PAID'}];
  public loadingData = false;
  public loadingData2 = false;
  currentUser = null;
  constructor(private navigatorService: NavigatorService,
              private alertService: BootstrapNotifyService,
              private encryptSservice: EncryptDataService,
              private superAdminService: SuperAdminService, private utilService: UtilService) { }

  ngOnInit() {
    this.currentUser = this.utilService.getAuthUser();
    this.breadCrumb  = {
      name: 'Completed Sponsorship',
      parent: 'Fund',
      subLink: null
    };
    this.getSponsorships();
  }
  gotoURL(url) {
    this.navigatorService.navigateUrl(url);
  }

  getSponsorships(filter=this.filter, query=this.selected_status) {
    this.totalCompleted = 0;
    this.showData = false;
    this.loadingData = true;
    // selected_status = this.selected_status
    if(this.selected_status === 'ALL') {
      query = null;// for now, but once we have limit and pages kindly re-modify this.
    }
    this.superAdminService.getCompletedSponsorship(filter, query)
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.completedSponsorship = res.data;
        this.loadingData = false;
        this.showData = true;
        this.utilService.neutralDatatable('all-completed-sponsorship-history', true);
        this.completedSponsorship.forEach((sponsor) => {
          this.totalCompleted += parseFloat(sponsor.roi_amount || 0);
        });
      }, error => {
        console.log('Error ', error);
        this.loadingData = false;
        this.showData = true;
        this.totalCompleted = 0;
        this.utilService.neutralDatatable('all-completed-sponsorship-history', true);

      });
  }

  filterSponsorByMonth(event) {
    console.log('Evenrt ', event);
    if(!event) {
      this.getSponsorships(null);
    } else {
      this.completedSponsorship = [];
      this.filter = {month: event.value};
      this.getSponsorships({month: event.value, year: this.year});
    }
  }

  filterSponsorshipByStatus(event) {
    console.log('EVENT ', event);
    if (!event) { return false; }
    this.selected_status = event.value;
    this.getSponsorships(this.filter, this.selected_status);
  }


  payToWallet(sponsor, i) {
    this.processing = true;
    $('#processing-' + i).removeClass('d-none');
    this.encryptSservice.jsEncryptWithCallback({valid: true, sponsor: sponsor._id}, (result) => {
      console.log('RESULT ', result);
      this.superAdminService.fundUserWalletSingle({token: result}).subscribe((res: IResponse) => {
        console.log('REs ', res);
        $('.processing-').addClass('d-none');
        this.processing = false;
        this.getSponsorships();
      }, (error) => {
        console.log('Error ', error);
        this.processing = false;
        $('.processing-').addClass('d-none');
        this.alertService.error(error.error.msg || 'Unable to transfer money to user wallet');
      });
    });
  }
  checkAll() {
    $('input:checkbox').prop('checked', true);
  }
}
