import { Component, OnInit } from '@angular/core';
import {IResponse} from '../../interfaces/iresponse';
import {UtilService} from '../../services/utilService/util.service';
import {SuperAdminService} from '../../services/api-handlers/superAdminService/super-admin.service';

@Component({
  selector: 'app-paystack-reference',
  templateUrl: './paystack-reference.component.html',
  styleUrls: ['./paystack-reference.component.css']
})
export class PaystackReferenceComponent implements OnInit {
  breadCrumb: any;

  loaders = {
    saving: false
  };
  paystackReferences: any [] = [];
  newCustomer = {
    name: null,
    status: true
  };
  public activeCustomer = null;
  public loadingData = false;
  public modal = {
    title: 'Add New Customer',
    btnText: 'SAVE'
  };
  showData = true;

  constructor(private superAdminService: SuperAdminService,
              private utilService: UtilService) {
  }

  ngOnInit() {
    this.breadCrumb = {
      name: 'Paystack References',
      parent: 'Home',
      subLink: null
    };
    this.getPaystackReferences();
  }

  getPaystackReferences() {
    this.showData = false;
    this.loadingData = true;
    this.superAdminService.getPaystackRef()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.paystackReferences = res.data.data;
        this.utilService.startDatatable('all-paystack-ref');
        this.loadingData = false;
        this.showData = true;
      }, error => {
        console.log('Error ', error);
        this.utilService.startDatatable('all-paystack-ref');
        this.loadingData = false;
        this.showData = true;
      });
  }
}
