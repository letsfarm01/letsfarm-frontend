import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaystackReferenceComponent } from './paystack-reference.component';

describe('PaystackReferenceComponent', () => {
  let component: PaystackReferenceComponent;
  let fixture: ComponentFixture<PaystackReferenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaystackReferenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaystackReferenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
