import { Component, OnInit } from '@angular/core';
import {IResponse} from '../../../interfaces/iresponse';
import {SuperAdminService} from '../../../services/api-handlers/superAdminService/super-admin.service';
import {UtilService} from '../../../services/utilService/util.service';

@Component({
  selector: 'app-newsletter-subscribers',
  templateUrl: './newsletter-subscribers.component.html',
  styleUrls: ['./newsletter-subscribers.component.css']
})
export class NewsletterSubscribersComponent implements OnInit {
  breadCrumb: any;

  loaders = {
    saving: false
  };
  newsletters: any [] = [];
  newCustomer =  {
    name: null,
    status: true
  };
  public activeCustomer = null;
  public loadingData = false;
  public modal = {
    title: 'Add New Customer',
    btnText: 'SAVE'
  };
  showData = true;

  constructor(private superAdminService: SuperAdminService,
              private utilService: UtilService) { }

  ngOnInit() {
    this.breadCrumb  = {
      name: 'Newsletters',
      parent: 'Web-Content',
      subLink: null
    };
    this.getNewsletters();
  }

  getNewsletters() {
    this.showData = false;
    this.loadingData = true;
    this.superAdminService.getNewsletter()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.newsletters = res.data.data;
        this.utilService.startDatatable('all-newsletters');
        this.loadingData = false;
        this.showData = true;
      }, error => {
        console.log('Error ', error);
        this.utilService.startDatatable('all-newsletters');
        this.loadingData = false;
        this.showData = true;
      });
  }

}
