import { Component, OnInit } from '@angular/core';
import {UtilService} from "../../../services/utilService/util.service";
import {SuperAdminService} from "../../../services/api-handlers/superAdminService/super-admin.service";
import {IResponse} from "../../../interfaces/iresponse";
import {BootstrapNotifyService} from "../../../services/bootstrap-notify/bootstrap-notify.service";
import {DeleteService} from "../../../services/firebase-delete/delete.service";

@Component({
  selector: 'app-manage-team',
  templateUrl: './manage-team.component.html',
  styleUrls: ['./manage-team.component.css']
})
export class ManageTeamComponent implements OnInit {
  breadCrumb: any;
  group = {
    name: null,
    description: null,
    sort_order: 1
  };
  loaders = {
    saving: false,
    loadingImg: false
  };
  editorDiv = false;
  activeGroup = null;
  modal = {
    title: 'New Group',
    btnTxt: 'Save'
  };
  memberModal = {
    title: 'New Member',
    btnTxt: 'ADD'
  };
  member = {
    name: null,
    email: null,
    image: null,
    about: null,
    position: null,
    twitter: null,
    linkedin: null,
    teamGroup: null,
    teamGroupName: null,
  };
  activeMember = null;
  teamGroups = [];
  teamMembers = {};
  loadingData = true;
  constructor(private utilService: UtilService,
              private alertService: BootstrapNotifyService,
              private firebaseDeleteService: DeleteService,
              private superadminService: SuperAdminService) { }

  ngOnInit() {
    this.breadCrumb  = {
      name: 'Team Management',
      parent: 'Web-content',
      subLink: null
    };
    this.getGroups();
  }
  getGroups() {
    this.loadingData = true;
    this.superadminService.getTeamMembers()
      .subscribe((res: IResponse) => {
      console.log('RES ', res);
        this.loadingData = false;
        this.teamGroups = res.data.teamGroups;
        this.teamMembers = res.data.result;
      }, error => {
      this.alertService.error(error.error.msg || 'Unable to list team groups');
      this.loadingData = false;
      console.log({error});
      });
  }
  createNewGroup() {
    console.log('GROUP ');
    this.activeGroup = null;
    this.resetGroup();
    this.modal = {title: 'New Group', btnTxt: 'Save'};
    this.utilService.openModal('createGroup');
  }
  updateGroupDialog(group) {
    this.resetGroup();
    this.activeGroup = this.group = JSON.parse(JSON.stringify(group));
    this.modal = {title: 'Update Group', btnTxt: 'Update'};
    this.utilService.openModal('createGroup');
  }
  newGroup() {
    if (!this.group.name) {
      return this.alertService.error('Team group name is required!');
    } else {
      this.loaders.saving = true;
      if (this.activeGroup) {
        this.updateGroup();
      } else {
        this.superadminService.createTeamGroup(this.group)
          .subscribe((res: IResponse) => {
          console.log({res});
          this.alertService.success(res.msg || 'Group created successfully!');
          this.loaders.saving = false;
          this.utilService.closeModal('createGroup');
          this.getGroups();
            this.resetGroup();
          }, error => {
          console.log({error});
            this.loaders.saving = false;
            this.alertService.error(error.error.msg || 'Unable to create group!');
          });
      }
    }
  }
  updateGroup() {
    this.superadminService.updateTeamGroup(this.group, this.activeGroup._id)
      .subscribe((res: IResponse) => {
        console.log({res});
        this.alertService.success(res.msg || 'Group updated successfully!');
        this.loaders.saving = false;
        this.utilService.closeModal('createGroup');
        this.getGroups();
        this.resetGroup();
      }, error => {
        console.log({error});
        this.loaders.saving = false;
        this.alertService.error(error.error.msg || 'Unable to update group!');
      });
  }
  resetGroup() {
    this.group = {
      name: null,
      description: null,
      sort_order: 1
    };
  }
  deleteGroup(group) {
    this.utilService.confirmAction(() => {
      this.superadminService.deleteTeamGroup(group._id)
        .subscribe((res: IResponse) => {
        console.log({res});
        this.getGroups();
        this.alertService.success(res.msg || 'Group deleted successfully!');
      }, error => {
        console.log({error});
        this.alertService.error(error.error.msg || 'Unable to delete group!');
      });
    });
  }
  deleteMember(member) {
    this.utilService.confirmAction(() => {
      this.superadminService.deleteTeamMember(member._id)
        .subscribe((res: IResponse) => {
        console.log({res});
        this.getGroups();
        this.alertService.success(res.msg || 'Member deleted successfully!');
      }, error => {
        console.log({error});
        this.alertService.error(error.error.msg || 'Unable to delete member!');
      });
    });
  }
public resetMember() {
  this.member = {
    name: null,
    email: null,
    image: null,
    about: null,
    position: null,
    twitter: null,
    linkedin: null,
    teamGroup: null,
    teamGroupName: null
  };
}


  public triggerEdit(member) {
    this.editorDiv = false;
    this.resetMember();
    this.activeMember = JSON.parse(JSON.stringify(member));
    this.member = this.activeMember;
    $('#main-table').addClass('d-none');
    this.member.teamGroup = member.teamGroup._id;
    this.member.teamGroupName = member.teamGroup.name;
    this.memberModal = {
      title: 'Update Member',
      btnTxt: 'UPDATE'
    };
    this.openDialog();
  }
  openDialog(group = null) {
    this.editorDiv = true;
    if (group) {
      this.member.teamGroup = group._id;
      this.member.teamGroupName = group.name;
    }
    $('#main-table').addClass('d-none');
  }
  closeEditor() {
    $('.profilepage_1').addClass('animated fadeOutDown');
    setTimeout(() => {
      this.editorDiv = false;
      this.memberModal = {
        title: 'New Member',
        btnTxt: 'ADD'
      };
      this.resetMember();
    }, 500);
    $('#main-table').removeClass('d-none');
  }
  saveTeamMember() {
    /*else if (!this.member.twitter || !this.member.linkedin) {
      return this.alertService.info('Member social links are required');
    } */
    if (!this.member.name) {
      return this.alertService.info('Member name is required');
    } else if (!this.member.position) {
      return this.alertService.info('Member position is required');
    } else if (!this.member.image) {
      return this.alertService.info('Member photo is required');
    } else if (!this.member.about) {
      return this.alertService.info('About member is required');
    } else if (!this.member.teamGroup) {
      return this.alertService.info('Team group are required');
    } else {
      this.loaders.saving = true;
      if (this.activeMember) {
        this.updateMember();
      } else {
        this.superadminService.createTeamMember(this.member).subscribe((res: IResponse) => {
          this.loaders.saving = false;
          this.alertService.success(res.msg || 'Member added to team successfully!');
          console.log('res ', res);
          this.getGroups();
          this.resetMember();
          this.closeEditor();
        }, error => {
          this.alertService.error(error.error.msg || 'Unable to add member to team');
          this.loaders.saving = false;
          console.log({error});
        });
      }
    }
  }
  public updateMember() {
    this.superadminService.updateTeamMember(this.member, this.activeMember._id)
      .subscribe((res: IResponse) => {
      this.loaders.saving = false;
      this.alertService.success(res.msg || 'Member info update to team successfully!');
      console.log('res ', res);
        this.getGroups();
        this.resetMember();
        this.closeEditor();
    }, error => {
      this.alertService.error(error.error.msg || 'Unable to update member');
      this.loaders.saving = false;
      console.log({error});
    });
  }
  public startLoading(event) {
    if (event === 'showLoading') {
      setTimeout(() => {
        this.loaders.loadingImg = false;
      }, 3000);
      this.loaders.loadingImg = true;
    }
  }
  public removeImageAndRestorePicker(itemKey: string, position?: number) {
    console.log('Remove');
    let imageInFireBase;
    if (position) {
      imageInFireBase = this.member[itemKey][position];
      this.member[itemKey][position] = null;
    } else {
      imageInFireBase = this.member[itemKey];
      this.member[itemKey] = null;
    }
    this.deleteFromFireBase(imageInFireBase);
    console.log('Remove', this.member);
  }

  public deleteFromFireBase(file) {
    const imageName = file.split('https://firebasestorage.googleapis.com/v0/b/letsfarm-33131.appspot.com/o/letsfarm_images%2F')[1];
    const image = imageName.split('?alt=');
    console.log('Image ', image);
    this.firebaseDeleteService.deleteImages(image[0]);
  }
}
