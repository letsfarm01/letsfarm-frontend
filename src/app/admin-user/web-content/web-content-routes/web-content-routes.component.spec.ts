import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebContentRoutesComponent } from './web-content-routes.component';

describe('WebContentRoutesComponent', () => {
  let component: WebContentRoutesComponent;
  let fixture: ComponentFixture<WebContentRoutesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebContentRoutesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebContentRoutesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
