import { Component, OnInit } from '@angular/core';
import {IResponse} from '../../../interfaces/iresponse';
import {SuperAdminService} from '../../../services/api-handlers/superAdminService/super-admin.service';
import {UtilService} from '../../../services/utilService/util.service';
import {BootstrapNotifyService} from '../../../services/bootstrap-notify/bootstrap-notify.service';
import {DeleteService} from "../../../services/firebase-delete/delete.service";
import {UploadService} from "../../../services/uploadService/upload.service";

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.css']
})
export class AdminEducationComponent implements OnInit {
  breadCrumb: any;
  editorDiv = false;
  loaders = {
    saving: false,
    loading: false,
    loadingImg: false
  };
  activeEducation = null;
  educations: any [] = [];
  allTypes: any [] = [];
  public activeCategory = null;
  public loadingData = false;
  showData = true;
  newEducation = {
    title: null,
    message_body: "<p>Post Body...</p>",
    author: null,
    img_link: null,
    video_link: null,
    edu_category: null,
    publish: false
  };
  public modal = {
    title: 'Create Education Post',
    btnText: 'SAVE'
  };
  public catModal = {
    title: 'Create Category',
    btnText: 'SAVE'
  };
  newType = {
    name: null
  };

  initEditor = {
    height: 500,
    menubar: true,
    plugins: [
      'advlist autolink lists link image charmap print preview anchor',
      'searchreplace visualblocks code fullscreen',
      'insertdatetime media table paste code help wordcount'
    ],
    images_upload_url: 'https://letsfarm.com.ng',
    images_upload_handler: (blobInfo, success, failure) =>  {
      console.log(blobInfo);
      setTimeout(() => {
        console.log('image=> ', blobInfo);
        this.uploadService.uploadOnEditor(blobInfo.blob(), 'editor_images',(url) => {
          success(url);
        });
      }, 10);
    },
    toolbar:
      'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help'
  };
  constructor(private superAdminService: SuperAdminService,
              private alertService: BootstrapNotifyService,
              private firebaseDeleteService: DeleteService,
              private uploadService: UploadService,
              private utilService: UtilService) {
    this.getEducationTypes();
  }

  ngOnInit() {
    this.breadCrumb = {
      name: 'Education Posts',
      parent: 'Web-Content',
      subLink: null
    };
    this.getEducationPosts();
  }

  getEducationPosts() {
    this.showData = false;
    this.loadingData = true;
    this.superAdminService.getEducationPosts()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.educations = res.data.data;
        this.utilService.startDatatable('all-educations');
        this.loadingData = false;
        this.showData = true;
      }, error => {
        console.log('Error ', error);
        this.utilService.startDatatable('all-educations');
        this.loadingData = false;
        this.showData = true;
      });
  }
  deleteEducation(education) {
    this.utilService.confirmAction(() => {
      this.superAdminService.deleteEducation(education._id).subscribe((res: IResponse) => {
        this.alertService.success(res.msg || 'Post deleted successfully!');
        this.getEducationPosts();
      }, error => {
        this.alertService.error(error.error.msg || 'Unable to delete post!');
      });
    });
  }

  public triggerEdit(fEducation) {
    this.editorDiv = false;
    this.activeEducation = JSON.parse(JSON.stringify(fEducation));
    // console.log('this.activeEducation ', this.activeEducation, fEducation);
    this.resetEducation();
    this.newEducation = this.activeEducation;
    this.newEducation.edu_category = this.activeEducation.edu_category._id;
    // this.newEducation.farm_status = this.activeEducation.farm_status._id;
    console.log('NEW SHOP ', this.newEducation, this.activeEducation);
    $('#main-table').addClass('d-none');
    this.modal = {
      title: 'Update Education',
      btnText: 'UPDATE'
    };
    this.openDialog();
  }
  openDialog() {
    this.editorDiv = true;
    // this.activeEducation = null;
    $('#main-table').addClass('d-none');
    // this.initPicker();
  }
  closeEditor() {
    this.activeEducation = null;
    $('.profilepage_1').addClass('animated fadeOutDown');
    setTimeout(() => {
      this.editorDiv = false;
      this.modal = {
        title: 'Create Education Post',
        btnText: 'SAVE'
      };
      this.resetEducation();
    }, 500);
    $('#main-table').removeClass('d-none');
  }
  public updateEducation() {
    if (this.newEducation.img_link.includes('https://firebasestorage.googleapis.com/v0/b/letsfarm-33131.appspot.com')) {
      this.superAdminService.updateEducation(this.newEducation, this.activeEducation._id).subscribe((res) => {
        console.log('Res ', res);
        this.getEducationPosts();
        this.loaders.saving = false;
        this.resetEducation();
        this.closeEditor();
        this.alertService.success(res.msg || 'Education updated successfully!', 'right');
      }, error => {
        console.log('Error ', error);
        this.alertService.error(error.error.message || 'Unable to update shop!');
        this.loaders.saving = false;
      });
    } else {
      this.alertService.error('Unable to update post, display image not set!');
      this.loaders.saving = false;

    }
  }

  saveEducation() {
    this.loaders.saving = true;
    this.checkError(this.newEducation, () => {
      if (this.activeEducation) {
        this.updateEducation();
      } else {
        if (this.newEducation.img_link.includes('https://firebasestorage.googleapis.com/v0/b/letsfarm-33131.appspot.com')) {
          this.superAdminService.createEducation(this.newEducation).subscribe((res) => {
            console.log('Res ', res);
            this.getEducationPosts();
            this.loaders.saving = false;
            this.resetEducation();
            this.closeEditor();
            this.alertService.success(res.msg || 'Education post added successfully!', 'right');
          }, error => {
            console.log('Error ', error);
            this.alertService.error(error.error.msg || 'Unable to add new post!');
            this.loaders.saving = false;
          });
        } else {
          this.alertService.error('Unable to add new post, display image is not set!');
          this.loaders.saving = false;
        }
      }
    }, ['video_link', 'total_likes', 'total_dislikes', 'comments_count', 'deleted', '_id', '__v', 'publish']);

  }
  public checkError(data, cb, omit= []) {
    const keys = Object.keys(data);
    const resolvedKeys = keys.filter(d => !omit.includes(d));
    console.log('Resolved ', resolvedKeys);
    for (const key of resolvedKeys) {
      console.log('CValue ', data[key], key );
      if (!data[key]) {
        this.loaders.saving = false;
        return this.alertService.error(`${key.toString().toUpperCase().split('_').join(' ')} is Required`);
      }
    }
    cb();
  }
  resetEducation() {
    this.newEducation = {
      title: null,
      message_body: "<p>Post Body...</p>",
      author: null,
      img_link: null,
      video_link: null,
      edu_category: null,
      publish: false
    };
  }
  getEducationTypes() {
    this.loaders.loading = true;
    this.superAdminService.getEducationCategories()
      .subscribe((res: IResponse) => {
        this.allTypes = res.data;
        this.loaders.loading = false;
        this.utilService.startDatatable('educations-categories');
      }, error => {
        console.log('Error ', error);
        this.loaders.loading = false;
        this.utilService.startDatatable('educations-categories');
      });
  }

  public startLoading(event) {
    if (event === 'showLoading') {
      setTimeout(() => {
        this.loaders.loadingImg = false;
      }, 3000);
      this.loaders.loadingImg = true;
    }
  }
  public removeImageAndRestorePicker(itemKey: string, position?: number) {
    console.log('Remove');
    let imageInFireBase;
    if (position) {
      imageInFireBase = this.newEducation[itemKey][position];
      this.newEducation[itemKey][position] = null;
    } else {
      imageInFireBase = this.newEducation[itemKey];
      this.newEducation[itemKey] = null;
    }
    this.deleteFromFireBase(imageInFireBase);
    console.log('Remove', this.newEducation);
  }

  public deleteFromFireBase(file) {
    const imageName = file.split('https://firebasestorage.googleapis.com/v0/b/letsfarm-33131.appspot.com/o/letsfarm_images%2F')[1];
    const image = imageName.split('?alt=');
    console.log('Image ', image);
    this.firebaseDeleteService.deleteImages(image[0]);
  }
  public getUploadEvent(e) {
    console.log('Event ', e);
    // this.newEducation.display_img = e.display_img;
  }
  createCategory() {
    this.catModal = {
      title: 'Create Category',
      btnText: 'SAVE'
    };
    this.utilService.openModal('typeModal');
    this.activeCategory = null;
  }
  saveType() {
    if (!this.newType.name) {
      return this.alertService.error('Category name is required!');
    } else {
      this.loaders.saving = true;
      if (this.activeCategory) {
        this.updateCategory();
      } else {
        this.superAdminService.createEducationCategory(this.newType).subscribe(( res: IResponse) => {
          this.alertService.success(res.msg || 'Category created successfully!');
          console.log('RES ', res );
          this.loaders.saving = false;
          this.resetType();
          this.getEducationTypes();
        }, error => {
          this.loaders.saving = false;
          console.log('ERROR ', error);
          this.alertService.error(error.error.msg || 'Unable to create category!');
        });
      }
    }
  }
  updateCategory() {
    this.superAdminService.updateEducationCategory(this.newType, this.activeCategory._id).subscribe(( res: IResponse) => {
      this.alertService.success(res.msg || 'Category updated successfully!');
      console.log('RES ', res );
      this.loaders.saving = false;
      this.resetType();
      this.getEducationTypes();
    }, error => {
      this.loaders.saving = false;
      console.log('ERROR ', error);
      this.alertService.error(error.error.msg || 'Unable to update category!');
    });
  }
  editCategory(category) {
    this.resetType();
    this.activeCategory = JSON.parse(JSON.stringify(category));
    this.catModal = {
      title: 'Update Category',
      btnText: 'UPDATE'
    };
    this.newType = this.activeCategory;
    this.utilService.openModal('typeModal');
  }
  resetType() {
    this.utilService.closeModal('typeModal');
    this.newType = {
      name: null
    };
  }
  deleteCategory(type) {
    this.utilService.confirmAction(() => {
      this.superAdminService.deleteEducationType(type._id)
        .subscribe((res: IResponse) => {
          console.log('Res ', res);
          this.getEducationTypes();
        }, error => {
          console.log('Errro ', error);
        });
    });
  }

  triggerPublish(education, value) {
    this.superAdminService.updateEducation({publish: value}, education._id).subscribe((res) => {
      console.log('Res ', res);
      this.getEducationPosts();
      this.alertService.success(res.msg || 'Blog updated successfully!', 'right');
    }, error => {
      console.log('Error ', error);
      this.alertService.error(error.error.message || 'Unable to update blog!');
      this.loaders.saving = false;
    });
  }

}
