import { Component, OnInit } from '@angular/core';
import {IResponse} from '../../../interfaces/iresponse';
import {SuperAdminService} from '../../../services/api-handlers/superAdminService/super-admin.service';
import {UtilService} from '../../../services/utilService/util.service';
import {BootstrapNotifyService} from '../../../services/bootstrap-notify/bootstrap-notify.service';
import {DeleteService} from '../../../services/firebase-delete/delete.service';
import {UploadService} from "../../../services/uploadService/upload.service";

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class AdminBlogComponent implements OnInit {
  breadCrumb: any;
  editorDiv = false;
  loaders = {
    saving: false,
    loading: false,
    loadingImg: false
  };
  activeBlog = null;
  blogs: any [] = [];
  allTypes: any [] = [];
  public activeCategory = null;
  public loadingData = false;
  showData = true;
  newBlog = {
    title: null,
    author: null,
    img_link: null,
    large_img_link: null,
    // video_link: null,
    blog_category: null,
    message_body: "<p>Post Body...</p>",
    publish: false


  };
  initEditor = {
    height: 500,
    menubar: true,
    plugins: [
      'advlist autolink lists link image charmap print preview anchor',
      'searchreplace visualblocks code fullscreen',
      'insertdatetime media table paste code help wordcount'
    ],
    images_upload_url: 'https://letsfarm.com.ng',
    images_upload_handler: (blobInfo, success, failure) => {
      console.log(blobInfo);
      setTimeout( () => {
        console.log('image=> ', blobInfo);
        this.uploadService.uploadOnEditor(blobInfo.blob(), 'editor_images',(url) => {
          success(url);
        });
      }, 10);
    },
    toolbar:
      'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help'
  };
  public modal = {
    title: 'Create Blog Post',
    btnText: 'SAVE'
  };
  public catModal = {
    title: 'Create Category',
    btnText: 'SAVE'
  };
  newType = {
    name: null
  };
  constructor(private superAdminService: SuperAdminService,
              private alertService: BootstrapNotifyService,
              private firebaseDeleteService: DeleteService,
              private uploadService: UploadService,
              private utilService: UtilService) {
    this.getBlogTypes();
  }

  ngOnInit() {
    this.breadCrumb = {
      name: 'Blog Posts',
      parent: 'Web-Content',
      subLink: null
    };
    this.getBlogPosts();
  }

  getBlogPosts() {
    this.showData = false;
    this.loadingData = true;
    this.superAdminService.getBlogPosts()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.blogs = res.data.data;
        this.utilService.startDatatable('all-blogs');
        this.loadingData = false;
        this.showData = true;
      }, error => {
        console.log('Error ', error);
        this.utilService.startDatatable('all-blogs');
        this.loadingData = false;
        this.showData = true;
      });
  }
  deleteBlog(blog) {
    this.utilService.confirmAction(() => {
      this.superAdminService.deleteBlog(blog._id).subscribe((res: IResponse) => {
        this.alertService.success(res.msg || 'Post deleted successfully!');
        this.getBlogPosts();
      }, error => {
        this.alertService.error(error.error.msg || 'Unable to delete post!');
      });
    });
  }

  public triggerEdit(fBlog) {
    this.editorDiv = false;
    this.activeBlog = JSON.parse(JSON.stringify(fBlog));
    // console.log('this.activeBlog ', this.activeBlog, fBlog);
    this.resetBlog();
    this.newBlog = this.activeBlog;
    this.newBlog.blog_category = this.activeBlog.blog_category._id;
    // this.newBlog.farm_status = this.activeBlog.farm_status._id;
    console.log('NEW BLOG ', this.newBlog, this.activeBlog);
    $('#main-table').addClass('d-none');
    this.modal = {
      title: 'Update Blog',
      btnText: 'UPDATE'
    };
    this.openDialog();
  }
  openDialog() {
    this.editorDiv = true;
    // this.activeBlog = null;
    $('#main-table').addClass('d-none');
    // this.initPicker();
  }
  closeEditor() {
    this.activeBlog = null;
    $('.profilepage_1').addClass('animated fadeOutDown');
    setTimeout(() => {
      this.editorDiv = false;
      this.modal = {
        title: 'Create Blog Post',
        btnText: 'SAVE'
      };
      this.resetBlog();
    }, 500);
    $('#main-table').removeClass('d-none');
  }
  public updateBlog() {
    if (this.newBlog.img_link.includes('https://firebasestorage.googleapis.com/v0/b/letsfarm-33131.appspot.com') &&
      this.newBlog.large_img_link.includes('https://firebasestorage.googleapis.com/v0/b/letsfarm-33131.appspot.com')) {
      this.superAdminService.updateBlog(this.newBlog, this.activeBlog._id).subscribe((res) => {
        console.log('Res ', res);
        this.getBlogPosts();
        this.loaders.saving = false;
        this.resetBlog();
        this.closeEditor();
        this.alertService.success(res.msg || 'Blog updated successfully!', 'right');
      }, error => {
        console.log('Error ', error);
        this.alertService.error(error.error.message || 'Unable to update blog!');
        this.loaders.saving = false;
      });
    } else {
      this.alertService.error('Unable to update post, display image not set!');
      this.loaders.saving = false;

    }
  }

  triggerPublish(blog, value) {
    this.superAdminService.updateBlog({publish: value}, blog._id).subscribe((res) => {
      console.log('Res ', res);
      this.getBlogPosts();
      this.alertService.success(res.msg || 'Blog updated successfully!', 'right');
    }, error => {
      console.log('Error ', error);
      this.alertService.error(error.error.message || 'Unable to update blog!');
      this.loaders.saving = false;
    });
  }

  saveBlog() {
    this.loaders.saving = true;
    this.checkError(this.newBlog, () => {
      if (this.activeBlog) {
        this.updateBlog();
      } else {
        if (this.newBlog.img_link.includes('https://firebasestorage.googleapis.com/v0/b/letsfarm-33131.appspot.com') &&
          this.newBlog.large_img_link.includes('https://firebasestorage.googleapis.com/v0/b/letsfarm-33131.appspot.com')) {
          this.superAdminService.createBlog(this.newBlog).subscribe((res) => {
            console.log('Res ', res);
            this.getBlogPosts();
            this.loaders.saving = false;
            this.resetBlog();
            this.closeEditor();
            this.alertService.success(res.msg || 'Blog post added successfully!', 'right');
          }, error => {
            console.log('Error ', error);
            this.alertService.error(error.error.msg || 'Unable to add new post!');
            this.loaders.saving = false;
          });
        } else {
          this.alertService.error('Unable to add new post, display image is not set!');
          this.loaders.saving = false;
        }
      }
    }, ['total_likes', 'total_dislikes', 'comments_count', 'deleted', '_id', '__v', 'publish', 'top_post']);

  }
  public checkError(data, cb, omit= []) {
    const keys = Object.keys(data);
    const resolvedKeys = keys.filter(d => !omit.includes(d));
    console.log('Resolved ', resolvedKeys);
    for (const key of resolvedKeys) {
      console.log('CValue ', data[key], key );
      if (!data[key]) {
        this.loaders.saving = false;
        return this.alertService.error(`${key.toString().toUpperCase().split('_').join(' ')} is Required`);
      }
    }
    cb();
  }
  resetBlog() {
    this.newBlog = {
      title: null,
      author: null,
      img_link: null,
      large_img_link: null,
      // video_link: null,
      blog_category: null,
      message_body: "<p>Post Body...</p>",
      publish: false
    };
  }
  getBlogTypes() {
    this.loaders.loading = true;
    this.superAdminService.getBlogCategories()
      .subscribe((res: IResponse) => {
        this.allTypes = res.data;
        this.loaders.loading = false;
        this.utilService.startDatatable('blogs-categories');
      }, error => {
        console.log('Error ', error);
        this.loaders.loading = false;
        this.utilService.startDatatable('blogs-categories');
      });
  }

  public startLoading(event) {
    if (event === 'showLoading') {
      setTimeout(() => {
        this.loaders.loadingImg = false;
      }, 3000);
      this.loaders.loadingImg = true;
    }
  }
  public removeImageAndRestorePicker(itemKey: string, position?: number) {
    console.log('Remove');
    let imageInFireBase;
    if (position) {
      imageInFireBase = this.newBlog[itemKey][position];
      this.newBlog[itemKey][position] = null;
    } else {
      imageInFireBase = this.newBlog[itemKey];
      this.newBlog[itemKey] = null;
    }
    this.deleteFromFireBase(imageInFireBase);
    console.log('Remove', this.newBlog);
  }

  public deleteFromFireBase(file) {
    const imageName = file.split('https://firebasestorage.googleapis.com/v0/b/letsfarm-33131.appspot.com/o/letsfarm_images%2F')[1];
    const image = imageName.split('?alt=');
    console.log('Image ', image);
    this.firebaseDeleteService.deleteImages(image[0]);
  }
  public getUploadEvent(e) {
    console.log('Event ', e);
    // this.newBlog.display_img = e.display_img;
  }
  createCategory() {
    this.catModal = {
      title: 'Create Category',
      btnText: 'SAVE'
    };
    this.utilService.openModal('typeModal');
    this.activeCategory = null;
  }
  saveType() {
    if (!this.newType.name) {
      return this.alertService.error('Category name is required!');
    } else {
      this.loaders.saving = true;
      if (this.activeCategory) {
        this.updateCategory();
      } else {
        this.superAdminService.createBlogCategory(this.newType).subscribe(( res: IResponse) => {
          this.alertService.success(res.msg || 'Category created successfully!');
          console.log('RES ', res );
          this.loaders.saving = false;
          this.resetType();
          this.getBlogTypes();
        }, error => {
          this.loaders.saving = false;
          console.log('ERROR ', error);
          this.alertService.error(error.error.msg || 'Unable to create category!');
        });
      }
    }
  }
  updateCategory() {
    this.superAdminService.updateBlogCategory(this.newType, this.activeCategory._id).subscribe(( res: IResponse) => {
      this.alertService.success(res.msg || 'Category updated successfully!');
      console.log('RES ', res );
      this.loaders.saving = false;
      this.resetType();
      this.getBlogTypes();
    }, error => {
      this.loaders.saving = false;
      console.log('ERROR ', error);
      this.alertService.error(error.error.msg || 'Unable to update category!');
    });
  }
  editCategory(category) {
    this.resetType();
    this.activeCategory = JSON.parse(JSON.stringify(category));
    this.catModal = {
      title: 'Update Category',
      btnText: 'UPDATE'
    };
    this.newType = this.activeCategory;
    this.utilService.openModal('typeModal');
  }
  resetType() {
    this.utilService.closeModal('typeModal');
    this.newType = {
      name: null
    };
  }
  deleteCategory(type) {
    this.utilService.confirmAction(() => {
      this.superAdminService.deleteBlogType(type._id)
        .subscribe((res: IResponse) => {
          console.log('Res ', res);
          this.getBlogTypes();
        }, error => {
          console.log('Errro ', error);
        });
    });
  }
  makeTop(blog) {
    this.superAdminService.makeTopBlog({id: blog._id}).subscribe((res: IResponse) => {
      console.log('RES ', res);
      this.getBlogPosts();
    }, error => {
      console.log('ERROR ', error);
    });

  }
  // uploadTinyMCEImage
}
