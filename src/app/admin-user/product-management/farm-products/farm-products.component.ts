import { Component, OnInit } from '@angular/core';
import {SuperAdminService} from '../../../services/api-handlers/superAdminService/super-admin.service';
import {UtilService} from '../../../services/utilService/util.service';
import {BootstrapNotifyService} from '../../../services/bootstrap-notify/bootstrap-notify.service';
import {IResponse} from '../../../interfaces/iresponse';
import {EventsService} from '../../../services/eventServices/event.service';
import {isNullOrUndefined} from 'util';
import {Upload} from '../../../models/upload';
import {DeleteService} from '../../../services/firebase-delete/delete.service';
import * as firebase from 'firebase';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';

@Component({
  selector: 'app-farm-products',
  templateUrl: './farm-products.component.html',
  styleUrls: ['./farm-products.component.css']
})
export class AllFarmProductsComponent implements OnInit {
  breadCrumb: any = null;
  loaders = {
    saving: false,
    loadingImg: false
  };
  editorDiv = false;
  allProducts: any [] = [];
  allStatus: any [] = [];
  allTypes: any [] = [];
  newProduct =  {
    product_name: null,
    product_location: null,
    price: 0.0,
    currency: 'NGN',
    remaining_in_stock: 0,
    display_img: null,
    description: null,
    total_in_stock: 1,
    product_type: null,
    product_status: null,
  };
  public activeProduct = null;
  public loadingData = false;
  public modal = {
    title: 'Create Farm Product',
    btnText: 'SAVE'
  };
  showData = true;

  constructor(private superAdminService: SuperAdminService,
              private utilService: UtilService,
              private eventService: EventsService,
              private scrollToService: ScrollToService,
              private firebaseDeleteService: DeleteService,
              private alertService: BootstrapNotifyService) {
  }

  ngOnInit() {
    this.breadCrumb  = {
      name: 'Products',
      parent: 'Farm-Product',
      subLink: null
    };
    this.getFarmProducts();
    // this.initPicker();
    this.getProductStatus();
    this.getProductTypes();
  }
  getFarmProducts() {
    this.showData = false;
    this.loadingData = true;
    this.superAdminService.getFarmProducts()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.allProducts = res.data.data;
        this.utilService.startDatatable('all-farm-products');
        this.loadingData = false;
        this.showData = true;
      }, error => {
        console.log('Error ', error);
        this.utilService.startDatatable('all-farm-products');
        this.loadingData = false;
        this.showData = true;
      });
  }
  public triggerEdit(fProduct) {
    this.editorDiv = false;
    this.activeProduct = JSON.parse(JSON.stringify(fProduct));
    // console.log('this.activeProduct ', this.activeProduct, fProduct);
    this.resetProduct();
    this.newProduct = this.activeProduct;
    this.newProduct.product_type = this.activeProduct.product_type._id;
    this.newProduct.product_status = this.activeProduct.product_status._id;
    /*if (fProduct.farm_type.name.toLowerCase() === 'food trading'
      || fProduct.farm_type.name.toLowerCase() === 'food stuff trading') {
      this.foodStuffEdit = true;
    } else {
      this.foodStuffEdit = false;
      // console.log('ALL TYPES  ', this.allTypes);

      const allTypes = this.allTypes.filter((type) => type.name.toLowerCase() !== 'food stuff trading'
        && type.name.toLowerCase() !== 'food trading');
      console.log('ALL TYES ', allTypes);
      this.allTypes = allTypes;
    }*/
    console.log('NEW PRODUCT ', this.newProduct, this.activeProduct);
    $('#main-table').addClass('d-none');
    this.modal = {
      title: 'Update Farm Product',
      btnText: 'UPDATE'
    };
    this.editorDiv = true;
    $('#main-table').addClass('d-none');
    // this.initPicker();
  }
  openDialog() {
    this.editorDiv = true;
    this.activeProduct = null;
    $('#main-table').addClass('d-none');
  }
  closeEditor() {
    this.getProductTypes();
    $('.profilepage_1').addClass('animated fadeOutDown');
    setTimeout(() => {
      this.editorDiv = false;
      this.activeProduct = null;
      this.modal = {
        title: 'Create Farm Product',
        btnText: 'SAVE'
      };
      this.resetProduct();
    }, 500);
    $('#main-table').removeClass('d-none');
  }
  public updateProduct() {
    // this.alertService.error('EDITOR MODE');
    if (this.newProduct.display_img.includes('https://firebasestorage.googleapis.com/v0/b/letsfarm-33131.appspot.com')) {
      this.superAdminService.updateProduct(this.newProduct, this.activeProduct._id).subscribe((res) => {
        console.log('Res ', res);
        this.getFarmProducts();
        this.loaders.saving = false;
        this.resetProduct();
        this.closeEditor();
        this.alertService.success(res.msg || 'Product updated successfully!', 'right');
      }, error => {
        console.log('Error ', error);
        this.alertService.error(error.error.message || 'Unable to update product!');
        this.loaders.saving = false;
      });
    } else {
      this.alertService.error('Unable to update product, display image not set!');
      this.loaders.saving = false;

    }
  }

  saveProduct() {
    this.loaders.saving = true;
    // console.log('Active ', this.activeProduct);
    this.checkError(this.newProduct, () => {
      if (this.activeProduct) {
        this.updateProduct();
      } else {
        // this.alertService.error('NEW MODE');

        if (this.newProduct.display_img.includes('https://firebasestorage.googleapis.com/v0/b/letsfarm-33131.appspot.com')) {
          this.superAdminService.createProduct(this.newProduct).subscribe((res) => {
            console.log('Res ', res);
            this.getFarmProducts();
            this.loaders.saving = false;
            this.resetProduct();
            this.closeEditor();
            this.alertService.success(res.msg || 'Product added successfully!', 'right');
          }, error => {
            console.log('Error ', error);
            this.alertService.error(error.error.msg || 'Unable to add new product!');
            this.loaders.saving = false;
          });
        } else {
          this.alertService.error('Unable to add new product, display image is not set!');
          this.loaders.saving = false;
        }
      }
    }, ['duration_type', 'currency', 'opening_date', 'closing_date', 'deleted', '_id', '__v']);

  }
  public checkError(data, cb, omit= []) {
    const keys = Object.keys(data);
    const resolvedKeys = keys.filter(d => !omit.includes(d));
    console.log('Resolved ', resolvedKeys);
    for (const key of resolvedKeys) {
      console.log('CValue ', data[key], key );
      if (!data[key]) {
        this.loaders.saving = false;
        return this.alertService.error(`${key.toString().toUpperCase().split('_').join(' ')} is Required`);
      }
    }
    cb();
  }
  resetProduct() {
    this.newProduct =  {
      product_name: null,
      product_location: null,
      price: 0.0,
      currency: 'NGN',
      remaining_in_stock: 0,
      display_img: null,
      description: null,
      total_in_stock: 1,
      product_type: null,
      product_status: null,
    };
  }

  getProductStatus() {
    this.superAdminService.getActiveProductStatus()
      .subscribe((res: IResponse) => {
        this.allStatus = res.data;
      }, error => {
        console.log('Error ', error);
      });
  }

  getProductTypes() {
    this.superAdminService.getActiveProductTypes()
      .subscribe((res: IResponse) => {
        this.allTypes = res.data;
      }, error => {
        console.log('Error ', error);
      });
  }

  public startLoading(event) {
    if (event === 'showLoading') {
      setTimeout(() => {
        this.loaders.loadingImg = false;
      }, 3000);
      this.loaders.loadingImg = true;
    }
  }
  public removeImageAndRestorePicker(itemKey: string, position?: number) {
    console.log('Remove');
    let imageInFireBase;
    if (position) {
      imageInFireBase = this.newProduct[itemKey][position];
      this.newProduct[itemKey][position] = null;
    } else {
      imageInFireBase = this.newProduct[itemKey];
      this.newProduct[itemKey] = null;
    }
    this.deleteFromFireBase(imageInFireBase);
    console.log('Remove', this.newProduct);
  }

  public deleteFromFireBase(file) {
    const imageName = file.split('https://firebasestorage.googleapis.com/v0/b/letsfarm-33131.appspot.com/o/letsfarm_images%2F')[1];
    const image = imageName.split('?alt=');
    console.log('Image ', image);
    this.firebaseDeleteService.deleteImages(image[0]);
  }
  public getUploadEvent(e) {
    console.log('Event ', e);
    // this.newProduct.display_img = e.display_img;
  }
  public updateField(id) {
    setTimeout(() => {
      const val = $('#' + id).val();
      this.newProduct[id] = val;
    }, 1000);
  }
  public validateNumber(key) {
    if (Math.sign(this.newProduct[key]) < 0) {
      this.newProduct[key] = this.newProduct[key] * -1;
      if (!this.activeProduct) {
        this.newProduct.remaining_in_stock = this.newProduct.total_in_stock;
      }
    } else {
      if (!this.activeProduct) {
        this.newProduct.remaining_in_stock = this.newProduct.total_in_stock;
      }
      return;
    }
  }
  deleteProduct(type) {
    this.utilService.confirmAction(() => {
      this.superAdminService.deleteProduct(type._id)
        .subscribe((res: IResponse) => {
          console.log('Res ', res);
          this.getFarmProducts();
        }, error => {
          console.log('Errro ', error);
        });
    });
  }
}
