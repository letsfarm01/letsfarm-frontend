import { Component, OnInit } from '@angular/core';
import {SuperAdminService} from '../../../services/api-handlers/superAdminService/super-admin.service';
import {IResponse} from '../../../interfaces/iresponse';
import {UtilService} from '../../../services/utilService/util.service';
import {BootstrapNotifyService} from '../../../services/bootstrap-notify/bootstrap-notify.service';

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.css']
})
export class FarmProductStatusComponent implements OnInit {
  breadCrumb: any;

  loaders = {
    saving: false
  };
  allStatus: any [] = [];
  newStatus =  {
    name: null,
    status: true
  };
  public activeStatus = null;
  public loadingData = false;
  public modal = {
    title: 'Add New Status',
    btnText: 'SAVE'
  };
  showData = true;

  constructor(private superAdminService: SuperAdminService,
              private utilService: UtilService,
              private alertService: BootstrapNotifyService) { }

  ngOnInit() {
    this.breadCrumb  = {
      name: 'Status',
      parent: 'Farm-Product',
      subLink: null
    };
    this.getFarmStatus();
  }

  getFarmStatus() {
    this.showData = false;
    this.loadingData = true;
    this.superAdminService.getFarmProductStatus()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.allStatus = res.data;
        this.utilService.startDatatable('all-farm-product-status');
        this.loadingData = false;
        this.showData = true;
      }, error => {
        console.log('Error ', error);
        this.utilService.startDatatable('all-farm-product-status');
        this.loadingData = false;
        this.showData = true;
      });
  }

  openDialog(id) {
    this.resetStatus();
    this.utilService.openModal(id);
  }


  public triggerEdit(fStatus) {
    this.activeStatus = JSON.parse(JSON.stringify(fStatus));
    this.resetStatus();
    this.utilService.openModal('productStatusModal');
    this.newStatus = this.activeStatus;
    this.modal = {
      title: 'Update Status',
      btnText: 'UPDATE'
    };
  }
  public updateStatus() {
    if (this.newStatus.name && this.newStatus.status) {
      this.superAdminService.updateProductStatus(this.newStatus, this.activeStatus._id).subscribe((res) => {
        console.log('Res ', res);
        this.getFarmStatus();
        this.utilService.closeModal('productStatusModal');
        this.loaders.saving = false;
        this.resetStatus();
        this.alertService.success(res.msg || 'Status updated successfully!', 'right');

      }, error => {
        console.log('Error ', error);
        this.alertService.error(error.error.message || 'Unable to update status!');
        this.loaders.saving = false;

      });
    } else {
      this.alertService.error('Unable to update status!');
      this.loaders.saving = false;

    }
  }

  saveStatus() {
    this.loaders.saving = true;
    if (this.activeStatus) {
      this.updateStatus();
    } else {
      if (this.newStatus.name && this.newStatus.status) {
        this.superAdminService.createProductStatus(this.newStatus).subscribe((res) => {
          console.log('Res ', res);
          this.getFarmStatus();
          this.loaders.saving = false;
          this.utilService.closeModal('productStatusModal');
          this.resetStatus();
          this.alertService.success(res.msg || 'Status added successfully!', 'right');
        }, error => {
          console.log('Error ', error);
          this.alertService.error(error.error.msg || 'Unable to add new status!');
          this.loaders.saving = false;
        });
      } else {
        this.alertService.error('Unable to add new status!');
        this.loaders.saving = false;
      }
    }

  }
  deleteStatus(status) {
    this.utilService.confirmAction(() => {
      this.superAdminService.deleteProductStatus(status._id)
        .subscribe((res: IResponse) => {
          console.log('Res ', res);
          this.getFarmStatus();
        }, error => {
          console.log('Errro ', error);
        });
    });
  }

  toggleOrder(value, status) {
    this.superAdminService.updateProductStatus({can_order: value}, status._id).subscribe((res) => {
      console.log('Res ', res);
      this.getFarmStatus();
    }, error => {
      console.log('Error ', error);
      this.alertService.error(error.error.message || 'Unable to update product status!');

    });
  }
  resetStatus() {
    this.newStatus =  {
      name: null,
      status: true
    };
  }
}
