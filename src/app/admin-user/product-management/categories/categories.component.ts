import { Component, OnInit } from '@angular/core';
import {SuperAdminService} from '../../../services/api-handlers/superAdminService/super-admin.service';
import {IResponse} from '../../../interfaces/iresponse';
import {UtilService} from '../../../services/utilService/util.service';
import {BootstrapNotifyService} from '../../../services/bootstrap-notify/bootstrap-notify.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class FarmProductCategoriesComponent implements OnInit {
  breadCrumb: any = null;

  loaders = {
    saving: false
  };
  allType: any [] = [];
  newType =  {
    name: null,
    status: true
  };
  public activeType = null;
  public loadingData = false;
  public modal = {
    title: 'Add New Category',
    btnText: 'SAVE'
  };
  showData = true;

  constructor(private superAdminService: SuperAdminService,
              private utilService: UtilService,
              private alertService: BootstrapNotifyService) { }

  ngOnInit() {
    this.breadCrumb  = {
      name: 'Categories',
      parent: 'Farm-Product',
      subLink: null
    };
    this.getFarmType();
  }
  getFarmType() {
    this.showData = false;
    this.loadingData = true;
    this.superAdminService.getProductType()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.allType = res.data;
        this.utilService.startDatatable('all-product-type');
        this.loadingData = false;
        this.showData = true;
      }, error => {
        console.log('Error ', error);
        this.utilService.startDatatable('all-product-type');
        this.loadingData = false;
        this.showData = true;
      });
  }

  openDialog(id) {
    this.resetType();
    this.utilService.openModal(id);
  }


  public triggerEdit(fType) {
    this.activeType = JSON.parse(JSON.stringify(fType));
    this.resetType();
    this.utilService.openModal('productTypeModal');
    this.newType = this.activeType;
    this.modal = {
      title: 'Update Category',
      btnText: 'UPDATE'
    };
  }
  public updateType() {
    if (this.newType.name && this.newType.status) {
      this.superAdminService.updateProductType(this.newType, this.activeType._id).subscribe((res) => {
        console.log('Res ', res);
        this.getFarmType();
        this.utilService.closeModal('productTypeModal');
        this.loaders.saving = false;
        this.resetType();
        this.alertService.success(res.msg || 'Type updated successfully!', 'right');

      }, error => {
        console.log('Error ', error);
        this.alertService.error(error.error.message || 'Unable to update type!');
        this.loaders.saving = false;

      });
    } else {
      this.alertService.error('Unable to update type!');
      this.loaders.saving = false;

    }
  }

  saveType() {
    this.loaders.saving = true;
    if (this.activeType) {
      this.updateType();
    } else {
      if (this.newType.name && this.newType.status) {
        this.superAdminService.createProductType(this.newType).subscribe((res) => {
          console.log('Res ', res);
          this.getFarmType();
          this.loaders.saving = false;
          this.utilService.closeModal('productTypeModal');
          this.resetType();
          this.alertService.success(res.msg || 'Type added successfully!', 'right');
        }, error => {
          console.log('Error ', error);
          this.alertService.error(error.error.msg || 'Unable to add new type!');
          this.loaders.saving = false;
        });
      } else {
        this.alertService.error('Unable to add new type!');
        this.loaders.saving = false;
      }
    }

  }
  resetType() {
    this.newType =  {
      name: null,
      status: true
    };
  }
  deleteCategory(type) {
    this.utilService.confirmAction(() => {
      this.superAdminService.deleteProductType(type._id)
        .subscribe((res: IResponse) => {
          console.log('Res ', res);
          this.getFarmType();
        }, error => {
          console.log('Errro ', error);
        });
    });
  }
}
