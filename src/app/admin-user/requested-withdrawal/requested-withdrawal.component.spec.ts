import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestedWithdrawalComponent } from './requested-withdrawal.component';

describe('RequestedWithdrawalComponent', () => {
  let component: RequestedWithdrawalComponent;
  let fixture: ComponentFixture<RequestedWithdrawalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestedWithdrawalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestedWithdrawalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
