import { Component, OnInit } from '@angular/core';
import {NavigatorService} from '../../services/navigatorService/navigator.service';
import {IResponse} from '../../interfaces/iresponse';
import {UtilService} from '../../services/utilService/util.service';
import {BootstrapNotifyService} from '../../services/bootstrap-notify/bootstrap-notify.service';
import {SuperAdminService} from '../../services/api-handlers/superAdminService/super-admin.service';
@Component({
  selector: 'app-requested-withdrawal',
  templateUrl: './requested-withdrawal.component.html',
  styleUrls: ['./requested-withdrawal.component.css']
})
export class RequestedWithdrawalComponent implements OnInit {
  breadCrumb: any;
  withdrawalRequest: any[] = [];
  showData = true;
  showData2 = true;
  public loadingData = false;
  currentUser = null;
  processing = false;
  constructor(private navigatorService: NavigatorService,
              private alertService: BootstrapNotifyService,
              private superAdminService: SuperAdminService, private utilService: UtilService) { }

  ngOnInit() {
    this.currentUser = this.utilService.getAuthUser();
    this.breadCrumb  = {
      name: 'Requested Withdrawal',
      parent: 'Fund',
      subLink: null
    };
    this.getWithdrawRequest();
  }

  getWithdrawRequest() {
    this.showData = false;
    this.loadingData = true;
    this.superAdminService.getRequestedWithdraw()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.withdrawalRequest = res.data;
        this.loadingData = false;
        this.showData = true;
        this.utilService.neutralDatatable('all-requested-withdrawal', true);
      }, error => {
        console.log('Error ', error);
        this.loadingData = false;
        this.showData = true;
        this.utilService.neutralDatatable('all-requested-withdrawal', true);

      });
  }

  takeActionRequest(request, i, action) {
    this.processing = true;
    $('#processing-' + i).removeClass('d-none');
    if(action === 'approve') {
      this.superAdminService.acceptRequest({requestId: request._id, userId: request.userId._id}).subscribe((res: IResponse) => {
        console.log('REs ', res);
        $('.processing-').addClass('d-none');
        this.processing = false;
        this.getWithdrawRequest();
        this.alertService.success(res.msg || `Withdrawal request ${action}d successfully`)
      }, (error) => {
        console.log('Error ', error);
        this.processing = false;
        $('.processing-').addClass('d-none');
        this.alertService.error(error.error.msg || `Unable to ${action} withdrawal request`);
      });
    } else {
      this.superAdminService.rejectRequest({requestId: request._id}).subscribe((res: IResponse) => {
        console.log('REs ', res);
        $('.processing-').addClass('d-none');
        this.processing = false;
        this.getWithdrawRequest();
        this.alertService.success(res.msg || `Withdrawal request ${action}ed successfully`)
      }, (error) => {
        console.log('Error ', error);
        this.processing = false;
        $('.processing-').addClass('d-none');
        this.alertService.error(error.error.msg || `Unable to ${action} withdrawal request`);
      });
    }

  }
}
