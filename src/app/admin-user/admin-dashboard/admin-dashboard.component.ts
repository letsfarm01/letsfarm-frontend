import {AfterViewInit, Component, OnInit} from '@angular/core';
import {UserService} from '../../services/api-handlers/userService/user.service';
import {IResponse} from '../../interfaces/iresponse';
import {NavigatorService} from '../../services/navigatorService/navigator.service';
import {UtilService} from '../../services/utilService/util.service';
import { Label } from 'ng2-charts';

import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import {SuperAdminService} from '../../services/api-handlers/superAdminService/super-admin.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit, AfterViewInit {
  breadCrumb: any;
  dashboard = null;
  currentUser = null;
  loaders = {
    loading: true,
    loadingData: false
  };
  activeSponsor = [];
  archiveSponsors = [];


  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];

  public barChartData: ChartDataSets[] = [];
  constructor(private superadminService: SuperAdminService,
              private navigatorService: NavigatorService,
              private utilService: UtilService) {
    this.currentUser = this.utilService.getAuthUser();
  }

  ngOnInit() {
    this.breadCrumb  = {
      name: 'Dashboard',
      parent: 'Home',
      subLink: null
    };
  }
  ngAfterViewInit() {
    this.getDashboard();
  }
  public checkPrivilege(users) {
    // console.log('USERS ', this.currentUser.role, users);
    const myRole = this.currentUser.role.toLowerCase();
    return users.includes(myRole);
  }
  getDashboard() {
    this.loaders.loading = true;
    this.superadminService.getDashboard()
      .subscribe((res: IResponse) => {
      console.log('Res ', res);
      this.dashboard = res.data;
      this.loaders.loading = false;
    }, error => {
      console.log('Error ', error );
    });
  }
  gotoSponsor() {
    this.navigatorService.navigateUrl('/super/my-farm');
  }
  gotoFarm() {
    this.navigatorService.navigateUrl('/super/my-farm');
  }
}

/*  breadCrumb: any;

  public selectedFile: FileList;
  public formData: any;
  public dashboard: any = null;
  constructor(private alertService: BootstrapNotifyService) { }

  ngOnInit() {
    this.breadCrumb  = {
      name: 'Dashboard',
      parent: 'Home',
      subLink: null
    };
    this.setDashboard();
  }

  public setDashboard() {
    /!*this.superAdminService.getDashboard()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.dashboard = res.data;
      }, error => {
        console.log('Error ', error);
      });*!/
  }

  public importFiles(e) {
    console.info('File selected ', e);
    this.fileUpload(e);
  }

  public fileUpload(e) {
    this.selectedFile = e.target.files;
    if (this.selectedFile.length < 1) { return false; }
    console.log('Selected File: ', this.selectedFile);
    this.formData = new FormData();
    this.formData.append('file', e.target.files[0]);
    this.uploadFile();
  }
  public uploadFile() {
    Utils.proceedToImport(this.formData, '/users/migration-new', (response_) => {
      this.alertService.success('Upload file done');
      console.log('Res ', response_);
    }, (response_) => {
      this.alertService.error('Unable to upload file');
      console.log('Error ', response_);
      return false;
    });
  }

}*/
