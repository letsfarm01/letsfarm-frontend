import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaystackTransactionComponent } from './paystack-transaction.component';

describe('PaystackTransactionComponent', () => {
  let component: PaystackTransactionComponent;
  let fixture: ComponentFixture<PaystackTransactionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaystackTransactionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaystackTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
