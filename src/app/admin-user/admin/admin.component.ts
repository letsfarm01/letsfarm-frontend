import { Component, OnInit } from '@angular/core';
import {SuperAdminService} from '../../services/api-handlers/superAdminService/super-admin.service';
import {IResponse} from '../../interfaces/iresponse';
import {UtilService} from '../../services/utilService/util.service';
import {BootstrapNotifyService} from '../../services/bootstrap-notify/bootstrap-notify.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  breadCrumb: any;

  loaders = {
    saving: false
  };
  admins: any [] = [];
  newAdmin =  {
    first_name: null,
    last_name: null,
    email: null,
    phone_number: null,
    password: null,
    role: null,
    is_verified: true
  };
  public activeAdmin = null;
  public loadingData = false;
  public modal = {
    title: 'Add New Admin',
    btnTxt: 'SAVE'
  };
  showData = true;
  constructor(private superAdminService: SuperAdminService,
              private utilService: UtilService,
              private alertService: BootstrapNotifyService) { }

  ngOnInit() {
    this.breadCrumb  = {
      name: 'Admin',
      parent: 'Home',
      subLink: null
    };
    this.getAdmins();
  }

  getAdmins() {
    this.showData = false;
    this.loadingData = true;
    this.superAdminService.getUsers({role: 'SUPER'})
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.admins = res.data.data;
        this.utilService.startDatatable('all-admin');
        this.loadingData = false;
        this.showData = true;
      }, error => {
        console.log('Error ', error);
        this.utilService.startDatatable('all-admin');
        this.loadingData = false;
        this.showData = true;
      });
  }

  openDialog(id) {
    this.modal = {
      title: 'Create Admin',
      btnTxt: 'CREATE'
    };
    this.activeAdmin = null;
    this.resetAdmin();
    this.utilService.openModal(id);
  }


  public triggerEdit(fAdmin) {
    this.activeAdmin = JSON.parse(JSON.stringify(fAdmin));
    this.resetAdmin();
    this.utilService.openModal('adminModal');
    this.newAdmin = this.activeAdmin;
    this.modal = {
      title: 'Update Admin',
      btnTxt: 'UPDATE'
    };
  }
  public updateAdmin() {
    this.superAdminService.updateUser(this.newAdmin, this.activeAdmin._id).subscribe((res) => {
      console.log('Res ', res);
      this.getAdmins();
      this.utilService.closeModal('adminModal');
      this.loaders.saving = false;
      this.resetAdmin();
      this.alertService.success(res.msg || 'Admin updated successfully!', 'right');

    }, error => {
      console.log('Error ', error);
      this.alertService.error(error.error.message || 'Unable to update admin!');
      this.loaders.saving = false;

    });
  }

  saveAdmin() {
    this.loaders.saving = true;
    if(!this.newAdmin.first_name) {
      return this.alertService.error('First name is required!');
    } else if (!this.newAdmin.last_name) {
      return this.alertService.error('Last name is required!');
    } else if(!this.newAdmin.email) {
      return this.alertService.error('Email is required!');
    } else if(!this.newAdmin.phone_number) {
      return this.alertService.error('Phone number is required!');
    } else if(!this.newAdmin.role) {
      return this.alertService.error('Role is required!');
    } else {
      if (this.activeAdmin) {
        this.updateAdmin();
      } else {
        if(!this.newAdmin.password) {
          return this.alertService.error('Password is required!');
        }
        this.superAdminService.createAdmin(this.newAdmin).subscribe((res) => {
          console.log('Res ', res);
          this.getAdmins();
          this.loaders.saving = false;
          this.utilService.closeModal('adminModal');
          this.resetAdmin();
          this.alertService.success(res.msg || 'Admin created successfully!', 'right');
        }, error => {
          console.log('Error ', error);
          this.alertService.error(error.error.msg || 'Unable to create admin!');
          this.loaders.saving = false;
        });
      }
    }
  }
  deleteAdmin(admin) {
    this.utilService.confirmAction(() => {
       this.superAdminService.deleteAdmin(admin._id)
         .subscribe((res: IResponse) => {
           console.log('Res ', res);
           this.utilService.closeModal('adminModal');
           this.getAdmins();
         }, error => {
           console.log('Errro ', error);
         });
    });
  }
  resetAdmin() {
    this.newAdmin =  {
      first_name: null,
      last_name: null,
      email: null,
      phone_number: null,
      password: null,
      role: null,
      is_verified: true
    };
  }
}
