import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminFarmShopRoutesComponent } from './admin-farm-shop-routes.component';

describe('AdminFarmShopRoutesComponent', () => {
  let component: AdminFarmShopRoutesComponent;
  let fixture: ComponentFixture<AdminFarmShopRoutesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminFarmShopRoutesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminFarmShopRoutesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
