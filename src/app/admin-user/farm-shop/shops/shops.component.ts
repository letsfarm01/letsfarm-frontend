import {Component, NgZone, OnInit} from '@angular/core';
import {SuperAdminService} from '../../../services/api-handlers/superAdminService/super-admin.service';
import {UtilService} from '../../../services/utilService/util.service';
import {BootstrapNotifyService} from '../../../services/bootstrap-notify/bootstrap-notify.service';
import {IResponse} from '../../../interfaces/iresponse';
import {EventsService} from '../../../services/eventServices/event.service';
import {isNullOrUndefined} from 'util';
import {Upload} from '../../../models/upload';
import {DeleteService} from '../../../services/firebase-delete/delete.service';
import * as firebase from 'firebase';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';

@Component({
  selector: 'app-shops',
  templateUrl: './shops.component.html',
  styleUrls: ['./shops.component.css']
})
export class ShopsComponent implements OnInit {
breadCrumb: any = null;
  foodStuff = false;
  foodStuffEdit = false;
  loaders = {
    saving: false,
    loadingImg: false
  };
  editorDiv = false;
  allShops: any [] = [];
  allStatus: any [] = [];
  allTypes: any [] = [];
  newShop =  {
    farm_name: null,
    farm_location: null,
    percentage_to_gain: 0,
    amount_to_invest: 0.0,
    currency: 'NGN',
    remaining_in_stock: 0,
    duration: null,
    duration_type: 'MONTH',
    display_img: null,
    description: null,
    total_in_stock: 1,
    opening_date: null,
    closing_date: null,
    farm_type: null,
    farm_status: null,
    roi_interval: 0,
  };
  public activeShop = null;
  public loadingData = false;
  public modal = {
    title: 'Create Farm Shop',
    btnText: 'SAVE'
  };
  showData = true;

  constructor(private superAdminService: SuperAdminService,
              private utilService: UtilService,
              private eventService: EventsService,
              private scrollToService: ScrollToService,
              private firebaseDeleteService: DeleteService,
              private alertService: BootstrapNotifyService) {
  }

  ngOnInit() {
    this.breadCrumb  = {
      name: 'Shops',
      parent: 'Farm-Shop',
      subLink: null
    };
    this.getFarmShops();
    this.initPicker();
    this.getFarmStatus();
    this.getFarmTypes();
  }
  initPicker() {
    setTimeout(() => {
      (<any>$('.datepicker')).datetimepicker({
        icons: {
          time: 'fa fa-clock-o',
          date: 'fa fa-calendar',
          up: 'fa fa-chevron-up',
          down: 'fa fa-chevron-down',
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-screenshot',
          clear: 'fa fa-trash',
          close: 'fa fa-remove'
        },
        format: 'YYYY-MM-DD'
      });
    }, 400);
  }

  getFarmShops() {
    this.showData = false;
    this.loadingData = true;
    this.superAdminService.getFarmShops()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.allShops = res.data.data;
        this.utilService.startDatatable('all-farm-shop');
        this.loadingData = false;
        this.showData = true;
      }, error => {
        console.log('Error ', error);
        this.utilService.startDatatable('all-farm-shop');
        this.loadingData = false;
        this.showData = true;
      });
  }
  public triggerEdit(fShop) {
    this.editorDiv = false;
    this.activeShop = JSON.parse(JSON.stringify(fShop));
    // console.log('this.activeShop ', this.activeShop, fShop);
    this.resetShop();
    this.newShop = this.activeShop;
    this.newShop.farm_type = this.activeShop.farm_type._id;
    this.newShop.farm_status = this.activeShop.farm_status._id;
    if (fShop.farm_type.name.toLowerCase() === 'food trading'
      || fShop.farm_type.name.toLowerCase() === 'food stuff trading') {
      this.foodStuffEdit = true;
    } else {
      this.foodStuffEdit = false;
      // console.log('ALL TYPES  ', this.allTypes);

      const allTypes = this.allTypes.filter((type) => type.name.toLowerCase() !== 'food stuff trading'
        && type.name.toLowerCase() !== 'food trading');
      console.log('ALL TYES ', allTypes);
      this.allTypes = allTypes;
    }
    console.log('NEW SHOP ', this.newShop, this.activeShop);
    $('#main-table').addClass('d-none');
    this.modal = {
      title: 'Update Farm Shop',
      btnText: 'UPDATE'
    };
    this.editorDiv = true;
    this.foodStuffEdit = false;
    $('#main-table').addClass('d-none');
    this.initPicker();
  }
  openDialog() {
    this.editorDiv = true;
    this.activeShop = null;
    this.foodStuffEdit = false;
    $('#main-table').addClass('d-none');
    this.initPicker();
  }
  closeEditor() {
    this.getFarmTypes();
    $('.profilepage_1').addClass('animated fadeOutDown');
    setTimeout(() => {
      this.editorDiv = false;
      this.activeShop = null;
      this.modal = {
        title: 'Create Farm Shop',
        btnText: 'SAVE'
      };
      this.resetShop();
    }, 500);
    $('#main-table').removeClass('d-none');
  }
  public updateShop() {
    // this.alertService.error('EDITOR MODE');
    if (this.newShop.display_img.includes('https://firebasestorage.googleapis.com/v0/b/letsfarm-33131.appspot.com')) {
      this.superAdminService.updateShop(this.newShop, this.activeShop._id).subscribe((res) => {
        console.log('Res ', res);
        this.getFarmShops();
        this.loaders.saving = false;
        this.resetShop();
        this.closeEditor();
        this.alertService.success(res.msg || 'Shop updated successfully!', 'right');
      }, error => {
        console.log('Error ', error);
        this.alertService.error(error.error.message || 'Unable to update shop!');
        this.loaders.saving = false;
      });
    } else {
      this.alertService.error('Unable to update shop, display image not set!');
      this.loaders.saving = false;

    }
  }

  saveShop() {
    this.loaders.saving = true;
    // console.log('Active ', this.activeShop);
    this.checkError(this.newShop, () => {
      if (this.activeShop) {
        this.updateShop();
      } else {
        // this.alertService.error('NEW MODE');

        if (this.newShop.display_img.includes('https://firebasestorage.googleapis.com/v0/b/letsfarm-33131.appspot.com')) {
          this.superAdminService.createShop(this.newShop).subscribe((res) => {
            console.log('Res ', res);
            this.getFarmShops();
            this.loaders.saving = false;
            this.resetShop();
            this.closeEditor();
            this.alertService.success(res.msg || 'Shop added successfully!', 'right');
          }, error => {
            console.log('Error ', error);
            this.alertService.error(error.error.msg || 'Unable to add new shop!');
            this.loaders.saving = false;
          });
        } else {
          this.alertService.error('Unable to add new shop, display image is not set!');
          this.loaders.saving = false;
        }
      }
    }, ['duration_type', 'currency', 'opening_date', 'closing_date', 'deleted', '_id', '__v', 'roi_interval', 'alertSent']);

  }
  public checkError(data, cb, omit= []) {
    const keys = Object.keys(data);
    const resolvedKeys = keys.filter(d => !omit.includes(d));
    console.log('Resolved ', resolvedKeys);
    for (const key of resolvedKeys) {
      console.log('CValue ', data[key], key );
      if (!data[key]) {
        this.loaders.saving = false;
        return this.alertService.error(`${key.toString().toUpperCase().split('_').join(' ')} is Required`);
      }
    }
    cb();
  }
  resetShop() {
    this.foodStuff = false;
    this.newShop =  {
      farm_name: null,
      farm_location: null,
      percentage_to_gain: 0,
      remaining_in_stock: 0,
      amount_to_invest: 0.0,
      currency: 'NGN',
      duration: null,
      duration_type: 'MONTH',
      display_img: null,
      description: null,
      total_in_stock: 1,
      opening_date: null,
      closing_date: null,
      farm_type: null,
      farm_status: null,
      roi_interval: 0
    };
  }

  fixDuration(e) {
    if (e && e.name.toLowerCase() === 'food trading' || e.name.toLowerCase() === 'food stuff trading') {
      this.newShop.duration_type = 'YEAR';
      this.foodStuff = true;
    } else {
      this.foodStuff = false;
    }
  }
  getFarmStatus() {
    this.superAdminService.getActiveFarmStatus()
      .subscribe((res: IResponse) => {
        this.allStatus = res.data;
      }, error => {
        console.log('Error ', error);
      });
  }

  getFarmTypes() {
    this.superAdminService.getActiveFarmTypes()
      .subscribe((res: IResponse) => {
        this.allTypes = res.data;
      }, error => {
        console.log('Error ', error);
      });
  }

  public startLoading(event) {
    if (event === 'showLoading') {
      setTimeout(() => {
        this.loaders.loadingImg = false;
      }, 3000);
      this.loaders.loadingImg = true;
    }
  }
  public removeImageAndRestorePicker(itemKey: string, position?: number) {
    console.log('Remove');
    let imageInFireBase;
    if (position) {
      imageInFireBase = this.newShop[itemKey][position];
      this.newShop[itemKey][position] = null;
    } else {
      imageInFireBase = this.newShop[itemKey];
      this.newShop[itemKey] = null;
    }
    this.deleteFromFireBase(imageInFireBase);
    console.log('Remove', this.newShop);
  }

  public deleteFromFireBase(file) {
    const imageName = file.split('https://firebasestorage.googleapis.com/v0/b/letsfarm-33131.appspot.com/o/letsfarm_images%2F')[1];
    const image = imageName.split('?alt=');
    console.log('Image ', image);
    this.firebaseDeleteService.deleteImages(image[0]);
  }
  public getUploadEvent(e) {
    console.log('Event ', e);
    // this.newShop.display_img = e.display_img;
  }
  public updateField(id) {
    setTimeout(() => {
      const val = $('#' + id).val();
      this.newShop[id] = val;
    }, 1000);
  }
  public validateNumber(key) {
    if (Math.sign(this.newShop[key]) < 0) {
      this.newShop[key] = this.newShop[key] * -1;
      if (!this.activeShop) {
        this.newShop.remaining_in_stock = this.newShop.total_in_stock;
      }
    } else {
      if (!this.activeShop) {
        this.newShop.remaining_in_stock = this.newShop.total_in_stock;
      }
      return;
    }
  }
  deleteShop(type) {
    this.utilService.confirmAction(() => {
      this.superAdminService.deleteShop(type._id)
        .subscribe((res: IResponse) => {
          console.log('Res ', res);
          this.getFarmShops();
        }, error => {
          console.log('Errro ', error);
        });
    });
  }
}
