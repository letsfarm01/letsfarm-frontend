import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserGroupPrivilegeComponent } from './user-group-privilege.component';

describe('UserGroupPrivilegeComponent', () => {
  let component: UserGroupPrivilegeComponent;
  let fixture: ComponentFixture<UserGroupPrivilegeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserGroupPrivilegeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserGroupPrivilegeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
