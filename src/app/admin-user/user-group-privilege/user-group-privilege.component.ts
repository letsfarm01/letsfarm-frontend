import { Component, OnInit } from '@angular/core';
import {SuperAdminService} from '../../services/api-handlers/superAdminService/super-admin.service';
import {IResponse} from '../../interfaces/iresponse';
import {UtilService} from '../../services/utilService/util.service';
import {BootstrapNotifyService} from '../../services/bootstrap-notify/bootstrap-notify.service';

@Component({
  selector: 'app-user-group-privilege',
  templateUrl: './user-group-privilege.component.html',
  styleUrls: ['./user-group-privilege.component.css']
})
export class UserGroupPrivilegeComponent implements OnInit {
  breadCrumb: any;

  loaders = {
    saving: false
  };
  privileges: any [] = [];
  userGroups: any [] = [];
  newAdmin =  {
    name: null,
    status: true
  };
  public activeAdmin = null;
  public loadingData = false;
  public modal = {
    title: 'Add New Admin',
    btnText: 'SAVE'
  };
  showData = true;
  constructor(private superAdminService: SuperAdminService,
              private utilService: UtilService,
              private alertService: BootstrapNotifyService) { }

  ngOnInit() {
    this.breadCrumb  = {
      name: 'Admin',
      parent: 'Home',
      subLink: null
    };
    this.getPrivileges();
    this.getUserGroups();
  }

  getPrivileges() {
    this.showData = false;
    this.loadingData = true;
    this.superAdminService.getPrivileges()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.privileges = res.data.data;
        this.loadingData = false;
        this.showData = true;
      }, error => {
        console.log('Error ', error);
        this.loadingData = false;
        this.showData = true;
      });
  }

  getUserGroups() {
    this.showData = false;
    this.loadingData = true;
    this.superAdminService.getUserGroups()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.privileges = res.data.data;
        this.loadingData = false;
        this.showData = true;
      }, error => {
        console.log('Error ', error);
        this.loadingData = false;
        this.showData = true;
      });
  }

  openDialog(id) {
    this.resetAdmin();
    this.utilService.openModal(id);
  }


  public triggerEdit(fAdmin) {
    this.activeAdmin = JSON.parse(JSON.stringify(fAdmin));
    this.resetAdmin();
    this.utilService.openModal('statusModal');
    this.newAdmin = this.activeAdmin;
    this.modal = {
      title: 'Update Admin',
      btnText: 'UPDATE'
    };
  }/*
  public updatePri() {
    if (this.newAdmin.name && this.newAdmin.status) {
      this.superAdminService.updateUser(this.newAdmin, this.activeAdmin._id).subscribe((res) => {
        console.log('Res ', res);
        this.getAdmins();
        this.utilService.closeModal('statusModal');
        this.loaders.saving = false;
        this.resetAdmin();
        this.alertService.success(res.msg || 'Admin updated successfully!', 'right');

      }, error => {
        console.log('Error ', error);
        this.alertService.error(error.error.message || 'Unable to update status!');
        this.loaders.saving = false;

      });
    } else {
      this.alertService.error('Unable to update status!');
      this.loaders.saving = false;

    }
  }
  toggleSponsor(value, status) {
    this.superAdminService.updateUser({can_sponsor: value}, status._id).subscribe((res) => {
      console.log('Res ', res);
      this.getAdmins();
    }, error => {
      console.log('Error ', error);
      this.alertService.error(error.error.message || 'Unable to update status!');

    });
  }

  saveAdmin() {
    this.loaders.saving = true;
    if (this.activeAdmin) {
      this.updateAdmin();
    } else {
      if (this.newAdmin.name && this.newAdmin.status) {
        /!*  this.superAdminService.createAdmin(this.newAdmin).subscribe((res) => {
            console.log('Res ', res);
            this.getFarmAdmin();
            this.loaders.saving = false;
            this.utilService.closeModal('statusModal');
            this.resetAdmin();
            this.alertService.success(res.msg || 'Admin added successfully!', 'right');
          }, error => {
            console.log('Error ', error);
            this.alertService.error(error.error.msg || 'Unable to add new status!');
            this.loaders.saving = false;
          });
       *!/ } else {
        this.alertService.error('Unable to add new status!');
        this.loaders.saving = false;
      }
    }

  }
  deleteAdmin(status) {
    this.utilService.confirmAction(() => {
      /!* this.superAdminService.deleteAdmin(status._id)
         .subscribe((res: IResponse) => {
           console.log('Res ', res);
           this.getFarmAdmin();
         }, error => {
           console.log('Errro ', error);
         });*!/
    });
  }*/
  resetAdmin() {
    this.newAdmin =  {
      name: null,
      status: true
    };
  }
  saveAdmin() {
   /* this.loaders.saving = true;
    if (this.activeAdmin) {
      this.updateAdmin();
    } else {
      if (this.newAdmin.name && this.newAdmin.status) {
        /!*  this.superAdminService.createAdmin(this.newAdmin).subscribe((res) => {
            console.log('Res ', res);
            this.getFarmAdmin();
            this.loaders.saving = false;
            this.utilService.closeModal('statusModal');
            this.resetAdmin();
            this.alertService.success(res.msg || 'Admin added successfully!', 'right');
          }, error => {
            console.log('Error ', error);
            this.alertService.error(error.error.msg || 'Unable to add new status!');
            this.loaders.saving = false;
          });
       *!/ } else {
        this.alertService.error('Unable to add new status!');
        this.loaders.saving = false;
      }
    }*/

  }
}
