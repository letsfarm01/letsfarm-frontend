import { Component, OnInit } from '@angular/core';
import {SuperAdminService} from '../../services/api-handlers/superAdminService/super-admin.service';
import {IResponse} from '../../interfaces/iresponse';
import {UtilService} from '../../services/utilService/util.service';
import {BootstrapNotifyService} from '../../services/bootstrap-notify/bootstrap-notify.service';
import {UserService} from '../../services/api-handlers/userService/user.service';
import {NavigatorService} from "../../services/navigatorService/navigator.service";

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {
  breadCrumb: any;

  loaders = {
    saving: false
  };
  customers: any [] = [];
  newCustomer =  null;
  public activeCustomer = null;
  public loadingData = false;
  public modal = {
    title: 'Add New Customer',
    btnText: 'SAVE'
  };
  showData = true;

  kin_info = null;
  business_info = null;
  bank_info = null;
  sponsorShip = [];
  constructor(private superAdminService: SuperAdminService,
              private utilService: UtilService,
              private userService: UserService,
              private navigatorService: NavigatorService,
              private alertService: BootstrapNotifyService) { }

  ngOnInit() {
    this.breadCrumb  = {
      name: 'Customers',
      parent: 'Home',
      subLink: null
    };
    this.getCustomers();
  }

  getCustomers() {
    this.showData = false;
    this.loadingData = true;
    this.superAdminService.getUsers({role: 'USER'})
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.customers = res.data.data;
        this.utilService.startDatatable('all-customer');
        this.loadingData = false;
        this.showData = true;
      }, error => {
        console.log('Error ', error);
        this.utilService.startDatatable('all-customer');
        this.loadingData = false;
        this.showData = true;
      });
  }

  openDialog(id) {
    this.resetCustomer();
    this.utilService.openModal(id);
  }
  viewInfo(customer) {
    this.activeCustomer = JSON.parse(JSON.stringify(customer));
    this.getActiveSponsor(customer);
    this.getUserBankInfo(customer);
    this.getUserNextKin(customer);
    this.getUserBusinessInfo(customer);
    this.utilService.openModal('viewCustomerModal');
  }



  public getUserBusinessInfo(user) {
    this.business_info = null;
    this.userService.getBusinessInfo(user._id).subscribe((res: IResponse) => {
      console.log('Res ', res);
      this.business_info = res.data;
    }, error => {
      console.log(error);
    });
  }
  public getUserBankInfo(user) {
    this.bank_info = null;
    this.userService.getBankInfo(user._id).subscribe((res: IResponse) => {
      console.log('Res ', res);
      this.bank_info = res.data;
    }, error => {
      console.log(error);
    });
  }

  public getUserNextKin(user) {
    this.kin_info = null;
    this.userService.getNextKin(user._id).subscribe((res: IResponse) => {
      console.log('Res ', res);
      this.kin_info = res.data;
    }, error => {
      console.log(error);
    });
  }

  getActiveSponsor(user) {
    this.sponsorShip = [];
    this.userService.getActiveSponsorship(user._id)
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.sponsorShip = res.data;
      }, error => {
        console.log('Error ', error);
      });
  }

  public triggerEdit(customer) {
    this.activeCustomer = JSON.parse(JSON.stringify(customer));
    setTimeout(() => {
      this.utilService.openModal('customerModal');
    }, 500);
    this.newCustomer = this.activeCustomer;
    this.modal = {
      title: 'Update Customer',
      btnText: 'UPDATE'
    };
  }
  public updateCustomer() {
    console.log('Basic info');
    if (!this.activeCustomer.first_name) {
      return this.alertService.error('First name is required!');
    } else if (!this.activeCustomer.last_name) {
      return this.alertService.error('Last name is required!');
    } else if (!this.activeCustomer.phone_number) {
      return this.alertService.error('Phone number is required!');
    } else if (!this.activeCustomer.email) {
      return this.alertService.error('Email address is required!');
    } else if (!this.activeCustomer.username) {
      return this.alertService.error('Username is required!');
    } else {
      this.superAdminService.updateUser(this.newCustomer, this.activeCustomer._id).subscribe((res) => {
        console.log('Res ', res);
        this.getCustomers();
        this.utilService.closeModal('customerModal');
        this.loaders.saving = false;
        this.resetCustomer();
        this.alertService.success(res.msg || 'Customer details updated successfully!', 'right');

      }, error => {
        console.log('Error ', error);
        this.alertService.error(error.error.msg || 'Unable to update customer details!');
        this.loaders.saving = false;

      });
    }
  }
  deleteCustomer(customer) {
    this.utilService.confirmAction(() => {
      this.superAdminService.deleteUser(customer._id)
        .subscribe((res: IResponse) => {
          console.log('Res ', res);
          this.getCustomers();
        }, error => {
          console.log('Errro ', error);
        });
    });
  }
  resetCustomer() {
    this.newCustomer = this.activeCustomer = null;
  }
  refundSponsor(sponsor) {
    this.alertService.info('This is not possible at the moment!');
  }
  cancelSponsor (sponsor) {
    console.log('CUSTOMER ', this.activeCustomer);
    this.utilService.confirmAction((res: IResponse) => {
      this.superAdminService.cancelSponsorship(this.activeCustomer._id, sponsor._id).subscribe(() => {
        this.alertService.success(res.msg || 'Sponsorship canceled successfully!');
      }, error => {
        console.log({error});
        this.alertService.error(error.error.msg || 'Unable to cancel sponsorship!');
      });
    });
  }
  viewDashboard() {
    this.utilService.closeModal('viewCustomerModal');
    this.navigatorService.navigateUrl('/super/view-user/dashboard/' + this.activeCustomer._id);
  }
}
