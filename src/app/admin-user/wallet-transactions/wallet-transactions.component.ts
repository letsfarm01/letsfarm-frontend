import { Component, OnInit } from '@angular/core';
import {IResponse} from '../../interfaces/iresponse';
import {SuperAdminService} from '../../services/api-handlers/superAdminService/super-admin.service';
import {UtilService} from '../../services/utilService/util.service';

@Component({
  selector: 'app-wallet-transactions',
  templateUrl: './wallet-transactions.component.html',
  styleUrls: ['./wallet-transactions.component.css']
})
export class WalletTransactionsComponent implements OnInit {
  breadCrumb: any;

  loaders = {
    saving: false
  };
  walletTransactions: any [] = [];
  newCustomer =  {
    name: null,
    status: true
  };
  initiator = null;
  public activeCustomer = null;
  public loadingData = false;
  public modal = {
    title: 'Add New Customer',
    btnText: 'SAVE'
  };
  showData = true;

  constructor(private superAdminService: SuperAdminService,
              private utilService: UtilService) { }

  ngOnInit() {
    this.breadCrumb  = {
      name: 'Wallet Transaction',
      parent: 'Report',
      subLink: null
    };
    this.getTransactions();
  }

  getTransactions() {
    this.showData = false;
    this.loadingData = true;
    this.superAdminService.getWalletTransactions()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.walletTransactions = res.data.data;
        this.utilService.startDatatable('walletTransaction-list');
        this.loadingData = false;
        this.showData = true;
      }, error => {
        console.log('Error ', error);
        this.utilService.startDatatable('walletTransaction-list');
        this.loadingData = false;
        this.showData = true;
      });
  }

  viewInitiator(transaction) {
    this.initiator = transaction.userId;
    this.utilService.openModal('initiatorModal');
    this.superAdminService.getWallet(transaction.userId._id).subscribe((res: IResponse) => {
      this.initiator.wallet = res.data || null;
      console.log('this.initiator ', this.initiator );
    }, e => {
      this.initiator.wallet = null;
    });
  }
}
