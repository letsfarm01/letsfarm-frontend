import { Component, OnInit } from '@angular/core';
import {IResponse} from '../../interfaces/iresponse';
import {SuperAdminService} from '../../services/api-handlers/superAdminService/super-admin.service';
import {UtilService} from '../../services/utilService/util.service';
import {BootstrapNotifyService} from '../../services/bootstrap-notify/bootstrap-notify.service';
import {DeleteService} from '../../services/firebase-delete/delete.service';
import {UploadService} from "../../services/uploadService/upload.service";

@Component({
  selector: 'app-updates',
  templateUrl: './updates.component.html',
  styleUrls: ['./updates.component.css']
})
export class UpdatesComponent implements OnInit {
  breadCrumb: any;
  editorDiv = false;
  loaders = {
    saving: false,
    loading: false,
    loadingImg: false
  };
  publishTo = 'undefined';
  activeUpdate = null;
  updates: any [] = [];
  allShops: any [] = [];
  public loadingData = false;
  showData = true;
  newUpdate = {
    title: null,
    img_link: null,
    message_body: "<p>Update Message...</p>"
  };
  initEditor = {
    height: 500,
    menubar: true,
    plugins: [
      'advlist autolink lists link image charmap print preview anchor',
      'searchreplace visualblocks code fullscreen',
      'insertdatetime media table paste code help wordcount'
    ],
    images_upload_url: 'https://letsfarm.com.ng',
    images_upload_handler: (blobInfo, success, failure) => {
      console.log(blobInfo);
      setTimeout( () => {
        console.log('image=> ', blobInfo);
        this.uploadService.uploadOnEditor(blobInfo.blob(), 'editor_images',(url) => {
          success(url);
        });
      }, 10);
    },
    toolbar:
      'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help'
  };
  public modal = {
    title: 'Create Farm Update',
    btnText: 'SAVE'
  };
  constructor(private superAdminService: SuperAdminService,
              private alertService: BootstrapNotifyService,
              private firebaseDeleteService: DeleteService,
              private uploadService: UploadService,
              private utilService: UtilService) {  }

  ngOnInit() {
    this.breadCrumb = {
      name: 'Notifications and Updates',
      parent: 'Home',
      subLink: null
    };
    this.getUpdates();

    this.getFarmShops();
  }

  getUpdates() {
    this.showData = false;
    this.loadingData = true;
    this.superAdminService.getUpdates()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.updates = res.data.data;
        this.utilService.startDatatable('all-updates');
        this.loadingData = false;
        this.showData = true;
      }, error => {
        console.log('Error ', error);
        this.utilService.startDatatable('all-updates');
        this.loadingData = false;
        this.showData = true;
      });
  }
  deleteUpdate(update) {
    this.utilService.confirmAction(() => {
      this.superAdminService.deleteUpdate(update._id).subscribe((res: IResponse) => {
        this.alertService.success(res.msg || 'Farm Update deleted successfully!');
        this.getUpdates();
      }, error => {
        this.alertService.error(error.error.msg || 'Unable to delete farm update!');
      });
    });
  }

  public triggerEdit(fUpdate) {
    this.editorDiv = false;
    this.activeUpdate = JSON.parse(JSON.stringify(fUpdate));
    this.resetUpdate();
    this.newUpdate = this.activeUpdate;
    console.log('NEW BLOG ', this.newUpdate, this.activeUpdate);
    $('#main-table').addClass('d-none');
    this.modal = {
      title: 'Update Farm Update',
      btnText: 'UPDATE'
    };
    this.openDialog();
  }
  openDialog() {
    this.editorDiv = true;
    // this.activeUpdate = null;
    $('#main-table').addClass('d-none');
    // this.initPicker();
  }
  closeEditor() {
    this.activeUpdate = null;
    $('.profilepage_1').addClass('animated fadeOutDown');
    setTimeout(() => {
      this.editorDiv = false;
      this.modal = {
        title: 'Create Farm Update',
        btnText: 'SAVE'
      };
      this.resetUpdate();
    }, 500);
    $('#main-table').removeClass('d-none');
  }
  public updateUpdate() {
    if (this.newUpdate.img_link.includes('https://firebasestorage.googleapis.com/v0/b/letsfarm-33131.appspot.com')) {
      this.superAdminService.updateUpdate(this.newUpdate, this.activeUpdate._id).subscribe((res) => {
        console.log('Res ', res);
        this.getUpdates();
        this.loaders.saving = false;
        this.resetUpdate();
        this.closeEditor();
        this.alertService.success(res.msg || 'Farm Update updated successfully!', 'right');
      }, error => {
        console.log('Error ', error);
        this.alertService.error(error.error.message || 'Unable to update farm update!');
        this.loaders.saving = false;
      });
    } else {
      this.alertService.error('Unable to update farm update, display image not set!');
      this.loaders.saving = false;

    }
  }

  saveUpdate() {
    this.loaders.saving = true;
    this.checkError(this.newUpdate, () => {
      if (this.activeUpdate) {
        this.updateUpdate();
      } else {
        if (this.newUpdate.img_link.includes('https://firebasestorage.googleapis.com/v0/b/letsfarm-33131.appspot.com')) {
          this.superAdminService.createUpdate(this.newUpdate).subscribe((res) => {
            console.log('Res ', res);
            this.getUpdates();
            this.loaders.saving = false;
            this.resetUpdate();
            this.closeEditor();
            this.alertService.success(res.msg || 'Farm Update saved successfully!', 'right');
          }, error => {
            console.log('Error ', error);
            this.alertService.error(error.error.msg || 'Unable to save farm update!');
            this.loaders.saving = false;
          });
        } else {
          this.alertService.error('Unable to save farm update, display image is not set!');
          this.loaders.saving = false;
        }
      }
    }, ['deleted', '_id', '__v', 'publish']);

  }
  public checkError(data, cb, omit= []) {
    const keys = Object.keys(data);
    const resolvedKeys = keys.filter(d => !omit.includes(d));
    console.log('Resolved ', resolvedKeys);
    for (const key of resolvedKeys) {
      console.log('CValue ', data[key], key );
      if (!data[key]) {
        this.loaders.saving = false;
        return this.alertService.error(`${key.toString().toUpperCase().split('_').join(' ')} is Required`);
      }
    }
    cb();
  }
  resetUpdate() {
    this.newUpdate = {
      title: null,
      img_link: null,
      message_body: "<p>Update Body...</p>"
    };
  }
  getFarmShops() {
    this.superAdminService.getFarmShops()
      .subscribe((res: IResponse) => {
      console.log('FARM SHOPS ', res);
        this.allShops = res.data.data;
      }, error => {
        console.log('Error ', error);
      });
  }

  public startLoading(event) {
    if (event === 'showLoading') {
      setTimeout(() => {
        this.loaders.loadingImg = false;
      }, 3000);
      this.loaders.loadingImg = true;
    }
  }
  public removeImageAndRestorePicker(itemKey: string, position?: number) {
    console.log('Remove');
    let imageInFireBase;
    if (position) {
      imageInFireBase = this.newUpdate[itemKey][position];
      this.newUpdate[itemKey][position] = null;
    } else {
      imageInFireBase = this.newUpdate[itemKey];
      this.newUpdate[itemKey] = null;
    }
    this.deleteFromFireBase(imageInFireBase);
    console.log('Remove', this.newUpdate);
  }
  public deleteFromFireBase(file) {
    const imageName = file.split('https://firebasestorage.googleapis.com/v0/b/letsfarm-33131.appspot.com/o/letsfarm_images%2F')[1];
    const image = imageName.split('?alt=');
    console.log('Image ', image);
    this.firebaseDeleteService.deleteImages(image[0]);
  }
  triggerPublish(update) {
    this.activeUpdate = update;
    this.utilService.openModal('updateFarmModal');
  }
  publishUpdate() {
    if(!this.publishTo || this.publishTo == 'undefined'){
      return false;
    } else {
      this.loaders.saving = true;
      this.superAdminService.publishUpdate({updateId: this.activeUpdate._id, pushTo: this.publishTo})
        .subscribe((res: IResponse) => {
          this.alertService.success('Update published successfully!');
          this.publishTo = 'undefined';
          this.loaders.saving = false;
          this.utilService.closeModal('updateFarmModal');
          this.getUpdates();
        }, error => {
          this.loaders.saving = false;
          this.alertService.error(error.error.msg || 'Unable to publish update');
          console.log('Error ', error);
        });
    }
  }
}
