import { Component, OnInit } from '@angular/core';
import {UserService} from "../../services/api-handlers/userService/user.service";
import {BootstrapNotifyService} from "../../services/bootstrap-notify/bootstrap-notify.service";
import {IResponse} from "../../interfaces/iresponse";

@Component({
  selector: 'app-farm-updates',
  templateUrl: './farm-updates.component.html',
  styleUrls: ['./farm-updates.component.css']
})
export class FarmUpdatesComponent implements OnInit {
  breadCrumb: any;
  allUpdates = [];
  loading = false;
  // updates: any = [];
  constructor(private userService: UserService,
              private alertService: BootstrapNotifyService) { }

  ngOnInit() {
    this.breadCrumb  = {
      name: 'Farm Updates',
      parent: 'Home',
      subLink: null
    };
    this.getUpdates();
  }

  getUpdates() {
    this.loading = true;
    this.userService.getUpdates().subscribe((res: IResponse) => {
      this.loading = false;
      console.log('Res ', res);
       const data = res.data;
      this.allUpdates = [...data.all, ...data.updates];
      console.log('Updates ', this.allUpdates);
    }, error => {
      this.loading = false;
      console.log('Error ', error);
    });
  }
  continueReading(i) {
    $('.update-body').addClass('collapse-text-ellipsis');
    $('.update-btn').removeClass('d-none');
    setTimeout(() => {
      $('#ellipsis-'+i).toggleClass('collapse-text-ellipsis');
      $('#continue-'+i).addClass('d-none');
    }, 200);
  }
}
