import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmUpdatesComponent } from './farm-updates.component';

describe('FarmUpdatesComponent', () => {
  let component: FarmUpdatesComponent;
  let fixture: ComponentFixture<FarmUpdatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmUpdatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmUpdatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
