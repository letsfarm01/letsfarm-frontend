import {AfterViewInit, Component, OnInit} from '@angular/core';
import {UserService} from '../../services/api-handlers/userService/user.service';
import {IResponse} from '../../interfaces/iresponse';
import {NavigatorService} from '../../services/navigatorService/navigator.service';
import {UtilService} from '../../services/utilService/util.service';
import { Label } from 'ng2-charts';

import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.css']
})
export class UserDashboardComponent implements OnInit, AfterViewInit {
  breadCrumb: any;
  dashboard = null;
  currentUser = null;
  loaders = {
    loading: true,
    loadingData: false
  };
  activeSponsor = [];
  archiveSponsors = [];
  roiData = [];


  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];

  public barChartData: ChartDataSets[] = [];
  constructor(private userService: UserService,
              private navigatorService: NavigatorService,
              private utilService: UtilService) {
  }

  ngOnInit() {
    this.currentUser = this.utilService.getAuthUser();
    this.breadCrumb  = {
      name: 'Dashboard',
      parent: 'Home',
      subLink: null
    };
    this.getActiveSponsor();
    this.getArchive();
  }
  ngAfterViewInit() {
    this.getDashboard();
  }
  getDashboard() {
    this.loaders.loading = true;
    this.userService.getDashboard().subscribe((res: IResponse) => {
      console.log('Res ', res);
      this.dashboard = res.data;
      let invested, returns, interests, descriptions;
      /*if (this.dashboard.expected_returns.invested.length > 9) {
        descriptions = this.dashboard.expected_returns.description.reverse().slice(0, 9).reverse();
        invested = this.dashboard.expected_returns.invested.reverse().slice(0, 9).reverse();
        returns = this.dashboard.expected_returns.returns.reverse().slice(0, 9).reverse();
        interests = this.dashboard.expected_returns.interests.reverse().slice(0, 9).reverse();
      } else {*/
        descriptions = this.dashboard.expected_returns.description;
        invested = this.dashboard.expected_returns.invested;
        returns = this.dashboard.expected_returns.returns;
        interests = this.dashboard.expected_returns.interests;
      // }
      this.barChartLabels = descriptions;
      console.log('DATA ', invested, returns, interests);
      this.barChartData = [
        { data: invested, label: 'Amount Invested' },
        { data: returns, label: 'Expected Return' },
        { data: interests, label: 'Calculated Interest' }
      ];
      this.loaders.loading = false;
    }, error => {
      console.log('Error ', error );
    });
  }
  gotoSponsor() {
    this.navigatorService.navigateUrl('/user/my-farm');
  }
  gotoFarm() {
    this.navigatorService.navigateUrl('/user/my-farm');
  }

  getActiveSponsor() {
    this.loaders.loadingData = true;
    this.userService.getActiveSponsorship(this.currentUser._id)
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.activeSponsor = res.data;
        this.loaders.loadingData = false;
      }, error => {
        console.log('Error ', error);
        this.loaders.loadingData = false;
      });
  }
  getArchive() {
    this.userService.getArchiveSponsorship({email: this.currentUser.email})
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.archiveSponsors = res.data;
        this.utilService.startDatatable('archive-farm-sponsor');
      }, error => {
        console.log('Error ', error);
        this.utilService.startDatatable('archive-farm-sponsor');
      });
  }
  viewROI(data) {
    this.roiData = data;
    this.utilService.openModal('foodTrading');
  }
  closeTradingModal() {
    this.utilService.closeModal('foodTrading');
    this.roiData = [];
  }
}
