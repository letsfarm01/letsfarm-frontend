import { Component, OnInit} from '@angular/core';
import {NavigatorService} from '../../services/navigatorService/navigator.service';
import {IResponse} from '../../interfaces/iresponse';
import {UserService} from '../../services/api-handlers/userService/user.service';
import {BootstrapNotifyService} from '../../services/bootstrap-notify/bootstrap-notify.service';
import {UtilService} from '../../services/utilService/util.service';
import {EventsService} from "../../services/eventServices/event.service";

@Component({
  selector: 'app-farm-update',
  templateUrl: './farm-update.component.html',
  styleUrls: ['./farm-update.component.css']
})
export class FarmUpdateComponent implements OnInit  {
  breadCrumb: any = null;
  allShops: any = [];
  loading = false;
  adding = false;
  shopData = null;
  quantity = 1;
  currentUser = null;
  percentageLeft = 0;
  constructor(private navigatorService: NavigatorService,
              private alertService: BootstrapNotifyService,
              private utilService: UtilService,
              private eventService: EventsService,
              private userService: UserService) {
  }

  ngOnInit() {
    this.breadCrumb  = {
      name: 'Farm Shop',
      parent: 'Home',
      subLink: null
    };
    this.getFarmShops();
    this.currentUser = this.utilService.getAuthUser();
  }
  getFarmShops() {
    this.loading = true;
    this.userService.getActiveShop()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.allShops = res.data.data;
        this.loading = false;
      }, error => {
        console.log('Error ', error);
        this.loading = false;
      });
  }
  loadThisShop(shop) {
    this.shopData = shop;
    this.quantity = 1;
    this.percentageLeft = Math.round((this.shopData.remaining_in_stock / this.shopData.total_in_stock ) * 100);
  }
  closeViewFarm() {
    this.shopData = null;
    this.quantity = 1;
  }

  validateQty(id) {
    const value: any = $(`#${id}`).val();
    if (value === 0) {
      $(`#${id}`).val(1);
      this.quantity = 1;
    }
    this.quantity = Math.abs(value) || 1;
  }
  handleInvestment(farmShop) {
    this.alertService.info('Adding Item to cart!');
    this.adding = true;
    this.userService.addShopToCart({
      userId: this.currentUser._id,
      farmId: farmShop._id,
      quantity: this.quantity
    }).subscribe((res: IResponse) => {
      this.navigatorService.navigateUrl('/user/cart');
      this.adding = false;
      this.eventService.broadcast('CART_UPDATED', res);
    }, error => {
      this.adding = false;
      console.log('Error ', error);
      this.alertService.error(error.error.msg || 'Unable to add to cart!');
    });
  }

  gotoURL(url) {
    this.navigatorService.navigateUrl(url);
  }
}
