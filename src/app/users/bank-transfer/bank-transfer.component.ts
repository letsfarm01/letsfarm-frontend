import { Component, OnInit } from '@angular/core';
import {CacheService} from '../../services/cacheService/cache.service';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../../services/api-handlers/userService/user.service';
import {IResponse} from '../../interfaces/iresponse';
import {BootstrapNotifyService} from '../../services/bootstrap-notify/bootstrap-notify.service';
import {UploadService} from '../../services/uploadService/upload.service';
import {Upload} from '../../models/upload';
import {UtilService} from '../../services/utilService/util.service';
import {NavigatorService} from '../../services/navigatorService/navigator.service';
import swal from 'sweetalert2';
@Component({
  selector: 'app-bank-transfer',
  templateUrl: './bank-transfer.component.html',
  styleUrls: ['./bank-transfer.component.css']
})
export class ConfirmBankTransferComponent implements OnInit {
  public transactionRef: any;
  breadCrumb: any;
  transaction: any;
  proof = {
    message: null,
    documents: [],
    bank_name: null,
    transactionId: null,
    userId: null,
    account_name: null,
    amount: null,
    transactionRef: null,
    rawImage: []
  };
  currentUser = null;
  location = window.location.origin;
  loaders = {
    loading: false
  };
  constructor(private cacheService: CacheService, private route: ActivatedRoute,
              private alertService: BootstrapNotifyService,
              private uploadService: UploadService,
              private utilService: UtilService,
              private navigatorService: NavigatorService,
              private userService: UserService) {
    this.getTransactionRef();
  }

  ngOnInit() {
    this.currentUser = this.utilService.getAuthUser();
    this.breadCrumb  = {
      name: 'Bank Transfer',
      parent: 'Payment',
      subLink: null
    };
  }
  getTransactionRef() {
    this.transactionRef = this.route.snapshot.paramMap.get('paymentId') || null;
    if (this.transactionRef) {
    this.cacheService.setStorage('BankRef', this.transactionRef);
    } else {
    this.transactionRef = this.cacheService.getStorage('BankRef') || null;
    }
    if (this.transactionRef) {
      console.log('Bank Reference ', this.transactionRef);
      this.getTransactionByRef();
    } else {
      // do nothing
      this.transactionRef = null;
      this.cacheService.deleteStorage('BankRef');
    }
  }
  public getTransactionByRef() {
    this.userService.getTransactionByRef(this.transactionRef)
      .subscribe((res: IResponse) => {
      console.log('Transaction Details ', res);
      this.transaction = res.data;
      this.proof.transactionId = res.data._id;
      this.proof.transactionRef = res.data.orderReference;
      }, error => {
      console.log('Error');
      });
  }
  public triggerUploader(id) {
    $('#' + id).trigger('click');
  }
  public saveProof() {
    if (!this.transactionRef) {
      swal({title: 'Hello ' + this.currentUser.full_name,
        html: 'Before uploading a proof of payment kindly check your ' +
        '<a href=' + this.location + '/user/payment/pending-transfer>PENDING PAYMENTS</a> ' +
        'to select the correct transaction that you paid for, thank you.',
        type: 'warning'});
      return this.alertService.warning('Invalid action, no transaction payment is pending at the moment!');
    }
    this.loaders.loading = true;
    if (this.proof.rawImage.length < 1) {
      this.alertService.error('No evidence of payment uploaded, please consider uploading a document as prove of payment!');
      this.loaders.loading = false;
      return false;
    } else {
      this.uploader(this.proof.rawImage, 0);
    }

  }
  public uploader(images, index) {
    console.log('INDEXER ', index, this.proof.rawImage.length);
    if (this.proof.rawImage.length === index ) {
      // done with upload
      this.proceedToUpload();
      return false;
    }
    this.uploadService.uploadMultiple(new Upload(images[index]), 'proof_payment').then(success => {
      // console.log('Sus ', success, index, this.proof.rawImage.length);
      this.proof.documents.push(success);
        const next = index + 1;
      // console.log('NEXt SUS', next, this.proof.rawImage[next]);
      this.uploader(this.proof.rawImage, next);
    }).catch(err => {
      // console.log('Error ', err, index, this.proof.rawImage.length);
      this.proof.documents.push('upload_failed');
        const next = index + 1;
        // console.log('NEXt ', next, this.proof.rawImage[next]);
        this.uploader(this.proof.rawImage, next);
    });
  }
  public uploadFiles(events) {
    console.log('Events ', events);
    this.proof.rawImage = events.target.files;
  }
  public proceedToUpload() {
    console.log('PROOF', this.proof);
    this.proof.userId = this.currentUser._id;
    delete this.proof.rawImage;
    this.userService.confirmBankTransfer(this.proof).subscribe((res: IResponse) => {
      console.log('Res ', res);
      this.loaders.loading = false;
      this.alertService.success('Payment proofs saved successful, details will be reviewed in the next 24hours');
      this.cacheService.deleteStorage('BankRef');
      this.navigatorService.navigateUrl('/user/payment/pending-transfer');
    }, error => {
      console.log('Error ', error);
      this.loaders.loading = false;
      this.alertService.error(error.error.msg || 'Unable to save payment proofs.');
      swal({title: 'Hello ' + this.currentUser.full_name,
        html: 'Before uploading a proof of payment kindly check your ' +
        '<a href=' + this.location + '/user/payment/pending-transfer>PENDING PAYMENTS</a> ' +
        'to select the correct transaction that you paid for, thank you.',
        type: 'warning'});
    });
  }
}
