import { Component, OnInit } from '@angular/core';
import {NavigatorService} from '../../services/navigatorService/navigator.service';
import {IResponse} from '../../interfaces/iresponse';
import {UserService} from '../../services/api-handlers/userService/user.service';
import {UtilService} from '../../services/utilService/util.service';
import {BootstrapNotifyService} from '../../services/bootstrap-notify/bootstrap-notify.service';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.css']
})
export class TransactionComponent implements OnInit {
  breadCrumb: any;
  transactions: any[] = [];
  showData = true;
  public loadingData = false;
  orderDetail = null;
  currentUser = null;
  loaders = {
    loading: false
  };
  constructor(private navigatorService: NavigatorService,
              private userService: UserService, private utilService: UtilService,
              private alertService: BootstrapNotifyService) { }

  ngOnInit() {
    this.currentUser = this.utilService.getAuthUser();
    this.getTransactions();
    this.breadCrumb  = {
      name: 'Transactions',
      parent: 'Report',
      subLink: null
    };
  }
  gotoURL(url) {
    this.navigatorService.navigateUrl(url);
  }

  getTransactions() {
    this.showData = false;
    this.loadingData = true;
    this.userService.getTransactions(this.currentUser._id)
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.transactions = res.data;
        this.utilService.startDatatable('transaction-list');
        this.loadingData = false;
        this.showData = true;
      }, error => {
        console.log('Error ', error);
        this.utilService.startDatatable('transaction-list');
        this.loadingData = false;
        this.showData = true;
      });
  }
  public viewDetails(transaction, i) {
    this.loaders.loading = false;
    $(`#btn-${i}`).addClass('d-none');
    $('.loaders-btn').addClass('d-none');
    $(`#load-${i}`).removeClass('d-none');
    this.alertService.info('Loading transaction details, please wait!');
    this.userService.getOrdersByRef(transaction.orderReference).subscribe((res: IResponse) => {
      console.log('Response ', res);
      this.loaders.loading = true;
      this.orderDetail = res.data;
      $(`#btn-${i}`).removeClass('d-none');
      $(`#load-${i}`).addClass('d-none');
      this.utilService.openModal('transactionDetails');
    }, error => {
      console.log('Error ', error);
      this.alertService.error('Unable to fetch transaction detail');
      $(`#btn-${i}`).removeClass('d-none');
      $(`#load-${i}`).addClass('d-none');
    });
  }
}
