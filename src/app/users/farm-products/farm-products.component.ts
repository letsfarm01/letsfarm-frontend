import { Component, OnInit } from '@angular/core';
import {NavigatorService} from '../../services/navigatorService/navigator.service';
import {IResponse} from '../../interfaces/iresponse';
import {UserService} from '../../services/api-handlers/userService/user.service';
import {UtilService} from '../../services/utilService/util.service';

@Component({
  selector: 'app-farm-products',
  templateUrl: './farm-products.component.html',
  styleUrls: ['./farm-products.component.css']
})
export class FarmProductsComponent implements OnInit {
  breadCrumb: any;
  transactions: any[] = [];
  showData = true;
  public loadingData = false;
  currentUser = null;

  constructor(private navigatorService: NavigatorService,
              private userService: UserService, private utilService: UtilService) {
  }

  ngOnInit() {
    this.currentUser = this.utilService.getAuthUser();
    this.getOrders();
    this.breadCrumb = {
      name: 'Orders',
      parent: 'Farm Products',
      subLink: null
    };
  }

  gotoURL(url) {
    this.navigatorService.navigateUrl(url);
  }

  getOrders() {
    this.showData = false;
    this.loadingData = true;
    this.userService.getTransactions(this.currentUser._id)
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.transactions = res.data;
        this.utilService.startDatatable('transaction-list');
        this.loadingData = false;
        this.showData = true;
      }, error => {
        console.log('Error ', error);
        this.utilService.startDatatable('transaction-list');
        this.loadingData = false;
        this.showData = true;
      });
  }
}
