import {AfterViewInit, Component, OnInit} from '@angular/core';
import {UtilService} from '../../services/utilService/util.service';
import {BootstrapNotifyService} from '../../services/bootstrap-notify/bootstrap-notify.service';
import {UserService} from '../../services/api-handlers/userService/user.service';
import {IResponse} from '../../interfaces/iresponse';
import {AuthService} from '../../services/authService/auth.service';
import {NavigatorService} from '../../services/navigatorService/navigator.service';
import {EventsService} from '../../services/eventServices/event.service';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, AfterViewInit {
  currentUser = null;
  breadCrumb: any;
  banks: any[] = [];
  addressBooks: any[] = [];
  activeAddress = null;
  basic_info: any;
  loaders = {
    loadingImg: false,
    saving: false
  };
  password = {
    old_password: null,
    new_password: null,
    confirm_password: null,
    userId: null
  };
  updating = false;
  business_info = {
    company_name: null,
    company_address: null,
    company_phone: null,
    company_email: null,
    userId: null,
    _id: null
  };
  bank_info = {
    account_name: null,
    account_number: null,
    // bvn: null,
    bank_name: 'Access Bank',
    bank_code: null,
  userId: null

};
  selectedBank = [];
  kin_info = {
    name: null,
    address: null,
    email: null,
    relationship: null,
    userId: null,
    phone_number: null
  };
  modal = {
    title: 'Add a New Address',
    btnTxt: 'Save Address'
  };
  addressBook = {
    first_name: null,
    last_name: null,
    address: null,
    phone_number: null,
    alt_phone_number: null,
    address_line: null,
    additional_information: null,
    nearest_bus_stop: null,
    city: null,
    country: null,
    state: null,
    postal_code: null,
    userId: null
  };
  constructor(private utilService: UtilService, private alertService: BootstrapNotifyService,
              private navigatorService: NavigatorService,
              private eventService: EventsService,
              private route: ActivatedRoute,
              private userService: UserService, private authService: AuthService) {
  }

  ngOnInit() {
    this.currentUser = this.utilService.getAuthUser();
    this.basic_info = this.currentUser;
    this.password.userId = this.currentUser._id;
    this.breadCrumb  = {
      name: 'My Profile',
      parent: 'Home',
      subLink: null
    };
    console.log('currentUser ', this.currentUser);
    this.getUserProfile();
    this.getUserBusinessInfo();
    this.getUserBankInfo();
    this.getUserNextKin();
    this.initPicker();
    this.getBanks();
    this.getAddressBooks();
  }
  ngAfterViewInit() {
    if (this.route.snapshot.paramMap.get('shipping_address')
      && this.route.snapshot.paramMap.get('shipping_address') === 'shipping_address') {
      console.log('HELLO WORLD , trigger click');
      // $('.nav-link a[href="#shippingAddress"]').tab('show');
      $('#shipping_address_tab').trigger('click');
    }
  }
  public getUserProfile() {
    this.userService.getUser(this.currentUser._id).subscribe((res: IResponse) => {
      console.log('Res ', res);
      this.basic_info = res.data;
    }, error => {
      console.log(error);
    });
  }
  public getBanks() {
    this.userService.getBanks().subscribe((res: IResponse) => {
      console.log('Res ', res);
      this.banks = res.data;
      // this.bank_info.bank_name = this.banks[0]._id;
    }, error => {
      console.log(error);
    });
  }

  public getUserBusinessInfo() {
    this.userService.getBusinessInfo(this.currentUser._id).subscribe((res: IResponse) => {
      console.log('Res ', res);
      this.business_info = res.data;
    }, error => {
      console.log(error);
    });
  }
  public getUserBankInfo() {
    this.userService.getBankInfo(this.currentUser._id).subscribe((res: IResponse) => {
      console.log('Res ', res);
      this.bank_info = res.data;
    }, error => {
      console.log(error);
    });
  }

  public getUserNextKin() {
    this.userService.getNextKin(this.currentUser._id).subscribe((res: IResponse) => {
      console.log('Res ', res);
      this.kin_info = res.data;
    }, error => {
      console.log(error);
    });
  }

  initPicker() {
    setTimeout(() => {
      (<any>$('.datepicker')).datetimepicker({
        icons: {
          time: 'fa fa-clock-o',
          date: 'fa fa-calendar',
          up: 'fa fa-chevron-up',
          down: 'fa fa-chevron-down',
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-screenshot',
          clear: 'fa fa-trash',
          close: 'fa fa-remove'
        },
        format: 'YYYY-MM-DD',
        maxDate: '2020-01-01'
      });
    }, 400);
  }
  changePassword() {
    console.log('PAssword ', this.password);
    if (this.currentUser.auth_type === 'NO_AUTH') {
      if (!this.password.old_password) {
        return this.alertService.error('Password is required!');
      }
    }
    if (!this.password.new_password) {
      return this.alertService.error('Enter new password!');
    } else if (!this.password.confirm_password) {
      return this.alertService.error('Confirm new password!');
    } else if (this.password.confirm_password !== this.password.new_password) {
      return this.alertService.error('New password does not match confirm password!');
    } else if (this.password.new_password.length < 10) {
      return this.alertService.error('New password length must be greater than 9!');
    } else {
      this.password.userId = this.currentUser._id;
      this.userService.changePassword(this.password)
        .subscribe((res: IResponse) => {
        console.log('Res ', res);
        this.alertService.success(res.msg || 'Password changed successfully!');
        this.authService.logOut();
        this.navigatorService.navigateUrl('/');
      }, error => {
        this.alertService.error(error.error.msg || 'Unable to change password!');
        console.log('Error ', error);
      });

    }
  }
  public updateField(id) {
    setTimeout(() => {
      const val = $('#' + id).val();
      this.basic_info[id] = val;
    }, 1000);
  }
  public updateBasicInfo() {
    console.log('Basic info');
    if (!this.basic_info.first_name) {
      return this.alertService.error('First name is required!');
    } else if (!this.basic_info.last_name) {
      return this.alertService.error('Last name is required!');
    } else if (!this.basic_info.phone_number) {
      return this.alertService.error('Phone number is required!');
    } else if (!this.basic_info.email) {
      return this.alertService.error('Email address is required!');
    } else {
      delete this.basic_info.password;
      this.userService.updateUser(this.basic_info).subscribe((res: IResponse) => {
        this.alertService.success(res.msg || 'User profile updated successfully!');
        this.eventService.broadcast('PROFILE_UPDATED', res.data);
        this.getUserProfile();
      }, error => {
        console.log('Error ', error);
        this.alertService.error(error.error.msg || 'Unable to update user profile');
      });
    }
  }
  public updateBusinessInfo() {
    console.log('Business info');
    if (!this.business_info.company_name) {
      return this.alertService.error('Company name is required!');
    } else if (!this.business_info.company_email) {
      return this.alertService.error('Company email is required!');
    } else {
      this.business_info.userId = this.currentUser._id;
      this.userService.updateBusinessInfo(this.business_info, this.currentUser._id)
        .subscribe((res: IResponse) => {
        this.alertService.success(res.msg || 'Business info updated successfully!');
        this.getUserBusinessInfo();
      }, error => {
        console.log('Error ', error);
        this.alertService.error(error.error.msg || 'Unable to update business information');
      });
    }
  }
  public startLoading(event) {
    if (event === 'showLoading') {
      setTimeout(() => {
        this.loaders.loadingImg = false;
      }, 3000);
      this.loaders.loadingImg = true;
    }
  }
  public saveUpdate() {
    this.updateBasicInfo();
  }
  changeBank() {
    this.selectedBank = this.banks.filter((bank) => bank.name.toLowerCase() === this.bank_info.bank_name.toLowerCase());
    this.bank_info.bank_code = this.selectedBank[0].code;
  }
  public updateBankInfo() {
    this.updating = true;
    this.selectedBank = this.banks.filter((bank) => bank.name.toLowerCase() === this.bank_info.bank_name.toLowerCase());
    if(this.selectedBank[0]) {
      this.bank_info.bank_code = this.selectedBank[0].code;
    }
    console.log('Bank info', this.bank_info.bank_code, this.selectedBank[0], this.selectedBank[0].code);
    if (!this.bank_info.bank_name) {
      this.updating = false;
      return this.alertService.error('Bank name is required!');
    } else if (!this.bank_info.account_name) {
      this.updating = false;
      return this.alertService.error('Account name is required!');
    } /*else if (!this.bank_info.bvn) {
      this.updating = false;
      return this.alertService.error('BVN is required!');
    } */else if (!this.bank_info.account_number) {
      this.updating = false;
      return this.alertService.error('Account number is required!');
    } else {
      this.bank_info.userId = this.currentUser._id;
      this.userService.updateBankInfo(this.bank_info, this.currentUser._id)
        .subscribe((res: IResponse) => {
          this.updating = false;
          this.alertService.success(res.msg || 'Bank info updated successfully!');
        this.getUserBankInfo();
      }, error => {
        console.log('Error ', error);
          this.updating = false;
          this.alertService.error(error.error.msg || 'Unable to update bank information');
      });
    }
  }
  public updateNextOfKin() {
    console.log('Next of Kin');
    if (!this.kin_info.name) {
      return this.alertService.error('Next of kin name is required!');
    } else if (!this.kin_info.phone_number) {
      return this.alertService.error('Next of kin phone number is required!');
    } else if (!this.kin_info.email) {
      return this.alertService.error('Next of kin email is required!');
    } else if (!this.kin_info.address) {
      return this.alertService.error('Next of kin address is required!');
    }  else if (!this.kin_info.relationship) {
      return this.alertService.error('Relationship with next of kin is required!');
    } else {
      this.kin_info.userId = this.currentUser._id;
      this.userService.updateNextKin(this.kin_info, this.currentUser._id)
        .subscribe((res: IResponse) => {
        this.alertService.success(res.msg || 'Next of kin updated successfully!');
        this.getUserBankInfo();
      }, error => {
        console.log('Error ', error);
        this.alertService.error(error.error.msg || 'Unable to update next of kin');
      });
    }
  }
  updateUsername() {
    if (!this.basic_info.username) {
      this.alertService.error('Username not set, first name will be make username');
    } else {
      this.userService.updateUser({_id: this.basic_info._id,
        username: this.basic_info.username || this.basic_info.first_name})
        .subscribe((res: IResponse) => {
          console.log('Res ', res);
          this.alertService.success('Username updated successfully!');
          this.getUserProfile();
        }, error => {
          console.log('Error ', error);
        });
    }
  }
  getAddressBooks() {
    this.userService.getAddressBooks(this.currentUser._id).subscribe((res: IResponse) => {
      this.addressBooks = res.data;
    }, error => {});
  }
  createAddressBook() {
    this.modal = {
      title: 'Add a New Address',
      btnTxt: 'Save Address'
    };
    this.activeAddress = null;
    this.resetAddressBook();
    this.utilService.openModal('AddressBook');
  }
  triggerEditAddress(address) {
    this.resetAddressBook();
    this.activeAddress = this.addressBook = JSON.parse(JSON.stringify(address));
    this.modal = {
      title: 'Update New Address',
      btnTxt: 'Update Address'
    };
    this.utilService.openModal('AddressBook');
  }
  setDefaultAddress(address) {
    this.userService.setDefaultAddressBook({shippingAddressId: address._id}, this.currentUser._id)
      .subscribe((res: IResponse) => {
      this.alertService.success(res.msg || 'Shipping address set as default successfully');
      this.getAddressBooks();
    }, error => {
      this.alertService.error(error.error.msg || 'Unable to update shipping address as default');
    });
  }
  deleteAddress(address) {
    this.utilService.confirmAction(() => {
      this.userService.deleteAddressBook(address._id).subscribe((res: IResponse) => {
        this.alertService.success(res.msg || 'Shipping address deleted successfully');
        this.getAddressBooks();
      }, error => {
        this.alertService.error(error.error.msg || 'Unable to delete shipping address');
      });
    });
  }
  resetAddressBook() {
    this.addressBook = {
      first_name: null,
      last_name: null,
      address: null,
      phone_number: null,
      alt_phone_number: null,
      address_line: null,
      additional_information: null,
      nearest_bus_stop: null,
      city: null,
      country: 'Nigeria',
      state: null,
      postal_code: null,
      userId: null
    };
  }
  saveUpdateShippingAddress() {
    if (!this.addressBook.first_name || !this.addressBook.last_name) {
      return this.alertService.error('Enter the first name and last name');
    } else if (!this.addressBook.phone_number && !this.addressBook.alt_phone_number) {
      return this.alertService.error('Enter phone number for this address');
    } else if (!this.addressBook.address_line) {
      return this.alertService.error('Enter the full address');
    } else {
      this.loaders.saving = true;
      if (this.activeAddress) {
        this.updateAddress();
      } else {
        this.addressBook.userId = this.currentUser._id;
        this.userService.saveAddressBook(this.addressBook).subscribe((res: IResponse) => {
          this.alertService.success(res.msg || 'Shipping address added successfully');
          this.getAddressBooks();
          this.loaders.saving = false;
          this.resetAddressBook();
          this.utilService.closeModal('AddressBook');
        }, error => {
          this.alertService.error(error.error.msg || 'Unable to save shipping address');
          this.loaders.saving = false;
        });
      }
    }
  }
  updateAddress() {
    this.addressBook.userId = this.currentUser._id;
    this.userService.updateAddressBook(this.addressBook, this.activeAddress._id).subscribe((res: IResponse) => {
      this.alertService.success(res.msg || 'Shipping address updated successfully');
      this.getAddressBooks();
      this.loaders.saving = false;
      this.resetAddressBook();
      this.utilService.closeModal('AddressBook');
    }, error => {
      this.alertService.error(error.error.msg || 'Unable to update shipping address');
      this.loaders.saving = false;
    });
  }
}
