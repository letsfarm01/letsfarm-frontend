import {AfterViewInit, Component, OnInit} from '@angular/core';
import {NavigatorService} from '../../services/navigatorService/navigator.service';
import {IResponse} from '../../interfaces/iresponse';
import {UserService} from '../../services/api-handlers/userService/user.service';
import {UtilService} from '../../services/utilService/util.service';
import {BootstrapNotifyService} from '../../services/bootstrap-notify/bootstrap-notify.service';
import {ActivatedRoute} from '@angular/router';
import {EventsService} from '../../services/eventServices/event.service';

@Component({
  selector: 'app-wallet-history',
  templateUrl: './wallet-history.component.html',
  styleUrls: ['./wallet-history.component.css']
})
export class WalletHistoryComponent implements OnInit, AfterViewInit {
  breadCrumb: any;
  walletTransactions: any[] = [];
  showData = true;
  public loadingData = false;
  currentUser = null;
  loaders = {
    loading: false,
    saving: false

  };
  wallet_balance = 0.0;
  bank_info = null;
  paymentId = null;
  withdrawFund = {
    amount: 0,
    userId: null,
    password: null
  };
  constructor(private navigatorService: NavigatorService,
              private userService: UserService, private utilService: UtilService,
              private route: ActivatedRoute,
              private eventService: EventsService,
              private alertService: BootstrapNotifyService) {
    this.paymentId = this.route.snapshot.paramMap.get('paymentId') || null;
    if (this.paymentId) {
      this.verifyPayment();
    }
  }

  ngOnInit() {
    this.currentUser = this.utilService.getAuthUser();
    this.getTransactions();
    this.breadCrumb  = {
      name: 'Wallet-History',
      parent: 'Report',
      subLink: null
    };
  }
  ngAfterViewInit() {
    this.getWallet();
    this.getUserBankInfo();
  }
  getWallet() {
    this.userService.getWallet(this.currentUser._id).subscribe((res: IResponse) => {
      this.wallet_balance = res.data.wallet_balance || 0.0;
    });
  }
  private verifyPayment() {
    this.userService.verifyWalletFundingPayment({transactionId: this.paymentId})
      .subscribe((res: IResponse) => {
        this.alertService.success(res.msg || 'Payment verified successfully!');
        this.eventService.broadcast('WALLET_UPDATED');
        this.navigatorService.navigateUrl('/user/wallet-history');
      }, error => {
        this.alertService.error(error.error.msg || 'Unable to verify transaction payment');
      });
  }

  public getUserBankInfo() {
    this.userService.getBankInfo(this.currentUser._id).subscribe((res: IResponse) => {
      console.log('Res ', res);
      this.bank_info = res.data;
    }, error => {
      console.log(error);
    });
  }
  gotoURL() {
    this.utilService.openModal('fundWallet');
  }

  getTransactions() {
    this.showData = false;
    this.loadingData = true;
    this.userService.getWalletTransactions(this.currentUser._id)
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.walletTransactions = res.data;
        this.utilService.startDatatable('walletTransaction-list');
        this.loadingData = false;
        this.showData = true;
      }, error => {
        console.log('Error ', error);
        this.utilService.startDatatable('walletTransaction-list');
        this.loadingData = false;
        this.showData = true;
      });
  }
  public viewDetails(walletTransaction, i) {
    $(`#btn-${i}`).addClass('d-none');
    $('.loaders-btn').addClass('d-none');
    $(`#load-${i}`).removeClass('d-none');
    this.alertService.info('Order api still in progress');
    this.userService.getOrdersByRef(walletTransaction.orderReference).subscribe((res: IResponse) => {
      console.log('Response ', res);
      $(`#btn-${i}`).removeClass('d-none');
      $(`#load-${i}`).addClass('d-none');
    }, error => {
      console.log('Error ', error);
      $(`#btn-${i}`).removeClass('d-none');
      $(`#load-${i}`).addClass('d-none');
    });
  }

  public triggerFundWallet() {
    this.utilService.openModal('fundWallet');
  }
  public triggerFundWithdraw() {
    this.utilService.openModal('withdrawFund')
  }

  public validateAmount() {
    if (Math.sign(this.withdrawFund.amount) < 0) {
      this.withdrawFund.amount = this.withdrawFund.amount * -1;
    } else if (Math.sign(this.withdrawFund.amount) > 10000000) {
      this.withdrawFund.amount = 10000000;
    } else {
      return;
    }
  }
  public proceedToWithdraw() {
    console.log('DATA ', this.withdrawFund);
    if (this.withdrawFund.amount < 0 || this.withdrawFund.amount === 0) {
      return this.alertService.warning('Invalid Amount');
    } else if (this.withdrawFund.amount < 100 ) {
      return this.alertService.warning('Amount can not be less than NGN 100');
    } else if (this.withdrawFund.amount > 10000000 ) {
      return this.alertService.warning('Amount cannot exceed NGN 10,000,000');
    } else if (!this.withdrawFund.password ) {
      return this.alertService.warning('Account password is required to continue');
    } else {
      this.loaders.saving = true;
      this.withdrawFund.userId = this.currentUser._id;
      this.userService.requestToWithdraw(this.withdrawFund).subscribe((res: IResponse) => {
        this.loaders.saving = false;
        console.log('Withdrawing ', res);
        this.utilService.closeModal('withdrawFund');
        this.alertService.success(res.msg || 'Request to withdraw sent successfully!');
        this.eventService.broadcast('WALLET_UPDATED');
      }, error => {
        this.loaders.saving = false;
        this.alertService.error(error.error.msg || 'Unable to withdraw from your wallet at this moment, please try again later!', 'right');
      });
    }
  }

}
