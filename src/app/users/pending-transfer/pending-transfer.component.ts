import { Component, OnInit } from '@angular/core';
import {NavigatorService} from '../../services/navigatorService/navigator.service';
import {IResponse} from '../../interfaces/iresponse';
import {UserService} from '../../services/api-handlers/userService/user.service';
import {UtilService} from '../../services/utilService/util.service';
import {BootstrapNotifyService} from '../../services/bootstrap-notify/bootstrap-notify.service';
import {CacheService} from "../../services/cacheService/cache.service";


@Component({
  selector: 'app-pending-transfer',
  templateUrl: './pending-transfer.component.html',
  styleUrls: ['./pending-transfer.component.css']
})
export class PendingTransferComponent implements OnInit {
  breadCrumb: any;
  pendingTransfers: any[] = [];
  showData = true;
  public loadingData = false;
  currentUser = null;
  loaders = {
    loading: false
  };
  constructor(private navigatorService: NavigatorService,
              private cacheService: CacheService,
              private userService: UserService, private utilService: UtilService,
              private alertService: BootstrapNotifyService) { }

  ngOnInit() {
    this.currentUser = this.utilService.getAuthUser();
    this.getPendingBankTransfers();
    this.breadCrumb  = {
      name: 'Pending',
      parent: 'Bank Transfer',
      subLink: null
    };
  }
  gotoURL(url) {
    this.navigatorService.navigateUrl(url);
  }

  getPendingBankTransfers() {
    this.showData = false;
    this.loadingData = true;
    this.userService.getPendingTransfers(this.currentUser._id)
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.pendingTransfers = res.data;
        this.utilService.startDatatable('pending-bank-transfer');
        this.loadingData = false;
        this.showData = true;
      }, error => {
        console.log('Error ', error);
        this.utilService.startDatatable('pending-bank-transfer');
        this.loadingData = false;
        this.showData = true;
      });
  }
  public uploadProof(transaction) {
    this.cacheService.setStorage('BankRef', transaction.orderReference);
    this.navigatorService.navigateUrl('/user/payment/bank-transfer/' + transaction.orderReference );
  }
}
