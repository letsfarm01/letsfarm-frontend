import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyFarmsComponent } from './my-farms.component';

describe('MyFarmsComponent', () => {
  let component: MyFarmsComponent;
  let fixture: ComponentFixture<MyFarmsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyFarmsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyFarmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
