import { Component, OnInit } from '@angular/core';
import {NavigatorService} from "../../services/navigatorService/navigator.service";
import {UserService} from "../../services/api-handlers/userService/user.service";
import {IResponse} from "../../interfaces/iresponse";
import {UtilService} from "../../services/utilService/util.service";
import {BootstrapNotifyService} from "../../services/bootstrap-notify/bootstrap-notify.service";

@Component({
  selector: 'app-my-farms',
  templateUrl: './my-farms.component.html',
  styleUrls: ['./my-farms.component.css']
})
export class MyFarmsComponent implements OnInit {
  breadCrumb: any;
  farmHistory: any[] = [];
  activeSponsor: any[] = [];
  archiveSponsors: any[] = [];
  roiData: any[] = [];
  showData = true;
  showData2 = true;
  public loadingData = false;
  public loadingData2 = false;
  currentUser = null;
  constructor(private navigatorService: NavigatorService,
              private alertService: BootstrapNotifyService,
              private userService: UserService, private utilService: UtilService) { }

  ngOnInit() {
    this.currentUser = this.utilService.getAuthUser();
    this.breadCrumb  = {
      name: 'Active Sponsor & History',
      parent: 'Home',
      subLink: null
    };
    this.getActiveSponsor();
    this.getFarmHistory();
    this.getArchive();
  }
  gotoURL(url) {
    this.navigatorService.navigateUrl(url);
  }

  getFarmHistory() {
    this.showData = false;
    this.loadingData = true;
    this.userService.getFarmHistory(this.currentUser._id)
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.farmHistory = res.data;
        this.utilService.startDatatable('farm-history');
        this.loadingData = false;
        this.showData = true;
      }, error => {
        console.log('Error ', error);
        this.utilService.startDatatable('farm-history');
        this.loadingData = false;
        this.showData = true;
      });
  }

  getActiveSponsor() {
    this.showData2 = false;
    this.loadingData2 = true;
    this.userService.getActiveSponsorship(this.currentUser._id)
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.activeSponsor = res.data;
        this.utilService.startDatatable('active-farm-history');
        this.loadingData2 = false;
        this.showData2 = true;
      }, error => {
        console.log('Error ', error);
        this.utilService.startDatatable('active-farm-history');
        this.loadingData2 = false;
        this.showData2 = true;
      });
  }

  getArchive() {
    this.userService.getArchiveSponsorship({email: this.currentUser.email})
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.archiveSponsors = res.data;
        this.utilService.startDatatable('archive-farm-sponsor');
      }, error => {
        console.log('Error ', error);
        this.utilService.startDatatable('archive-farm-sponsor');
      });
  }
  cancelInvestment(sponsor) {
    this.utilService.actionRequireConfirmed(() => {
      this.userService.cancelSponsorship(this.currentUser._id, sponsor._id).subscribe((res: IResponse) => {
        console.log('RES', res);
        this.getActiveSponsor();
        this.getFarmHistory();
        this.alertService.success(res.msg || 'Sponsorship canceled successfully');
      }, error => {
        console.log('Error ', error);
        this.alertService.error(error.error.msg || 'Unable to cancel this sponsorship');
      });
    });
  }

  viewROI(data) {
    this.roiData = data;
    this.utilService.openModal('foodTrading');
  }
  closeTradingModal() {
    this.utilService.closeModal('foodTrading');
    this.roiData = [];
  }
}
