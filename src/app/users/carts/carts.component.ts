import { Component, OnInit } from '@angular/core';
import {IResponse} from '../../interfaces/iresponse';
import {UtilService} from '../../services/utilService/util.service';
import {BootstrapNotifyService} from '../../services/bootstrap-notify/bootstrap-notify.service';
import {UserService} from '../../services/api-handlers/userService/user.service';
import { Subject } from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {NavigatorService} from '../../services/navigatorService/navigator.service';
import {CacheService} from '../../services/cacheService/cache.service';
import {EventsService} from '../../services/eventServices/event.service';


@Component({
  selector: 'app-carts',
  templateUrl: './carts.component.html',
  styleUrls: ['./carts.component.css']
})
export class CartsComponent implements OnInit {
  breadCrumb: any;
  show = true;
  currentUser = null;
  checkOutType = 'farmCart';
  transactionData = {
    total: 0,
    subTotal: 0,
    charge: 0
  };
  cartCountNumber = 0;
  totalCartValue = 0.0;
  walletBalance = 0.0;
  cartCurrency = 'NGN';
  productCartCurrency = 'NGN';
  totalProductCartValue = 0;
  selectedPaymentGateway = null;
  userQuestionUpdate = new Subject<string>();
  loaders = {
    saving: false,
    proceeding: false
  };
  allCarts: any [] = [];
  allProductCarts: any [] = [];
  paymentChannels: any [] = [];
  public loadingData = false;
  showData = true;
  defaultAddress = null;
  paymentId = null;
  cartInFarm = true;
  constructor(private userService: UserService,
              private utilService: UtilService,
              private route: ActivatedRoute,
              private cacheService: CacheService,
              private eventService: EventsService,
              private navigatorService: NavigatorService,
              private alertService: BootstrapNotifyService) {
    this.userQuestionUpdate
      .distinctUntilChanged()
      .debounceTime(500).subscribe((event: any) => {
      console.log('Hello world', event);
      this.validateQty(event.cart, event.id);
    });
    this.getPaymentChannel();
    this.paymentId = this.route.snapshot.paramMap.get('paymentId') || null;
    if (this.paymentId && this.route.snapshot.paramMap.get('transactionType') === 'farm_transaction') {
      this.verifyFarmPayment();
    } else if (this.paymentId && this.route.snapshot.paramMap.get('transactionType') === 'product_transaction') {
      this.verifyProductPayment();
    } else {}
  }

  ngOnInit() {
    this.breadCrumb  = {
      name: 'Cart',
      parent: 'Home',
      subLink: null
    };
    this.currentUser = this.utilService.getAuthUser();
    this.getFarmCarts();
    this.getFarmCartCount();
    this.getWallet();
    this.getProductCarts();
    this.getDefaultShippingAddress();
  }
  getWallet() {
    this.userService.getWallet(this.currentUser._id).subscribe((res: IResponse) => {
      this.walletBalance = res.data.wallet_balance || this.currentUser.wallet_balance || 0.0;
    });
  }
  private verifyFarmPayment() {
    this.userService.verifyPayment({transactionId: this.paymentId})
      .subscribe((res: IResponse) => {
      this.alertService.success(res.msg || 'Payment verified successfully!');
      this.navigatorService.navigateUrl('/user/my-farm');
    }, error => {
      this.alertService.error(error.error.msg || 'Unable to verify transaction payment');
    });
  }
  private verifyProductPayment() {
    this.userService.verifyProductPurchasePayment({transactionId: this.paymentId})
      .subscribe((res: IResponse) => {
      this.alertService.success(res.msg || 'Payment verified successfully!');
      this.navigatorService.navigateUrl('/user/farm-products/orders');
    }, error => {
      this.alertService.error(error.error.msg || 'Unable to verify transaction payment');
    });
  }
  getFarmCarts() {
    this.showData = false;
    this.loadingData = true;
    this.userService.getCarts(this.currentUser._id)
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.allCarts = res.data;
        this.loadingData = false;
        this.showData = true;
        this.calculateTotal();
        this.cartInFarm = true;
      }, error => {
        console.log('Error ', error);
        this.loadingData = false;
        this.cartInFarm = false;
        this.showData = true;
      });
  }
  getProductCarts() {
    // this.showData = false;
    // this.loadingData = true;
    this.userService.getProductCarts(this.currentUser._id)
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.allProductCarts = res.data;
        // this.loadingData = false;
        // this.showData = true;
        this.cartInFarm = this.allCarts.length >= 1;
        this.calculateProductCartTotal();
      }, error => {
        console.log('Error ', error);
        // this.loadingData = false;
        // this.showData = true;
      });
  }
  getFarmCartCount() {
    this.userService.countCart(this.currentUser._id)
      .subscribe((res: IResponse) => {
      console.log('Res ', res.data);
        this.cartCountNumber = res.data;
      }, error => {      });
  }
  getDefaultShippingAddress() {
    this.userService.getDefaultAddressBook(this.currentUser._id)
      .subscribe((res: IResponse) => {
      console.log('defaultAddress ', res.data);
        this.defaultAddress = res.data;
      }, error => {
      this.defaultAddress = null;
      });
  }

  public updateCart(cart) {
   if (cart && cart.farmId) {
     if (cart.farmId.farm_status.name === 'Sold Out') {
       this.alertService.info('This item is already out of stock, please remove from cart!');
       return false;
     }
     const qty = cart.quantity;
     this.userService.updateCart(cart._id, {quantity: qty, farmId: cart.farmId._id}).subscribe((res) => {
       console.log('Res ', res);
       this.calculateTotal();
     }, error => {
       console.log('Error ', error);
       this.alertService.error(error.error.msg || 'Unable to increase item quantity');
     });
   } else if (cart && cart.productId) {
     if (cart.productId.product_status.name === 'Out of Stock') {
       this.alertService.info('This item is already out of stock, please remove from cart!');
       return false;
     }
     const qty = cart.quantity;
     this.userService.updateCart(cart._id, {quantity: qty, productId: cart.productId._id}).subscribe((res) => {
       console.log('Res ', res);
       this.calculateProductCartTotal();
     }, error => {
       console.log('Error ', error);
       this.alertService.error(error.error.msg || 'Unable to increase item quantity');
     });
   }
  }
  deleteCart(cart) {
    this.utilService.confirmAction(() => {
      this.userService.deleteCart(cart._id)
        .subscribe((res: IResponse) => {
          console.log('Res ', res);
          this.eventService.broadcast('CART_UPDATED', res);
          this.getFarmCarts();
          this.getProductCarts();
          this.getFarmCartCount();
        }, error => {
          console.log('Errro ', error);
        });
    });
  }
  validateQty(cart, id) {
    const value: any = $(`#${id}`).val();
    if (value === 0) {
      $(`#${id}`).val(1);
      cart.quantity = 1;
    }
    cart.quantity = Math.abs(value) || 1;
    cart.total_price = (parseInt(cart.quantity, 10) * parseFloat(cart.farmId.amount_to_invest));
    this.updateCart(cart);
  }
  calculateTotal() {
    this.totalCartValue = 0.0;
    if (this.allCarts.length) {
      this.cartCurrency = this.allCarts[0].farmId.currency;
      this.allCarts.forEach(cart => {
        this.totalCartValue  +=  parseFloat(cart.farmId.amount_to_invest) * parseInt(cart.quantity, 10);
      });
    }
  }
  calculateProductCartTotal() {
    this.totalProductCartValue = 0.0;
    if (this.allProductCarts.length) {
      this.productCartCurrency = this.allProductCarts[0].productId.currency;
      this.allProductCarts.forEach(cart => {
        this.totalProductCartValue  +=  parseFloat(cart.productId.price) * parseInt(cart.quantity, 10);
      });
    }
  }
  checkoutModal(id, type = 'farmCart') {
    if (type === 'productCart') {
      this.calculateCharge(this.totalProductCartValue);
      this.checkOutType = 'productCart';
      this.validateProductCart(() => {
        this.utilService.openModal(id);
      });
    } else {
      this.calculateCharge(this.totalCartValue);
      this.checkOutType = 'farmCart';
      this.validateCart(() => {
        this.utilService.openModal(id);
      });
    }
  }
  calculateCharge(amount) {
    this.userService.chargeTransaction({amount: amount})
      .subscribe((res: IResponse) => {
      console.log('Response ', res);
      this.transactionData = res.data;
      }, (err) => {
      console.log('Error ', err);
      this.calculateCharge(amount);
      });
    }
  validateCart(cb) {
    let invalidCart = false;
    for (const cart of this.allCarts) {
      console.log('CART ', cart);
      if (cart.farmId && cart.farmId.farm_status && cart.farmId.farm_status
        && (!cart.farmId.farm_status.can_sponsor || cart.farmId.farm_status.name === 'Sold Out')) {
        this.alertService.error('Some or One of the items in your cart is either out of stock or it cannot be sponsor at the moment!,' +
          ' please consider removing them');
        invalidCart = true;
        break;
      }
    }
    console.log('Cart Invalid ', invalidCart);
    if (!invalidCart) {
      cb();
    }
  }
  validateProductCart(cb) {
    let invalidCart = false;
    for (const cart of this.allProductCarts) {
      console.log('CART ', cart);
      if (cart.productId && cart.productId.product_status && cart.productId.product_status
        && (!cart.productId.product_status.can_order || cart.productId.product_status.name.toLowerCase() !== 'in stock')) {
        this.alertService.error('Some or One of the items in your farm product cart is either out of stock or it cannot ' +
          'be order at the moment!, please consider removing them');
        invalidCart = true;
        break;
      }
    }
    console.log('Cart Invalid ', invalidCart);
    if (!invalidCart) {
      cb();
    }
  }
  showCharge() {
    if (!this.transactionData) {
      return this.alertService.error('Unable to calculate paystack transaction charge, please use other payment method!');
    }
    this.show = false;
  }
  closeShowCharge() {
    this.show = true;
  }
  public proceedToPayment() {
    this.loaders.proceeding = true;
    this.eventService.broadcast('CART_UPDATED', '');
    console.log('this.selectedPaymentGateway', this.selectedPaymentGateway);
    if (this.selectedPaymentGateway.name.toLowerCase() === 'e-wallet' && this.walletBalance <= 0) {
      this.loaders.proceeding = false;
      return this.alertService.info('Insufficient wallet balance to carry out transaction!');
    }
    this.alertService.info('Validating cart and waiting for payment URL');
    if (this.checkOutType === 'productCart') {
      this.processProductCart();
    } else {
      this.userService.checkoutFarmShop({payment_gateway: this.selectedPaymentGateway._id})
        .subscribe((res: IResponse) => {
          console.log('Response : ', res);
          this.loaders.proceeding = false;
          if (this.selectedPaymentGateway.name.toLowerCase().includes('paystack')) {
            this.alertService.info('Redirecting to paystack');
            window.location.assign(res.data.authorization_url);
          } else if (this.selectedPaymentGateway.name.toLowerCase().includes('bank transfer')) {
            this.cacheService.setStorage('BankRef', res.data.transactionRef);
            this.utilService.closeModal('choosePaymentGateway');
            this.navigatorService.navigateUrl('/user/payment/bank-transfer/' + res.data.transactionRef || '111111111' );
          } else {
            this.utilService.closeModal('choosePaymentGateway');
            this.navigatorService.navigateUrl('/user/my-farm');
          }
        }, error => {
          console.log('ERROR ', error);
          this.alertService.error(error.error.msg || 'Unable to proceed to payment page!');
          this.loaders.proceeding = false;
        });
    }
  }
  processProductCart() {
    this.userService.checkoutFarmProduct({payment_gateway: this.selectedPaymentGateway._id, shipping_address: this.defaultAddress._id})
      .subscribe((res: IResponse) => {
        console.log('Response : ', res);
        this.loaders.proceeding = false;
        if (this.selectedPaymentGateway.name.toLowerCase().includes('paystack')) {
          this.alertService.info('Redirecting to paystack');
          window.location.assign(res.data.authorization_url);
        } else if (this.selectedPaymentGateway.name.toLowerCase().includes('bank transfer')) {
          this.cacheService.setStorage('BankRef', res.data.transactionRef);
          this.utilService.closeModal('choosePaymentGateway');
          this.navigatorService.navigateUrl('/user/payment/bank-transfer/' + res.data.transactionRef || '111111111' );
        } else {
          this.utilService.closeModal('choosePaymentGateway');
          this.navigatorService.navigateUrl('/user/farm-products/orders');
        }
      }, error => {
        console.log('ERROR ', error);
        this.alertService.error(error.error.msg || 'Unable to proceed to payment page!');
        this.loaders.proceeding = false;
      });
  }
  getPaymentChannel() {
    this.userService.getPaymentChannels()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.paymentChannels = res.data;
      }, error => {
      // this.getPaymentChannel();
      });
  }
  gotoURL(url) {
    this.navigatorService.navigateUrl(url);
  }
  changeDefaultAddress() {
    this.navigatorService.navigateUrl('/user/profile/shipping_address');
  }
}
