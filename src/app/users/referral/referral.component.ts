import { Component, OnInit } from '@angular/core';
import {UtilService} from '../../services/utilService/util.service';
import { environment as ENV } from '../../../environments/environment';
import {BootstrapNotifyService} from '../../services/bootstrap-notify/bootstrap-notify.service';
import {UserService} from '../../services/api-handlers/userService/user.service';
import {IResponse} from '../../interfaces/iresponse';
@Component({
  selector: 'app-referral',
  templateUrl: './referral.component.html',
  styleUrls: ['./referral.component.css']
})
export class ReferralComponent implements OnInit {
  breadCrumb: any;
  loaders = {
    sending: false,
    loading: false
  };
  refer = {
    email: null
  };
  referrals = [];
  currentUser = null;
  url = window.location.origin + '/sign-up/';
  constructor(private utilService: UtilService,
              private userService: UserService,
              private alertService: BootstrapNotifyService) { }

  ngOnInit() {
    this.breadCrumb  = {
      name: 'Referral',
      parent: 'Others',
      subLink: null
    };
    this.currentUser = this.utilService.getAuthUser();
    console.log('Current ', this.currentUser);
    this.getReferrerList();
  }
  openReferFriend(id) {
    this.utilService.openModal(id);
  }
  proceedToRefer() {
    if (!this.refer.email) {
      return this.alertService.warning('Friend email address is required!');
    } else if (this.refer.email && !this.refer.email.match(ENV.EMAIL_VALIDATION)) {
      return this.alertService.warning('Invalid email address!');
    } else {
      this.loaders.sending = true;
      this.userService.referUser(this.refer).subscribe((res: IResponse) => {
        this.loaders.sending = false;
        this.alertService.success(res.msg || 'Friend referred successfully');
        this.utilService.closeModal('referFriend');
        this.getReferrerList();
      }, error => {
        this.loaders.sending = false;
        this.alertService.error(error.error.msg);
      });
    }
  }
  getReferrerList() {
    this.loaders.loading = true;
    this.userService.getUserReferrers(this.currentUser._id).subscribe((res: IResponse) => {
      this.loaders.loading = false;
      this.referrals = res.data;
    }, error => {
      this.loaders.loading = false;
      this.alertService.error(error.error.msg);
    });
  }
  handleCashout() {
    this.alertService.info('Cashout in progress');
  }
  copyMessage (val: string) {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.alertService.info('Copied to clipboard');
  }
}
