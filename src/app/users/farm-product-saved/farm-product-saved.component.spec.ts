import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmProductSavedComponent } from './farm-product-saved.component';

describe('FarmProductSavedComponent', () => {
  let component: FarmProductSavedComponent;
  let fixture: ComponentFixture<FarmProductSavedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmProductSavedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmProductSavedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
