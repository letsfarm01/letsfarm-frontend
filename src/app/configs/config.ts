export const configs = {
  months: [
    {name: 'January', value: 1},
    {name: 'February', value: 2},
    {name: 'March', value: 3},
    {name: 'April', value: 4},
    {name: 'May', value: 5},
    {name: 'June', value: 6},
    {name: 'July', value: 7},
    {name: 'August', value: 8},
    {name: 'September', value: 9},
    {name: 'October', value: 10},
    {name: 'November', value: 11},
    {name: 'December', value: 12},
  ],
  years: [
    {name: '2019', value: '2019'},
    {name: '2020', value: '2020'},
    {name: '2021', value: '2021'},
    {name: '2022', value: '2022'},
    {name: '2023', value: '2023'},
    ],
  sponsorStatus: [
    {name: 'ALL', value: 'ALL'},
    {name: 'STARTED', value: 'STARTED'},
    {name: 'PROCESSING', value: 'PROCESSING'},
    {name: 'COMPLETED', value: 'COMPLETED'},
    {name: 'WITHDREW', value: 'WITHDREW'},
    {name: 'CANCELED', value: 'CANCELED'},
    {name: 'REFUNDED', value: 'REFUNDED'},
    {name: 'PART_WITHDRAW', value: 'PART_WITHDRAW'}
  ]
};
// export configs;
