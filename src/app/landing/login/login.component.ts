import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { environment as ENV } from '../../../environments/environment';
import {BootstrapNotifyService} from '../../services/bootstrap-notify/bootstrap-notify.service';
import {UserService} from '../../services/api-handlers/userService/user.service';
import {IResponse} from '../../interfaces/iresponse';
import {NavigatorService} from '../../services/navigatorService/navigator.service';
import {CacheService} from '../../services/cacheService/cache.service';
import {AuthService} from '../../services/authService/auth.service';
import {FacebookLoginProvider, GoogleLoginProvider, SocialAuthService, SocialUser} from 'angularx-social-login';
import {Subject} from 'rxjs/Subject';
import {takeUntil} from 'rxjs/operators';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, AfterViewInit, OnDestroy {
  public EMAIL_VALIDATION: any =  ENV.EMAIL_VALIDATION;
  public YOUR_SITE_KEY = ENV.RECAPTCHA_SITE_KEY;
  public credentials = {
    email: null,
    password: null
  };
  public userDetails: any;
  loaders = {
    login: false,
    showResetLink: false
  };
  user: SocialUser;
  loggedIn: boolean;
  private ngUnsubscribe = new Subject();
  @ViewChild('captchaRef') captchaRef: any;
  constructor(private bootstrapNotify: BootstrapNotifyService,
              private userService: UserService,
              private cacheService: CacheService,
              private navigateService: NavigatorService,
              private _authService: SocialAuthService,
              private authService: AuthService) {
    const redirectURL = this.cacheService.getSession('redirectUrl');
    const productsInCart = this.cacheService.getStorage(ENV.PRODUCTS_TO_CART);
    this.authService.logOut();
    this.cacheService.setSession('redirectUrl', redirectURL);
    this.cacheService.setStorage(ENV.PRODUCTS_TO_CART, productsInCart);
  }
  ngOnInit(): void {
    this._authService.signOut(true).catch(() => {});
    this.userDetails = null;
  }
  ngAfterViewInit() {
    try {
      this._authService.authState.pipe(
        takeUntil(this.ngUnsubscribe)
      ).subscribe((user) => {
        console.log('USERS ', user);
        this.loaders.login = false;
        if (user) {
          this.userService.socialLogin({accessToken: user.authToken, auth_type: user.provider}).subscribe((response: IResponse) => {
            this.loaders.login = false;
            this.bootstrapNotify.success(response.msg || 'Login successful!');
            const redirectURL = this.cacheService.getSession('redirectUrl');
            if (redirectURL &&
              (redirectURL.includes('localhost')
                || redirectURL.includes('https://test.letsfarm.com.ng')
                || redirectURL.includes('https://letsfarm.com.ng'))
              && response.data.user.role.toLowerCase() === 'user') {
              window.location.href = redirectURL;
              this.cacheService.deleteSession('redirectUrl');
            } else {
              this.cacheService.deleteSession('redirectUrl');
              this.navigateService.navigateUrl(`/${response.data.user.role.toLowerCase()}/dashboard`);
            }
          }, error => {
            this.bootstrapNotify.error(error.error && error.error.msg || 'Unable to login using social authenticator', 'right');
            this.loaders.login = false;
            console.info('Error => ', error);
          });
        }
      }, error => {
        console.log('AUTH ERROR ', error);
        this.loaders.login = false;
      });
    } catch (e) {}
  }
  public loginProcess() {
    this.loaders.login = true;
    this.cacheService.deleteSession(ENV.TOKEN);
    this.cacheService.deleteStorage(ENV.TOKEN);
    if (!this.credentials.email || !this.credentials.password) {
      this.bootstrapNotify.info('Provide login details!');
      this.loaders.login = false;
    } else {
      this.userService.auth(this.credentials).subscribe((response: IResponse) => {
        console.log('Response', response);
        this.loaders.login = false;

        this.bootstrapNotify.success(response.msg || 'Login successful!');
        const redirectURL = this.cacheService.getSession('redirectUrl');
        if (redirectURL &&
          (redirectURL.includes('localhost')
            || redirectURL.includes('https://test.letsfarm.com.ng')
            || redirectURL.includes('https://letsfarm.com.ng'))
          && response.data.user.role.toLowerCase() === 'user') {
          window.location.href = redirectURL;
          this.cacheService.deleteSession('redirectUrl');
        } else if (response.data.user.role.toLowerCase() === 'blogger') {
          this.navigateService.navigateUrl(`/super/web-content/education-management`);
        } else if (response.data.user.role.toLowerCase() === 'accountant') {
          this.navigateService.navigateUrl(`/super/dashboard`);
        } else {
          this.navigateService.navigateUrl(`/${response.data.user.role.toLowerCase()}/dashboard`);
          this.cacheService.deleteSession('redirectUrl');
        }
      }, error => {
        this.expired();
        this.bootstrapNotify.error(error.error && error.error.msg || 'Unable to login', 'right');
        this.loaders.login = false;
        console.info('Error => ', error);
      });
    }
  }
  public openUrl() {
    this.navigateService.navigateUrl('/user');
  }
  resolved(response: string) {
    this.loaders.login = true;
    console.log(`Resolved captcha with response: ${response}`);
    if (response) {
      console.log(response); // Will print the token
      this.userService.authRecaptcha({token: response}).subscribe((res: IResponse) => {
        console.log('Response ', response);
        if (res.data.success && res.data.score > 0.1) {
          this.loginProcess();
        } else {
          this.bootstrapNotify.error('Unable to login!');
          this.loaders.login = false;
          this.expired();
        }
      }, error => {
        this.bootstrapNotify.error('Unable to login!');
        console.log('Error ', error);
        this.loaders.login = false;
        this.expired();
      });
    } else {
      this.loaders.login = false;
      // this.bootstrapNotify.info('Verifying tht!');
    }
  }
  recaptchaExecute() {
    console.log('RECAPCHA ', this.captchaRef);
    this.loaders.login = true;
    this.captchaRef.execute();
  }
  expired() {
    this.captchaRef.reset();
    this.captchaRef.reset();
  }
  signInWithGoogle(): void {
    try {
      if (!this.loaders.login) {
        this.loaders.login = true;
        this.cancelLoaders(2000);
        this._authService.signIn(GoogleLoginProvider.PROVIDER_ID).catch(() => {});
      } else {
        this.bootstrapNotify.info('Login in progress!');
        this.cancelLoaders(0);
      }
    } catch (e) {}
  }

  signInWithFB(): void {
    try {
      if (!this.loaders.login) {
        this.loaders.login = true;
        this.cancelLoaders(2000);
        this._authService.signIn(FacebookLoginProvider.PROVIDER_ID).catch(() => {});
      } else {
        this.bootstrapNotify.info('Login in progress!');
        this.cancelLoaders(0);
      }
    } catch (e) {}
  }
  public ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
    this._authService.signOut(true).catch(() => {});
  }
  logoutAndRoute() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
    this._authService.signOut(true).then(() => {
      this.navigateService.navigateUrl('/register');
    }).catch(() => {
      this.navigateService.navigateUrl('/register');
    });
  }
  cancelLoaders(time = 2000) {
    setTimeout(() => {
      this.loaders.login = false;
    }, time);
  }
}
