import { Component, OnInit } from '@angular/core';
import {UserService} from "../../services/api-handlers/userService/user.service";
import {IResponse} from "../../interfaces/iresponse";
import {BootstrapNotifyService} from "../../services/bootstrap-notify/bootstrap-notify.service";
import {NavigatorService} from "../../services/navigatorService/navigator.service";

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
  topPost = null;
  loaders = {
    loading: false,
    posts: false
  };
  blogger = null;
  limit = 12;
  page =  1;
  searching = false;
  blogs = [];
  videoFilter = null;
  searchTitle = null;
  constructor(private userService: UserService, private alertService: BootstrapNotifyService,
              private navigatorService: NavigatorService) {
    this.getTopPost();
    this.getPosts();
  }

  ngOnInit() {
    this.getBlogTypes();
  }
  getTopPost() {
    this.loaders.loading = true;
    this.userService.getTopBlog().subscribe((res: IResponse) => {
      this.topPost = res.data;
      this.loaders.loading = false;
    }, error => {
      this.loaders.loading = false;
    });
  }
  getPosts() {
    this.loaders.posts = true;
    console.log('POST ', this.page);
    this.userService.getBlogs(this.limit, this.page).subscribe((res: IResponse) => {
      this.blogs = res.data.data;
      this.blogger = res.data;
      this.loaders.posts = false;
    }, error => {
      this.loaders.posts = false;
    });
  }

  getNextBlog() {
    this.page += 1;
    this.getPosts();
  }
  getPreviousBlog() {
    this.page -= 1;
    this.getPosts();
  }
  searchBlog() {
    if (this.searchTitle) {
      this.loaders.posts = true;
      this.searching = true;
      this.userService.searchBlogByTitle({title: this.searchTitle})
        .subscribe((res: IResponse) => {
          this.blogs = res.data.data;
          this.loaders.posts  = false;
          this.searching = false;
        }, error => {
          console.log('Error Blog', error);
          this.alertService.error(error.error.msg || 'Unable to search blog by title');
          this.loaders.posts  = false;
          this.searching = false;
        });
    }
  }
  getBlogsByCategory(e) {
    if (!e) {
      return false;
    }
    this.loaders.posts  = true;
    console.log('E ', e);
    this.userService.getBlogsByCategory({edu_category: e._id})
      .subscribe((res: IResponse) => {
        this.blogs = res.data.data;
        this.loaders.posts  = false;
      }, error => {
        console.log('Error Educ', error);
        this.alertService.error(error.error.msg || 'Unable to filter posts');
        this.loaders.posts  = false;
      });
  }
  getBlogTypes() {
    this.userService.getBlogCategories()
      .subscribe((res: IResponse) => {
        this.videoFilter = res.data;
      }, error => {
        console.log('Error ', error);
      });
  }
  readPost(blog) {
    this.navigatorService.navigateUrl('/blog/' + blog._id + '/' + blog.title.split(' ').join('_'));
  }
}
