import {AfterViewInit, Component, OnInit} from '@angular/core';
// import * as $ from 'jquery';
import Wow from 'wow.js';
import {NavigatorService} from '../../services/navigatorService/navigator.service';
import {IResponse} from '../../interfaces/iresponse';
import {UserService} from '../../services/api-handlers/userService/user.service';
import {UtilService} from '../../services/utilService/util.service';
import {environment as ENV} from '../../../environments/environment';
import {BootstrapNotifyService} from '../../services/bootstrap-notify/bootstrap-notify.service';
import {EventsService} from "../../services/eventServices/event.service";
// import 'owl.carousel';
declare var $: any;
// declare var jQuery: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterViewInit {
  loading = false;
  loadingProduct = false;
  farmShopsAvailable = true;
  allShops: any[] = [];
  allProducts: any[] = [];
  newsLetter = {
    email: null
  };
  currentUser = null;
  PATTERN = null;
  interval = null;
  constructor(private navigatorService: NavigatorService,
              private userService: UserService,
              private utilService: UtilService,
              private eventService: EventsService,
              private alertService: BootstrapNotifyService) {
    this.getFarmShops();
    this.getFarmShopStatus();
  }

  ngOnInit() {
    this.randomiseImage();
    $('#page-loader').fadeOut('fast', function() {
      $(this).remove();
    });
    this.PATTERN = ENV.EMAIL_VALIDATION;
    this.currentUser = this.utilService.getAuthUser();

  }
  ngAfterViewInit() {
    const wow: any = new Wow({
      boxClass: 'wow',
      animateClass: 'animated',
      offset: 50,
      mobile: false,
      live: true
    });
    wow.init();
    // Smooth Scrolling
    /*$('.scrolling, .navbar-brand').on('click', function(event) {
      const $anchor = $(this);
      $('html, body').stop().animate({
        scrollTop: $($anchor.attr('href')).offset().top - 75
      }, 1000, 'easeInOutExpo');
      event.preventDefault();
    });
    $('.nav.navbar-nav > li').on('click', 'a', function(event) {
      const $anchor = $(this);
      $('html, body').stop().animate({
        scrollTop: $($anchor.attr('href')).offset().top - 75
      }, 1000, 'easeInOutExpo');
      event.preventDefault();
    });
*/

/*


    $('.navbar-toggler ').on('click', function() {
      console.log('Hello Man');
      if ($('.navbar-collapse.collapse').hasClass('in')) {
        $('.navbar-collapse.collapse').removeClass('in');
      } else {
        $('.navbar-collapse.collapse').addClass('in');
      }
    });

    // close collapse nav after select
    $('.btn-sing, .navbar-nav > li ').on('click', 'a', function() {
      console.log('Hello woMan');
      $('.navbar-collapse.collapse').removeClass('in');
    });
*/




    setTimeout(() => {
      $(('.testimonial-slides') as any).owlCarousel({
        singleItem: true,
        items: 1,
        autoplay: true,
        loop: true,
        smartSpeed: 1000,
        dots: true,
        pagination: true,
      });
    }, 100);
    this.getFarmProducts();
  }
  randomiseImage() {
    clearInterval(this.interval);
    const selectorId = $('#intro-hero');
    selectorId.removeClass('intro-random-1');
    selectorId.removeClass('intro-random-2');
    selectorId.removeClass('intro-random-3');
    const rand = this.utilService.randomNumber(1, 4);
    const claName = `intro-random-${rand}`;
    console.log('ClassName ', claName);
    selectorId.addClass(`intro-random-${rand}`);
    this.interval = setInterval(() => {
      const random = this.utilService.randomNumber(1, 4);
      selectorId.removeClass('intro-random-1');
      selectorId.removeClass('intro-random-2');
      selectorId.removeClass('intro-random-3');
      selectorId.addClass(`intro-random-${random}`);
    }, 10000);
  }
  public loadThisShop(item) {
    this.navigatorService.navigateUrl('/farm-shop/' + item._id + '/details');
  }



  getFarmShops() {
    this.loading = true;
    this.userService.getActiveShop()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.allShops = res.data.data.slice(0, 4);
        this.loading = false;
      }, error => {
        console.log('Error ', error);
        this.loading = false;
      });
  }

  getFarmProducts() {
    this.loadingProduct = true;
    this.userService.getDistinctFarmProducts()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.allProducts = res.data.data.slice(0, 4);
        this.loadingProduct = false;
      }, error => {
        console.log('Error ', error);
        this.loadingProduct = false;
      });
  }


  getFarmShopStatus() {
    this.userService.checkIfFarmIsAvailable()
      .subscribe((res: IResponse) => {
      console.log('RES ', res);
        this.farmShopsAvailable = res.msg === 'FOUND';
      }, error => {
        console.log('Error ', error);
      });
  }
  openNewsletter() {
    this.utilService.openModal('newsletterModal');
  }
  saveNewsletter() {
    if (!this.newsLetter.email || !this.newsLetter.email.match(this.PATTERN)) {} else {
      this.userService.addNewSubscriber(this.newsLetter).subscribe(() => {
        this.utilService.closeModal('newsletterModal');
        this.alertService.success('You will get a mail from us once a new farm is available');
      }, () => {
        this.alertService.error('Unable to save this email, please use another email.');
      });
    }
  }
  openProduct(product) {
    console.log('PRODUCT ', product);
    if (product.product_status.name.toLowerCase() === 'in stock') {
      this.navigatorService.navigateUrl('/farm-product/' + product._id + '/details');
    } else {
      return this.alertService.error('Product is out of stock');
    }
  }
  addToCart(product) {
    if (product.product_status.name.toLowerCase() === 'in stock') {
      this.handlePurchase(product);
    } else {
      return this.alertService.error('Product is out of stock');
    }
  }
  handlePurchase(farmProduct) {
    // this.alertService.info('Adding to cart!');
    if (!this.currentUser) {
      this.utilService.keepInCache(farmProduct, 1);
    } else {
      this.userService.addProductToCart({
        userId: this.currentUser._id,
        productId: farmProduct._id,
        quantity: 1
      }).subscribe((res: IResponse) => {
        this.alertService.success('Product added to cart successfully');
        this.eventService.broadcast('CART_MODIFIED');
      }, error => {
        this.alertService.error(error.error.msg || 'Unable to add to cart!');
      });
    }
  }
}
