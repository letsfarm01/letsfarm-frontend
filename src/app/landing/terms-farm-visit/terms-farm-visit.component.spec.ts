import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermsFarmVisitComponent } from './terms-farm-visit.component';

describe('TermsFarmVisitComponent', () => {
  let component: TermsFarmVisitComponent;
  let fixture: ComponentFixture<TermsFarmVisitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermsFarmVisitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermsFarmVisitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
