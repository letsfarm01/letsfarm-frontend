import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {NavigatorService} from '../../services/navigatorService/navigator.service';
import {UserService} from '../../services/api-handlers/userService/user.service';
import {IResponse} from '../../interfaces/iresponse';
import {BootstrapNotifyService} from '../../services/bootstrap-notify/bootstrap-notify.service';
import {UtilService} from '../../services/utilService/util.service';
import {CacheService} from '../../services/cacheService/cache.service';

@Component({
  selector: 'app-farm-shop-details',
  templateUrl: './farm-shop-details.component.html',
  styleUrls: ['./farm-shop-details.component.css']
})
export class FarmShopDetailsComponent implements OnInit {
  farmShopId = null;
  quantity = 1;
  shopData: any = null;
  loading = false;
  adding = false;
  currentUser = null;
  percentageLeft = 0;
  constructor(private route: ActivatedRoute,
              private userService: UserService,
              private utilService: UtilService,
              private cacheService: CacheService,
              private alertService: BootstrapNotifyService,
              private navigatorService: NavigatorService) { }

  ngOnInit() {
    this.farmShopId = this.route.snapshot.paramMap.get('shopId') || null;
    if (!this.farmShopId) {
      this.navigatorService.navigateUrl('/farm-shop');
    } else {
      this.getFarmShop();
    }
    this.currentUser = this.utilService.getAuthUser();
  }
  getFarmShop() {
    this.loading = false;
    this.userService.getShop(this.farmShopId).subscribe((res: IResponse) => {
      console.log('RES ', res);
      this.loading = false;
      this.shopData = res.data;
      this.percentageLeft = Math.round((this.shopData.remaining_in_stock / this.shopData.total_in_stock ) * 100);
    }, error => {
      this.alertService.error(error.error.msg || 'Unable to get farm shop by ID');
      console.log('Error ', error);
      this.loading = false;
    });
  }
  handleInvestment(farmShop) {
    if (!this.currentUser) {
      const redirectUrl = window.location.href;
      this.cacheService.setSession('redirectUrl', redirectUrl);
      this.navigatorService.navigateUrl('/login');
    } else {
      this.alertService.info('Adding Item to cart!');
      this.adding = true;
      this.userService.addShopToCart({
        userId: this.currentUser._id,
        farmId: farmShop._id,
        quantity: this.quantity
      }).subscribe((res: IResponse) => {
        this.navigatorService.navigateUrl('/user/cart');
        this.adding = false;
      }, error => {
        this.adding = false;
        console.log('Error ', error);
        this.alertService.error(error.error.msg || 'Unable to add to cart!');
      });
    }
  }
  validateQty(id) {
    const value: any = $(`#${id}`).val();
    if (value === 0) {
      $(`#${id}`).val(1);
      this.quantity = 1;
    }
    this.quantity = Math.abs(value) || 1;
  }

}
