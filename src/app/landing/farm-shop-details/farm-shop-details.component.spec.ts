import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmShopDetailsComponent } from './farm-shop-details.component';

describe('FarmShopDetailsComponent', () => {
  let component: FarmShopDetailsComponent;
  let fixture: ComponentFixture<FarmShopDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmShopDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmShopDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
