import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {NavigatorService} from '../../services/navigatorService/navigator.service';
import {UserService} from '../../services/api-handlers/userService/user.service';
import {IResponse} from '../../interfaces/iresponse';
import {BootstrapNotifyService} from '../../services/bootstrap-notify/bootstrap-notify.service';
import {UtilService} from '../../services/utilService/util.service';
import {CacheService} from '../../services/cacheService/cache.service';
import {EventsService} from "../../services/eventServices/event.service";

@Component({
  selector: 'app-farm-product-details',
  templateUrl: './farm-product-details.component.html',
  styleUrls: ['./farm-product-details.component.css']
})
export class FarmLProductDetailsComponent implements OnInit {
  farmProductId = null;
  quantity = 1;
  productData: any = null;
  loading = false;
  adding = false;
  currentUser = null;
  percentageLeft = 0;
  constructor(private route: ActivatedRoute,
              private userService: UserService,
              private utilService: UtilService,
              private cacheService: CacheService,
              private eventService: EventsService,
              private alertService: BootstrapNotifyService,
              private navigatorService: NavigatorService) { }

  ngOnInit() {
    this.farmProductId = this.route.snapshot.paramMap.get('productId') || null;
    if (!this.farmProductId) {
      this.navigatorService.navigateUrl('/farm-products');
    } else {
      this.getFarmProduct();
    }
    this.currentUser = this.utilService.getAuthUser();
  }
  getFarmProduct() {
    this.loading = false;
    this.userService.getProduct(this.farmProductId).subscribe((res: IResponse) => {
      console.log('RES ', res);
      this.loading = false;
      this.productData = res.data;
      this.percentageLeft = Math.round((this.productData.remaining_in_stock / this.productData.total_in_stock ) * 100);
    }, error => {
      this.alertService.error(error.error.msg || 'Unable to get farm product by ID');
      console.log('Error ', error);
      this.loading = false;
    });
  }
  handlePurchase(farmProduct) {
    this.alertService.info('Adding to cart!');
    if (!this.currentUser) {
      this.utilService.keepInCache(farmProduct, this.quantity);
    } else {
      this.alertService.info('Adding Item to cart!');
      this.adding = true;
      this.userService.addProductToCart({
        userId: this.currentUser._id,
        productId: farmProduct._id,
        quantity: this.quantity
      }).subscribe((res: IResponse) => {
        this.navigatorService.navigateUrl('/user/cart');
        this.adding = false;
        this.eventService.broadcast('CART_MODIFIED');
      }, error => {
        this.adding = false;
        console.log('Error ', error);
        this.alertService.error(error.error.msg || 'Unable to add to cart!');
      });
    }
  }
  validateQty(id) {
    const value: any = $(`#${id}`).val();
    if (value === 0) {
      $(`#${id}`).val(1);
      this.quantity = 1;
    }
    this.quantity = Math.abs(value) || 1;
  }
}
