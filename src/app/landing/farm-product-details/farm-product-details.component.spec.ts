import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmProductDetailsComponent } from './farm-product-details.component';

describe('FarmProductDetailsComponent', () => {
  let component: FarmProductDetailsComponent;
  let fixture: ComponentFixture<FarmProductDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmProductDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmProductDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
