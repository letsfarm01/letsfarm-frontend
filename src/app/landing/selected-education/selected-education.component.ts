import { Component, OnInit } from '@angular/core';
import {NavigatorService} from '../../services/navigatorService/navigator.service';
import {IResponse} from '../../interfaces/iresponse';
import {UserService} from '../../services/api-handlers/userService/user.service';
import {ActivatedRoute} from '@angular/router';
import {UtilService} from '../../services/utilService/util.service';

@Component({
  selector: 'app-selected-education',
  templateUrl: './selected-education.component.html',
  styleUrls: ['./selected-education.component.css']
})
export class SelectedEducationComponent implements OnInit {
  loading = false;
  education = null;
  selectedPost = null;
  genURL = null;
  IP = null;
  currentUser = null;
  likes = 0;
  dislikes = 0;
  constructor(private navigatorService: NavigatorService,
              private route: ActivatedRoute,
              private utilService: UtilService,
              private userService: UserService) {
    this.currentUser = this.utilService.getAuthUser();
    this.selectedPost = this.route.snapshot.paramMap.get('id') || null;
    this.getIP();
    if (!this.selectedPost) {
      this.previous();
    } else {
      this.genURL = window.location.href;
      this.getEducations();
    }
  }

  ngOnInit() {
    this.getUserFeeling();
  }

  public previous() {
    this.navigatorService.navigateUrl('/education');
  }
  getEducations() {
    this.loading  = true;
    this.userService.getEducationById(this.selectedPost)
      .subscribe((res: IResponse) => {
        // https://www.youtube.com/embed/aqhrMh3p_9U
        this.education = res.data;
        if (this.education.video_link && !this.education.video_link.includes('/embed/')) {
          const url = this.education.video_link.split('v=')[1];
          this.education.video_link = `https://www.youtube.com/embed/${url}`;
        }
        this.likes = this.education.total_likes;
        this.dislikes = this.education.total_dislikes;
          this.loading  = false;
      }, error => {
        console.log('Error Educ', error);
        this.loading  = false;
      });
  }
  onNewComment(e) {
    console.log('Eben ', e);
    this.userService.saveEducationComment({
      educationId: this.selectedPost,
      message: e.message}).subscribe((res: IResponse) => {

    });
  }
  getIP() {
    $.getJSON('https://ipapi.co/json/', (data) => {
      this.IP = JSON.parse(JSON.stringify(data, null, 2));
      console.log('IP ', this.IP, this.IP.ip);
    });
  }
  feelingPost(feel) {
    console.log('POST ', this.IP);
   this.userService.saveEducationFeeling({
     userId: this.currentUser._id,
     actionType: feel,
     educationId:	this.selectedPost,
     userIdentity: this.IP ? this.IP.ip : null
   }).subscribe(() => {
     this.userService.getEducationById(this.selectedPost)
       .subscribe((res: IResponse) => {
         const education = res.data;
         this.likes = education.total_likes;
         this.dislikes = education.total_dislikes;
         }, error => {});
   });
  }
  getUserFeeling() {
   if (this.currentUser) {
     this.userService.getMyFeeling({
       userId: this.currentUser._id,
       educationId: this.selectedPost
     }).subscribe((res: IResponse) => {
       console.log('MY FEELING ', res);
     });
   }
  }

}
