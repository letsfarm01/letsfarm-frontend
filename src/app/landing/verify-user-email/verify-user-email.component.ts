import { Component, OnInit } from '@angular/core';
import { environment as ENV } from '../../../environments/environment';
import {BootstrapNotifyService} from '../../services/bootstrap-notify/bootstrap-notify.service';
import {UserService} from '../../services/api-handlers/userService/user.service';
import {IResponse} from '../../interfaces/iresponse';
import {NavigatorService} from '../../services/navigatorService/navigator.service';
import * as JWT_DECODE from 'jwt-decode';
import {CacheService} from '../../services/cacheService/cache.service';
import {ActivatedRoute, ActivatedRouteSnapshot, Router} from '@angular/router';
import {AuthService} from '../../services/authService/auth.service';
import {EventsService} from '../../services/eventServices/event.service';

@Component({
  selector: 'app-verify-user-email',
  templateUrl: './verify-user-email.component.html',
  styleUrls: ['./verify-user-email.component.css']
})
export class VerifyUserEmailComponent implements OnInit {
  public EMAIL_VALIDATION: any =  ENV.EMAIL_VALIDATION;
  public credentials = {
    email: null,
    password: null
  };
  public userDetails: any;
  loaders = {
    login: false,
    showResetLink: false
  };
  constructor(private bootstrapNotify: BootstrapNotifyService,
              private userService: UserService,
              private navigatorService: NavigatorService,
              private route: ActivatedRoute,
              private authService: AuthService,
              private cacheService: CacheService,
              private eventService: EventsService) {
    this.verifyAccount();
  }

  ngOnInit() {
  }
  public verifyAccount() {
    this.authService.logOut();
    const token = this.route.snapshot.paramMap.get('token') || null;
    const userId = this.route.snapshot.paramMap.get('userId') || null;
    console.log('TOKEN ', token.toString().charAt(0));
    if (token.toString().charAt(0) !== 'Q') {
      this.navigatorService.navigateUrl('/login');
    }

    this.cacheService.deleteSession(ENV.TOKEN);
    this.cacheService.deleteSession(ENV.USERTOKEN);
    this.cacheService.deleteStorage(ENV.USERTOKEN);
    this.cacheService.deleteSession(ENV.TOKEN);
    this.userService.verifyAccount({token, userId}).subscribe((response: IResponse) => {
      console.log('Response', response);
      this.bootstrapNotify.success(response['msg'] || 'Please proceed to login');
      this.navigatorService.navigateUrl('/login');
    }, error => {
      this.navigatorService.navigateUrl('/login');
      this.bootstrapNotify.error(error.error.msg || 'Unable to activate account', 'right');
      console.info('Error => ', error);
    });

  }
  public loginProcess() {
    this.loaders.login = true;
    this.cacheService.deleteSession(ENV.TOKEN);
    this.cacheService.deleteStorage(ENV.TOKEN);
    if (!this.credentials.email || !this.credentials.password) {
      this.bootstrapNotify.info('Provide login details!');
      this.loaders.login = false;
    } else {
      this.userService.auth(this.credentials).subscribe((response: IResponse) => {
        console.log('Response', response);
        this.loaders.login = false;
        this.navigatorService.navigateUrl(`/${response.data.user.role.toLowerCase()}/dashboard`);
      }, error => {
        this.bootstrapNotify.error(error.error && error.error.msg || 'Unable to login', 'right');
        this.loaders.login = false;
        console.info('Error => ', error);
      });
    }
  }
  public openUrl() {
    this.navigatorService.navigateUrl('/user');
  }
}
