import { Component, OnInit } from '@angular/core';
import {NavigatorService} from '../../services/navigatorService/navigator.service';
import {IResponse} from "../../interfaces/iresponse";
import {UserService} from "../../services/api-handlers/userService/user.service";
import {BootstrapNotifyService} from "../../services/bootstrap-notify/bootstrap-notify.service";

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.css']
})
export class EducationComponent implements OnInit {
  videoFilter: any[] = [];
  loading = false;
  searching = false;
  educations = [];
  searchTitle = null;
  constructor(private navigatorService: NavigatorService, private userService: UserService,
              private alertService: BootstrapNotifyService) { }

  ngOnInit() {
    this.getEducations();
    this.getEducationTypes();
  }
  public openNextPage(education) {
    this.navigatorService.navigateUrl('/education/' + education._id + '/' + education.title.split(' ').join('_'));
  }
  searchEducation() {
    if (this.searchTitle) {
      this.loading = true;
      this.searching = true;
      this.userService.searchEducationByTitle({title: this.searchTitle})
        .subscribe((res: IResponse) => {
          this.educations = res.data.data;
          this.loading  = false;
          this.searching = false;
        }, error => {
          console.log('Error Educ', error);
          this.alertService.error(error.error.msg || 'Unable to search posts by title');
          this.loading  = false;
          this.searching = false;
        });
    }
  }
  getEducationTypes() {
    this.userService.getEducationCategories()
      .subscribe((res: IResponse) => {
        this.videoFilter = res.data;
      }, error => {
        console.log('Error ', error);
      });
  }
  getEducations() {
    this.loading  = true;
    this.userService.getEducations()
      .subscribe((res: IResponse) => {
        this.educations = res.data.data;
        this.loading  = false;
      }, error => {
        console.log('Error Educ', error);
        this.loading  = false;
      });
  }
  getEducationsByCategory(e) {
    if (!e) {
      return false;
    }
    this.loading  = true;
    console.log('E ', e);
    this.userService.getEducationsByCategory({edu_category: e._id})
      .subscribe((res: IResponse) => {
        this.educations = res.data.data;
        this.loading  = false;
      }, error => {
        console.log('Error Educ', error);
        this.alertService.error(error.error.msg || 'Unable to filter posts');
        this.loading  = false;
      });
  }
}
