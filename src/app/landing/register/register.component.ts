import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import { environment as ENV } from '../../../environments/environment';
import {BootstrapNotifyService} from '../../services/bootstrap-notify/bootstrap-notify.service';
import {UserService} from '../../services/api-handlers/userService/user.service';
import {IResponse} from '../../interfaces/iresponse';
import {NavigatorService} from '../../services/navigatorService/navigator.service';
import * as JWT_DECODE from 'jwt-decode';
import {CacheService} from '../../services/cacheService/cache.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../services/authService/auth.service';
import {FacebookLoginProvider, GoogleLoginProvider, SocialAuthService} from 'angularx-social-login';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs/Subject';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})


export class RegisterComponent implements OnInit, AfterViewInit, OnDestroy {
  public EMAIL_VALIDATION: any =  ENV.EMAIL_VALIDATION;
  public user = {
    email: null,
    password: null,
    first_name: null,
    last_name: null,
    password_confirmation: null,
    phone_number: null,
    referral_email: null,
    channel_notice: null,
    accept_term: false,
    accessToken: null,
    auth_type: 'NO_AUTH'
  };
  private ngUnsubscribe = new Subject();
  public userDetails: any;
  loaders = {
    login: false,
    showResetLink: false
  };
  referralEmail = null;
  constructor(private bootstrapNotify: BootstrapNotifyService,
              private userService: UserService,
              private cacheService: CacheService,
              private navigateService: NavigatorService,
              private router: Router,
              private __authService: SocialAuthService,
              private authService: AuthService,
              private activeRoute: ActivatedRoute) {
    this.referralEmail = this.activeRoute.snapshot.paramMap.get('email') || this.cacheService.getStorage('referralEmail') || null;
    if (this.referralEmail) {
      this.user.referral_email = this.referralEmail;
      this.cacheService.setStorage('referralEmail', this.referralEmail);
    }
    const productsInCart = this.cacheService.getStorage(ENV.PRODUCTS_TO_CART);
    this.cacheService.setStorage(ENV.PRODUCTS_TO_CART, productsInCart);
  }
  ngOnInit(): void {
    console.log('TRY TO LOGOUT NOW');
    this.__authService.signOut(true).catch(() => {});
    this.userDetails = null;
  }
  ngAfterViewInit() {
    this.__authService.authState
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((user) => {
      if (user) {
        console.log('USERS from SOCIAL', user);
        this.user = {
          email: user.email,
          password: 'password',
          first_name: user.firstName,
          last_name: user.lastName,
          password_confirmation: 'password',
          phone_number: null,
          referral_email: null,
          channel_notice: null,
          accept_term: false,
          accessToken: user.authToken,
          auth_type: user.provider
        };
        this.bootstrapNotify.info('Please fill in missing information');
      }
    }, error => {
      console.log('AUTH ERROR ', error);
    });
  }
  public register() {
    this.loaders.login = true;
    this.cacheService.deleteSession(ENV.TOKEN);
    this.cacheService.deleteStorage(ENV.USERTOKEN);
    if (!this.user.accept_term) {
      this.bootstrapNotify.info('You must accept our terms and conditions to continue');
      this.loaders.login = false;
    } else if (!this.user.email) {
      this.bootstrapNotify.info('Email is Required');
      this.loaders.login = false;
    } else if (!this.user.phone_number) {
      this.bootstrapNotify.info('Phone Number is Required');
      this.loaders.login = false;
    } else if (!this.user.first_name) {
      this.bootstrapNotify.info('First Name is Required');
      this.loaders.login = false;
    } else if (!this.user.last_name) {
      this.bootstrapNotify.info('Last Name is Required');
      this.loaders.login = false;
    } else if (!this.user.password) {
      this.bootstrapNotify.info('Enter a secure password');
      this.loaders.login = false;
    }  else if (this.user.password !== this.user.password_confirmation) {
      this.bootstrapNotify.info('Password must match!');
      this.loaders.login = false;
    }  else if (this.user.referral_email && this.user.referral_email === this.user.email) {
      this.bootstrapNotify.info('You can\'t use yourself as referral!');
      this.loaders.login = false;
    } else {
      if (this.user.auth_type === 'NO_AUTH') {
        this.userService.register(this.user).subscribe((response: IResponse) => {
          console.log('Response', response);
          this.loaders.login = false;
          this.resetSignup();
          this.navigateService.navigateUrl(`/login`);
          this.bootstrapNotify.success(response.msg || 'Registration successful, please login');
          this.cacheService.deleteStorage('referralEmail');
        }, error => {
          this.bootstrapNotify.error(error.error.msg || 'Unable to register at this moment, please try again', 'right');
          this.loaders.login = false;
          console.info('Error => ', error);
        });
      } else {
        this.socialSignup();
      }
    }
  }
  socialSignup() {
    this.userService.socialRegister(this.user).subscribe((response: IResponse) => {
      console.log('Response', response);
      this.loaders.login = false;
      this.resetSignup();
      this.navigateService.navigateUrl(`/login`);
      this.bootstrapNotify.success(response.msg || 'Registration successful, signing in!!');
      this.cacheService.deleteStorage('referralEmail');
      // LOGIN after social sign in
      this.bootstrapNotify.success(response.msg || 'Login successful!');
      const redirectURL = this.cacheService.getSession('redirectUrl');
      if (redirectURL &&
        (redirectURL.includes('localhost')
          || redirectURL.includes('https://test.letsfarm.com.ng')
          || redirectURL.includes('https://letsfarm.com.ng'))
        && response.data.user.role.toLowerCase() === 'user') {
        window.location.href = redirectURL;
        this.cacheService.deleteSession('redirectUrl');
      } else {
        this.navigateService.navigateUrl(`/${response.data.user.role.toLowerCase()}/dashboard`);
        this.cacheService.deleteSession('redirectUrl');
      }
    }, error => {
      this.bootstrapNotify.error(error.error.msg || 'Unable to register at this moment, please try again', 'right');
      this.loaders.login = false;
      console.info('Error => ', error);
    });
  }
  public movePage(action, position, e) {
    e.preventDefault();
    $('.step-').addClass('d-none');
    $('.step-' + position).removeClass('d-none');
/*
    if(action === 'f') {
      $('.step-' + position).removeClass('d-none');
    } else {
      $('.step-' + position).removeClass('d-none');
    }*/
  }
  /*checkCompletion(action, position, e) {
    if (this.user.password && this.user.password_confirmation && this.user.email) {
      this.movePage(action, position, e);
      return true;
    } else {
      this.bootstrapNotify.info('Error in form, please fill all fields');
      return false;
    }
  }*/
  public resetSignup() {
    this.user = {
      email: null,
      password: null,
      first_name: null,
      last_name: null,
      password_confirmation: null,
      phone_number: null,
      referral_email: null,
      channel_notice: null,
      accept_term: false,
      accessToken: null,
      auth_type: 'NO_AUTH'
    };
  }

  signInWithGoogle(): void {
    this.__authService.signIn(GoogleLoginProvider.PROVIDER_ID).catch(() => {});
  }
  signInWithFB(): void {
    this.__authService.signIn(FacebookLoginProvider.PROVIDER_ID).catch(() => {});
  }

  logoutAndRoute() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
    this.__authService.signOut(true).then(() => {
      this.navigateService.navigateUrl('/login');
    }).catch(() => {
      this.navigateService.navigateUrl('/login');
    });
  }
  public ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
    this.__authService.signOut(true).catch(() => {});
  }
}
