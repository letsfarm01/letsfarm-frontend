import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermsSponsorshipComponent } from './terms-sponsorship.component';

describe('TermsSponsorshipComponent', () => {
  let component: TermsSponsorshipComponent;
  let fixture: ComponentFixture<TermsSponsorshipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermsSponsorshipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermsSponsorshipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
