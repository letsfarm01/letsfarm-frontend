import {AfterViewInit, ChangeDetectorRef, Component, OnChanges, OnInit} from '@angular/core';
import {NavigatorService} from '../../services/navigatorService/navigator.service';
import {IResponse} from '../../interfaces/iresponse';
import {UserService} from '../../services/api-handlers/userService/user.service';
import {BootstrapNotifyService} from '../../services/bootstrap-notify/bootstrap-notify.service';
import {UtilService} from "../../services/utilService/util.service";
import {EventsService} from "../../services/eventServices/event.service";

@Component({
  selector: 'app-farm-products',
  templateUrl: './farm-products.component.html',
  styleUrls: ['./farm-products.component.css']
})
export class FarmLProductsComponent implements OnInit, AfterViewInit, OnChanges {
  allProducts: any = [];
  allStatus: any = [];
  allTypes: any = [];
  loading = false;
  searchLoader = false;
  currentUser = null;
  noRecordFound = false;
  clearFilterInput = false;
  productSearchData = null;
  arrData = { product_type: [], product_status: []};
  constructor(private navigatorService: NavigatorService,
              private alertService: BootstrapNotifyService,
              private cdRef: ChangeDetectorRef,
              private eventService: EventsService,
              private utilService: UtilService,
              private userService: UserService) {
    this.getProductTypes();
    this.getProductStatus();
  }

  ngOnInit() {
    this.currentUser = this.utilService.getAuthUser();
    this.triggerAccordion();
    this.getFarmProducts();
  }
  ngAfterViewInit() {
    this.clearFilterInput = true;
    this.cdRef.detectChanges();
  }
  ngOnChanges() {
    this.clearFilterInput = true;
  }


  triggerAccordion(box= null, id= null) {
    if (!box || !id) {
      if ($('#farmType').hasClass('show')) {
        $('#farm-type-arrow-down').removeClass('d-none');
        $('#farm-type-arrow-right').addClass('d-none');
      } else {
        $('#farm-type-arrow-down').addClass('d-none');
        $('#farm-type-arrow-right').removeClass('d-none');
      }
      if ($('#farmStatus').hasClass('show')) {
        $('#farm-status-arrow-down').removeClass('d-none');
        $('#farm-status-arrow-right').addClass('d-none');
      } else {
        $('#farm-status-arrow-down').addClass('d-none');
        $('#farm-status-arrow-right').removeClass('d-none');
      }
    } else {
      console.log('BOX ', box, id);

      if ($(`#${id}-down`).hasClass('d-none')) {
        $(`#${id}-right`).addClass('d-none');
        $(`#${id}-down`).removeClass('d-none');
      } else {
        $(`#${id}-right`).removeClass('d-none');
        $(`#${id}-down`).addClass('d-none');
      }
    }
  }
  public loadThisShop(item) {
    this.navigatorService.navigateUrl('/farm-shop/' + item._id + '/details');
  }

  getFarmProducts() {
    this.loading = true;
    this.noRecordFound = false;
    this.productSearchData = null;
    this.arrData = { product_type: [], product_status: []};
    this.userService.getActiveFarmProducts()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.allProducts = res.data.data;
        this.loading = false;
      }, error => {
        console.log('Error ', error);
        this.noRecordFound = true;
        this.loading = false;
      });
  }
  getProductStatus() {
    this.userService.getActiveProductStatus()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.allStatus = res.data;
      }, error => {
        console.log('Error ', error);
      });
  }
  getProductTypes() {
    this.userService.getActiveProductType()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.allTypes = res.data;
      }, error => {
        console.log('Error ', error);
      });
  }
  keepChange(type, i) {
    const id = $(`#${type}${i}`).val();
    console.log(' change ', type, i, id);
    if (type === 'farmtype') {
      const index = this.arrData.product_type.indexOf(id);
      if (index === -1) {
        this.arrData.product_type.push(id);
      } else {
        this.arrData.product_type.splice(index, 1);
      }
    } else if (type === 'farmtypestatus') {
      const index = this.arrData.product_status.indexOf(id);
      if (index === -1) {
        this.arrData.product_status.push(id);
      } else {
        this.arrData.product_status.splice(index, 1);
      }
    } else {}
    console.log('Array ', this.arrData);
  }
  applyFilter() {
    this.loading = true;
    this.noRecordFound = false;
    this.allProducts = [];
    this.userService.filterFarmProduct(this.arrData).subscribe((res: IResponse) => {
      console.log('Res ', res);
      this.loading = false;
      this.allProducts = res.data.data;
      if (!res.data.data.length) {
        this.alertService.warning('No farm product matches filter criteria!');
        this.noRecordFound = true;
      }
    }, error => {
      console.log('Error ', error);
      this.getFarmProducts();
      this.alertService.error(error.error.msg || 'No record matches your filter criteria!');
    });
  }
  clearFilter() {
    this.getFarmProducts();
    this.clearFilterInput = false;
    this.arrData = { product_type: [], product_status: []};
    setTimeout(() => {
      this.clearFilterInput = true;
    }, 0);
  }
  searchProduct() {
    console.log('Search Farm ', this.productSearchData);
    if (this.productSearchData && !this.searchLoader) {
      this.searchLoader = true;
      this.noRecordFound = false;
      this.userService.searchFarmProduct({product_name: this.productSearchData}).subscribe((res: IResponse) => {
        this.searchLoader = false;
        this.allProducts = res.data.data;
        if (!res.data.data.length) {
          this.alertService.warning('No farm product with this search name!');
          this.noRecordFound = true;
        }
        this.alertService.success('Search completed!');
      }, error => {
        this.alertService.error(error.error.msg || 'No farm product with this name!');
        this.searchLoader = false;

      });
    }
  }

  openProduct(product) {
    console.log('PRODUCT ', product);
    this.navigatorService.navigateUrl('/farm-product/' + product._id + '/details');
  }
  addToCart(product) {
    if (product.product_status.name.toLowerCase() === 'in stock') {
      this.handlePurchase(product);
    } else {
      return this.alertService.error('Product is out of stock');
    }
  }

  handlePurchase(farmProduct) {
    this.alertService.info('Adding to cart!');
    if (!this.currentUser) {
      this.utilService.keepInCache(farmProduct, 1);
    } else {
      this.userService.addProductToCart({
        userId: this.currentUser._id,
        productId: farmProduct._id,
        quantity: 1
      }).subscribe((res: IResponse) => {
        this.alertService.success('Product added to cart successfully');
        this.eventService.broadcast('CART_MODIFIED');
      }, error => {
        this.alertService.error(error.error.msg || 'Unable to add to cart!');
      });
    }
  }
}
