import {AfterViewInit, ChangeDetectorRef, Component, OnChanges, OnInit} from '@angular/core';
import {NavigatorService} from '../../services/navigatorService/navigator.service';
import {IResponse} from '../../interfaces/iresponse';
import {UserService} from '../../services/api-handlers/userService/user.service';
import {BootstrapNotifyService} from '../../services/bootstrap-notify/bootstrap-notify.service';

@Component({
  selector: 'app-farm-shop',
  templateUrl: './farm-shop.component.html',
  styleUrls: ['./farm-shop.component.css']
})
export class FarmShopComponent implements OnInit, AfterViewInit, OnChanges {
  allShops: any = [];
  allStatus: any = [];
  allTypes: any = [];
  loading = false;
  searchLoader = false;
  noRecordFound = false;
  clearFilterInput = false;
  farmSearchData = null;
  arrData = { farm_type: [], farm_status: []};
  constructor(private navigatorService: NavigatorService,
              private alertService: BootstrapNotifyService,
              private cdRef: ChangeDetectorRef,
              private userService: UserService) {
    this.getFarmTypes();
    this.getFarmStatus();
  }

  ngOnInit() {
    this.triggerAccordion();
    this.getFarmShops();
  }
  ngAfterViewInit() {
      this.clearFilterInput = true;
      this.cdRef.detectChanges();
  }
  ngOnChanges() {
    this.clearFilterInput = true;
  }


  triggerAccordion(box= null, id= null) {
    if (!box || !id) {
      if ($('#farmType').hasClass('show')) {
          $('#farm-type-arrow-down').removeClass('d-none');
          $('#farm-type-arrow-right').addClass('d-none');
      } else {
        $('#farm-type-arrow-down').addClass('d-none');
        $('#farm-type-arrow-right').removeClass('d-none');
      }
      if ($('#farmStatus').hasClass('show')) {
        $('#farm-status-arrow-down').removeClass('d-none');
        $('#farm-status-arrow-right').addClass('d-none');
      } else {
        $('#farm-status-arrow-down').addClass('d-none');
        $('#farm-status-arrow-right').removeClass('d-none');
      }
    } else {
      console.log('BOX ', box, id);

      if ($(`#${id}-down`).hasClass('d-none')) {
        $(`#${id}-right`).addClass('d-none');
        $(`#${id}-down`).removeClass('d-none');
      } else {
        $(`#${id}-right`).removeClass('d-none');
        $(`#${id}-down`).addClass('d-none');
      }
    }
  }
  public loadThisShop(item) {
    this.navigatorService.navigateUrl('/farm-shop/' + item._id + '/details');
  }

  getFarmShops() {
    this.loading = true;
    this.noRecordFound = false;
    this.farmSearchData = null;
    this.arrData = { farm_type: [], farm_status: []};
    this.userService.getActiveShop()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.allShops = res.data.data;
        this.loading = false;
      }, error => {
        console.log('Error ', error);
        this.loading = false;
        this.noRecordFound = true;
      });
  }
  getFarmStatus() {
    this.userService.getActiveStatus()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.allStatus = res.data;
      }, error => {
        console.log('Error ', error);
      });
  }
  getFarmTypes() {
    this.userService.getActiveType()
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.allTypes = res.data;
      }, error => {
        console.log('Error ', error);
      });
  }
  keepChange(type, i) {
    const id = $(`#${type}${i}`).val();
    console.log(' change ', type, i, id);
    if (type === 'farmtype') {
      const index = this.arrData.farm_type.indexOf(id);
      if (index === -1) {
        this.arrData.farm_type.push(id);
      } else {
        this.arrData.farm_type.splice(index, 1);
      }
    } else if (type === 'farmtypestatus') {
      const index = this.arrData.farm_status.indexOf(id);
      if (index === -1) {
        this.arrData.farm_status.push(id);
      } else {
        this.arrData.farm_status.splice(index, 1);
      }
    } else {}
    console.log('Array ', this.arrData);
  }
  applyFilter() {
    this.loading = true;
    this.noRecordFound = false;
    this.allShops = [];
    this.userService.filterFarmShop(this.arrData).subscribe((res: IResponse) => {
      console.log('Res ', res);
      this.loading = false;
      this.allShops = res.data.data;
      if (!res.data.data.length) {
        this.alertService.warning('No farm shop matches filter criteria!');
        this.noRecordFound = true;
      }
    }, error => {
      console.log('Error ', error);
      this.getFarmShops();
      this.alertService.error(error.error.msg || 'No record matches your filter criteria!');
    });
  }
  clearFilter() {
    this.getFarmShops();
    this.clearFilterInput = false;
    this.arrData = { farm_type: [], farm_status: []};
    setTimeout(() => {
      this.clearFilterInput = true;
    }, 0);
  }
  searchFarm() {
    console.log('Search Farm ', this.farmSearchData);
    if (this.farmSearchData && !this.searchLoader) {
      this.searchLoader = true;
      this.noRecordFound = false;
      this.userService.searchFarmShop({farm_name: this.farmSearchData}).subscribe((res: IResponse) => {
        this.searchLoader = false;
        this.allShops = res.data.data;
        if (!res.data.data.length) {
          this.alertService.warning('No farm shop with this search name!');
          this.noRecordFound = true;
        }
        this.alertService.success('Search completed!');
      }, error => {
        this.alertService.error(error.error.msg || 'No farm shop with this name!');
        this.searchLoader = false;

      });
    }
  }
}
