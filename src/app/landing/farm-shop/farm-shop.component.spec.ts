import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmShopComponent } from './farm-shop.component';

describe('FarmShopComponent', () => {
  let component: FarmShopComponent;
  let fixture: ComponentFixture<FarmShopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmShopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmShopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
