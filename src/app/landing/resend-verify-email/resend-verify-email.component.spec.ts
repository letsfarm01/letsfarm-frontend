import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResendVerifyEmailComponent } from './resend-verify-email.component';

describe('ResendVerifyEmailComponent', () => {
  let component: ResendVerifyEmailComponent;
  let fixture: ComponentFixture<ResendVerifyEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResendVerifyEmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResendVerifyEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
