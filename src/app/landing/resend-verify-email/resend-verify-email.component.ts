import { Component, OnInit } from '@angular/core';
import { environment as ENV } from '../../../environments/environment';
import {BootstrapNotifyService} from '../../services/bootstrap-notify/bootstrap-notify.service';
import {UserService} from '../../services/api-handlers/userService/user.service';
import {IResponse} from '../../interfaces/iresponse';
import {NavigatorService} from '../../services/navigatorService/navigator.service';
import * as JWT_DECODE from 'jwt-decode';
import {CacheService} from '../../services/cacheService/cache.service';
import {ActivatedRoute, ActivatedRouteSnapshot, Router} from '@angular/router';
import {AuthService} from '../../services/authService/auth.service';

@Component({
  selector: 'app-resend-verify-email',
  templateUrl: './resend-verify-email.component.html',
  styleUrls: ['./resend-verify-email.component.css']
})
export class ResendVerifyEmailComponent implements OnInit {
  public EMAIL_VALIDATION: any =  ENV.EMAIL_VALIDATION;
  public credentials = {
    email: null
  };
  public userDetails: any;
  loaders = {
    loading: false
  };
  constructor(private bootstrapNotify: BootstrapNotifyService,
              private userService: UserService,
              private navigatorService: NavigatorService,
              private route: ActivatedRoute,
              private authService: AuthService,
              private cacheService: CacheService) {
    this.authService.logOut();
  }
  ngOnInit(): void {
    this.userDetails = null;
  }
  public resendEmail() {
    this.loaders.loading = true;
    if (!this.credentials.email ) {
      this.bootstrapNotify.info('Enter the email address you used in registering');
      this.loaders.loading = false;
      return;
    } else if (!this.credentials.email.match(this.EMAIL_VALIDATION) ) {
      this.bootstrapNotify.info('Not a valid email address!');
      this.loaders.loading = false;
      return;
    } else {
      this.userService.resendVerificationLink(this.credentials).subscribe((response: IResponse) => {
        console.log('Response - email', response);
        this.loaders.loading = false;
        this.bootstrapNotify.success(response['msg'] || 'Email verification link sent to you at ' + this.credentials.email);
        this.navigatorService.navigateUrl('/');
      }, error => {
        this.bootstrapNotify.error(error.error.msg || 'Unable to send email verification', 'right');
        this.loaders.loading = false;
        console.info('Error => ', error);
      });
    }
  }
}
