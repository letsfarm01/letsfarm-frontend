import { Component, OnInit } from '@angular/core';
import { environment as ENV } from '../../../environments/environment';
import {BootstrapNotifyService} from '../../services/bootstrap-notify/bootstrap-notify.service';
import {UserService} from '../../services/api-handlers/userService/user.service';
import {IResponse} from '../../interfaces/iresponse';
import {NavigatorService} from '../../services/navigatorService/navigator.service';
import * as JWT_DECODE from 'jwt-decode';
import {CacheService} from '../../services/cacheService/cache.service';
import {ActivatedRoute, ActivatedRouteSnapshot, Router} from '@angular/router';
import {AuthService} from '../../services/authService/auth.service';

@Component({
  selector: 'app-set-password',
  templateUrl: './set-password.component.html',
  styleUrls: ['./set-password.component.css']
})
export class SetPasswordComponent implements OnInit {
  public EMAIL_VALIDATION: any =  ENV.EMAIL_VALIDATION;
  public credentials = {
    confirmPassword: null,
    password: null,
    token: null,
  };
  public userDetails: any;
  loaders = {
    login: false,
    showResetLink: false
  };
  constructor(private bootstrapNotify: BootstrapNotifyService,
              private userService: UserService,
              private navigatorService: NavigatorService,
              private route: ActivatedRoute,
              private authService: AuthService,
              private cacheService: CacheService) {
    // this.authService.logOut();
    const secretCode = this.route.snapshot.paramMap.get('codeSecret') || null;
    console.log('SECRET CODE ', secretCode.toString().charAt(0));
    if (secretCode.toString().charAt(0) !== 'Q') {
      this.navigatorService.navigateUrl('/login');
    }
  }
  ngOnInit(): void {
    this.credentials.token = this.route.snapshot.paramMap.get('codeSecret') || null;
    this.userDetails = null;
  }
  public resetPassword() {
    this.loaders.login = true;
    this.cacheService.deleteSession(ENV.TOKEN);
    this.cacheService.deleteSession(ENV.USERTOKEN);
    this.cacheService.deleteStorage(ENV.USERTOKEN);
    this.cacheService.deleteSession(ENV.TOKEN);
    if (!this.credentials.confirmPassword || !this.credentials.password) {
      this.bootstrapNotify.info('Provide New Password!');
      this.loaders.login = false;
    } else if (this.credentials.confirmPassword !== this.credentials.password) {
      this.bootstrapNotify.info('Password must match!');
      this.loaders.login = false;
    } else {
      this.userService.resetPassword(this.credentials).subscribe((response: IResponse) => {
        console.log('Response', response);
        this.loaders.login = false;
        this.bootstrapNotify.success(response['msg'] || 'Please proceed to login');
        this.navigatorService.navigateUrl('/login');
      }, error => {
        this.bootstrapNotify.error(error.error.msg || 'Unable to reset password', 'right');
        this.loaders.login = false;
        console.info('Error => ', error);
      });
    }
  }
}
