import {AfterViewInit, Component, OnInit} from '@angular/core';
import {NavigatorService} from '../../services/navigatorService/navigator.service';
import {IResponse} from '../../interfaces/iresponse';
import {UserService} from '../../services/api-handlers/userService/user.service';
import {ActivatedRoute} from '@angular/router';
import {UtilService} from '../../services/utilService/util.service';

@Component({
  selector: 'app-selected-blog',
  templateUrl: './selected-blog.component.html',
  styleUrls: ['./selected-blog.component.css']
})
export class SelectedBlogComponent implements OnInit, AfterViewInit {
  loading = false;
  blog = null;
  selectedPost = null;
  genURL = null;
  IP = null;
  currentUser = null;
  likes = 0;
  dislikes = 0;
  constructor(private navigatorService: NavigatorService,
              private route: ActivatedRoute,
              private utilService: UtilService,
              private userService: UserService) {
    this.currentUser = this.utilService.getAuthUser() || null;
    this.selectedPost = this.route.snapshot.paramMap.get('id') || null;
    this.getIP();
    if (!this.selectedPost) {
      this.previous();
    } else {
      this.genURL = window.location.href;
      this.getBlogs();
    }
  }

  ngOnInit() {}

  ngAfterViewInit() {
    this.getUserFeeling();
  }
  public previous() {
    this.navigatorService.navigateUrl('/blog');
  }
  getBlogs() {
    this.loading  = true;
    this.userService.getBlogById(this.selectedPost)
      .subscribe((res: IResponse) => {
        // https://www.youtube.com/embed/aqhrMh3p_9U
        this.blog = res.data;
        /*if (this.blog.video_link && !this.blog.video_link.includes('/embed/')) {
          const url = this.blog.video_link.split('v=')[1];
          this.blog.video_link = `https://www.youtube.com/embed/${url}`;
        }*/
        this.likes = this.blog.total_likes;
        this.dislikes = this.blog.total_dislikes;
        this.loading  = false;
      }, error => {
        console.log('Error Educ', error);
        this.loading  = false;
      });
  }
  onNewComment(e) {
    console.log('Eben ', e);
    this.userService.saveBlogComment({
      blogId: this.selectedPost,
      message: e.message}).subscribe((res: IResponse) => {

    });
  }
  getIP() {
    $.getJSON('https://ipapi.co/json/', (data) => {
      this.IP = JSON.parse(JSON.stringify(data, null, 2));
      console.log('IP ', this.IP, this.IP.ip);
    });
  }
  feelingPost(feel) {
    console.log('POST ', this.IP);
    this.userService.saveBlogFeeling({
      userId: this.currentUser._id,
      actionType: feel,
      blogId:	this.selectedPost,
      userIdentity: this.IP ? this.IP.ip : null
    }).subscribe(() => {
      this.userService.getBlogById(this.selectedPost)
        .subscribe((res: IResponse) => {
          const blog = res.data;
          this.likes = blog.total_likes;
          this.dislikes = blog.total_dislikes;
        }, error => {});
    });
  }
  getUserFeeling() {
    if (this.currentUser) {
      this.userService.getMyBlogFeeling({
        userId: this.currentUser._id,
        blogId: this.selectedPost
      }).subscribe((res: IResponse) => {
        console.log('MY FEELING ', res);
      });
    }
  }
}

