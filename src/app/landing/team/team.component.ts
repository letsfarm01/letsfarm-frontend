import { Component, OnInit } from '@angular/core';
import {UtilService} from '../../services/utilService/util.service';
import {IResponse} from '../../interfaces/iresponse';
import {UserService} from '../../services/api-handlers/userService/user.service';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit {
  teamGroups: any[] = [];
  teamMembers = {};
  loadingData = false;
  activeMember = null;
  constructor(private utilService: UtilService,
              private userService: UserService) { }

  ngOnInit() {
    this.getGroups();
  }
  openModal(member) {
    if (member.about && member.about.split(' ').length > 10 ) {
      this.activeMember = member;
      this.utilService.openModal('teamMemberModal');
    }
  }
  closeModal() {
    this.activeMember = null;
    this.utilService.closeModal('teamMemberModal');
  }
  getGroups() {
    this.loadingData = true;
    this.userService.getTeamMembers()
      .subscribe((res: IResponse) => {
        console.log('RES ', res);
        this.loadingData = false;
        this.teamGroups = res.data.teamGroups;
        this.teamMembers = res.data.result;
      }, error => {
        this.loadingData = false;
        console.log({error});
      });
  }
}
