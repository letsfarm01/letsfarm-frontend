import {AfterViewInit, Component, Input, OnInit} from '@angular/core';
import {UtilService} from '../../../../services/utilService/util.service';
import {UserService} from '../../../../services/api-handlers/userService/user.service';
import {Router} from '@angular/router';
import {AuthService} from '../../../../services/authService/auth.service';
import {NavigatorService} from '../../../../services/navigatorService/navigator.service';
import {IResponse} from "../../../../interfaces/iresponse";
import {BootstrapNotifyService} from "../../../../services/bootstrap-notify/bootstrap-notify.service";
import {EventsService} from "../../../../services/eventServices/event.service";

@Component({
  selector: 'app-l-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, AfterViewInit {
@Input() active: string;
@Input() buttonLess: string;
public currentUser = null;
cartCountNumber = 0;
homeUrl = window.location.origin;
  constructor(private utilService: UtilService,
              private userService: UserService,
              private authService: AuthService,
              private alertService: BootstrapNotifyService,
              private navigatorService: NavigatorService,
              private eventService: EventsService,
              private router: Router) {
    this.eventService.on('CART_MODIFIED', () => {
      this.getFarmCartCount();
    });
  }

  ngOnInit() {
    this.utilService.triggerScrollTo();
    this.checkLogin();
    console.log('Active ', this.buttonLess);
    $('body').css('background-color', 'inherit');
    $('#page-loader').fadeOut('fast', function() {
      $(this).remove();
    });
  }
  ngAfterViewInit() {
    // Smooth Scrolling
    $('.scrolling, .navbar-brand').on('click', function(event) {
      const $anchor = $(this);
      try {
        $('html, body').stop().animate({
          scrollTop: $($anchor.attr('href')).offset().top - 75
        }, 1000, 'easeInOutExpo');
      } catch (e) {}
      event.preventDefault();
    });
    $('.nav.navbar-nav > li').on('click', 'a', function(event) {
      const $anchor = $(this);
      try {
        $('html, body').stop().animate({
          scrollTop: $($anchor.attr('href')).offset().top - 75
        }, 1000, 'easeInOutExpo');
      } catch (e) {}
      event.preventDefault();
    });

    $('.dropdown').on('show.bs.dropdown', function() {
      $(this).find('.dropdown-menu')
        .first().stop(!0, !0)
        .animate({top: '100%'}, 200); });

      $('.dropdown').on('hide.bs.dropdown', function() {
        $(this).find('.dropdown-menu').first().stop(!0, !0)
          .animate({top: '80%'}, 200); });



    $('.navbar-toggler ').on('click', function() {
      console.log('Hello Man');
      if ($('.navbar-collapse.collapse').hasClass('in')) {
        $('.navbar-collapse.collapse').removeClass('in');
      } else {
        $('.navbar-collapse.collapse').addClass('in');
      }
    });

    // close collapse nav after select
    $('.btn-sing, .navbar-nav > li ').on('click', 'a', function() {
      console.log('Hello woMan');
      $('.navbar-collapse.collapse').removeClass('in');
    });

  }
  openHomePage() {
    console.log('GO HOME');
  }
  public checkLogin() {
    this.currentUser = this.utilService.getAuthUser();
    this.getFarmCartCount();
    console.log('CurrentUser ', this.currentUser);
  }
  public logOut() {
    this.userService.logOut().subscribe(() => {
      this.authService.logOut();
    });
    this.authService.logOut();
    // this.router.navigate(['/']);
    this.currentUser = null;
  }
  public openDashboard() {
    // [routerLink]="['/user/dashboard']"
    this.navigatorService.navigateUrl(`/${this.currentUser.role.toLowerCase() === 'user' ? 'user' : 'super'}/dashboard`);
  }
  public openProfile() {
    // [routerLink]="['/user/dashboard']"
    this.navigatorService.navigateUrl(`/${this.currentUser.role.toLowerCase() === 'user' ? 'user' : 'super'}/profile`);
  }
  getFarmCartCount() {
    if (this.currentUser) {
      this.userService.countCart(this.currentUser._id)
        .subscribe((res: IResponse) => {
          console.log('Res ', res.data);
          this.cartCountNumber = res.data;
        }, error => {      });
    } else {
      this.cartCountNumber = this.utilService.countItemsInCache();

    }
  }
  openCart() {
    if (this.currentUser && this.currentUser.role === 'USER') {
      this.navigatorService.navigateUrl('/user/cart');
    } else {
      this.alertService.info('Login required!!');
      this.navigatorService.navigateUrl('/login');
    }
  }
}

