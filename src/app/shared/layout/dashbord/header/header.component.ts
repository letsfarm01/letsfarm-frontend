import {AfterViewInit, Component, OnInit} from '@angular/core';
import {UserService} from '../../../../services/api-handlers/userService/user.service';
import {AuthService} from '../../../../services/authService/auth.service';
import {Router} from '@angular/router';
import {UtilService} from '../../../../services/utilService/util.service';
import {CacheService} from '../../../../services/cacheService/cache.service';
import {DecryptService} from '../../../../services/decryptService/decrypt.service';
import {EventsService} from '../../../../services/eventServices/event.service';
import {BootstrapNotifyService} from '../../../../services/bootstrap-notify/bootstrap-notify.service';
import {NavigatorService} from '../../../../services/navigatorService/navigator.service';
import {IResponse} from '../../../../interfaces/iresponse';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, AfterViewInit {

  wallet_balance = 0.0;
  cartCountNumber = 0;
  public currentUser: any = null;

  constructor(private userService: UserService,
              private authService: AuthService,
              private utilService: UtilService,
              private alertService: BootstrapNotifyService,
              private eventService: EventsService,
              private navigateService: NavigatorService,
              private cacheService: CacheService,
              private decryptService: DecryptService,
              private router: Router) {
    this.eventService.on('WALLET_UPDATED', () => {
      console.log('WALLET UPDATE IN THE HEADER');
      this.currentUser = this.utilService.getAuthUser();
      this.getWallet();
    });
    this.eventService.on('PROFILE_UPDATED', () => {
      console.log('PROFILE UPDATED!!!!');
      this.getUserProfile();
    });
    this.eventService.on('CART_UPDATED', (res) => {
      console.log('CART  UPDATED!!!!', res);
      this.getAuthUser();
    });
  }

  ngOnInit() {
    this.getAuthUser();
    // this.getWallet();
    this.getUserProfile();
  }
  ngAfterViewInit(): void {
    $('.btn-toggle-offcanvas').on('click', function() {
      console.log('Toogle OffCanvas 3');
      $('body').toggleClass('offcanvas-active');
    });

    $('#main-content').on('click', function() {
      $('body').removeClass('offcanvas-active');
    });
    this.updateCartAfterLogin();
  }


  public getUserProfile() {
    this.userService.getUser(this.currentUser._id).subscribe((res: IResponse) => {
      console.log('Res ', res);
      this.currentUser = res.data;
    }, error => {
      console.log(error);
    });
  }
  public getAuthUser() {
    this.currentUser = this.utilService.getAuthUser();
    this.wallet_balance = this.currentUser.wallet_balance;
    console.info('Current User', this.currentUser);
    if(this.currentUser.role === 'USER') {
      this.getWallet();
      this.getFarmCartCount();}
  }
  public logOut() {
    this.userService.logOut().subscribe(() => {
      this.authService.logOut();
    });
    this.authService.logOut();
    this.router.navigate(['/']);
  }
  public openProfile() {
    this.navigateService.navigateUrl(`/${this.currentUser.role.toLowerCase()}/manage-profile`);
  }
  public toggleOffCanvas() {
    console.log('Toogle OffCanvas');
    $('body').toggleClass('offcanvas-active');
  }
  getWallet() {
    console.log('Wallet Wallet on HEader');
    this.userService.getWallet(this.currentUser._id).subscribe((res: IResponse) => {
      this.wallet_balance = res.data.wallet_balance || 0.0;
    });
  }
  getFarmCartCount() {
    this.userService.countCart(this.currentUser._id)
      .subscribe((res: IResponse) => {
        console.log('Res ', res.data);
        this.cartCountNumber = res.data;
      }, error => {      });
  }
  updateCartAfterLogin() {
    const items = this.utilService.getItemsInCache();
    if (items.length && this.currentUser.role.toLowerCase() === 'user') {
      const bulkItem = [];
      for (const item of items) {
        bulkItem.push({productId: item._id, quantity: item.quantity});
      }
      this.userService.addProductToCartInBulk({cart: bulkItem, userId: this.currentUser._id}).subscribe((res: IResponse) => {
        this.utilService.clearCartItemsInCache();
      }, error => {
        this.utilService.clearCartItemsInCache();
      });
    }
  }
}
