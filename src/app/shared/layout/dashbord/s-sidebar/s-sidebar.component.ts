import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {environment as ENV} from '../../../../../environments/environment';
import {AuthService} from '../../../../services/authService/auth.service';
import {UserService} from '../../../../services/api-handlers/userService/user.service';
import {UtilService} from '../../../../services/utilService/util.service';
import {NavigatorService} from '../../../../services/navigatorService/navigator.service';
import {BootstrapNotifyService} from "../../../../services/bootstrap-notify/bootstrap-notify.service";
import {SuperAdminService} from "../../../../services/api-handlers/superAdminService/super-admin.service";
import {IResponse} from "../../../../interfaces/iresponse";

declare const $: any;
@Component({
  selector: 'app-s-sidebar',
  templateUrl: './s-sidebar.component.html',
  styleUrls: ['./s-sidebar.component.css']
})
export class SSidebarComponent implements OnInit, AfterViewInit {
  version = {
    min_allowed_android_version: null,
    min_allowed_ios_version: null,
  };
  loaders = {
    saving: false
  };
  public reportFile: any;
  public currentUser: any;
  static updateActiveBar(url) {
    setTimeout(() => {
      console.info('URL : ', url);
      let oldUrl;
      oldUrl = 'dashboard';
      $('.nav-link-special').removeClass('active');
      $(`#${url}`).addClass('active');
      if ($(`#${url}`).hasClass('active')) {
        oldUrl = url;
      } else {
        $(`#${oldUrl}`).addClass('active');
      }
    }, 500);
  }
  constructor(
    private route: Router,
    private userService: UserService,
    private utilService: UtilService,
    private authService: AuthService,
    private alertService: BootstrapNotifyService,
    private superadminService: SuperAdminService,
    private navigatorService: NavigatorService
  ) {
    this.currentUser = this.utilService.getAuthUser();
  }
  ngOnInit() {
    // this.checkPrivilege(['super', 'blogger']);
    this.getVersion();
  }
  ngAfterViewInit(): void {
    const routes = this.route.url.split('/');
    SSidebarComponent.updateActiveBar(routes[routes.length - 1]);
    const navLink = document.querySelectorAll('.nav-link-special');
    navLink.forEach((el) => {
      // console.info('Ele ', el);
      el.addEventListener('click', (e) => {
        $('.navLinks').removeClass('active');
        $('.childNavDropDown').removeClass('in');
        const id = el['id'];
        // console.info('ellll ', el['id']);
        if (id.includes('#') || !id) {return false; }
        $(`#${id}`).addClass('active');
        $(`#${id} > ul`).addClass('in');

      });
    });
  }
  showVersionModal() {
    this.utilService.openModal('appVersion');
  }
  public logOut() {
    this.userService.logOut().subscribe(() => {
      this.authService.logOut();
    });
    this.authService.logOut();
    this.route.navigate(['/']);
  }
  public openProfile() {
    // [routerLink]="['/user/dashboard']"
    this.navigatorService.navigateUrl(`/${this.currentUser.role.toLowerCase()}/profile`);
  }
  getVersion() {
    this.superadminService.getVersion().subscribe((res: IResponse) => {
      this.version = res.data || this.version;
    }, error => {
      this.alertService.error(error.error.msg || 'Unable to get latest version!');
    });
  }
  saveVersion() {
    if (!this.version.min_allowed_android_version || !this.version.min_allowed_ios_version) {
      return this.alertService.error('Version is required!');
    } else {
      this.loaders.saving = true;
      this.superadminService.saveVersion({
        min_allowed_ios_version: this.version.min_allowed_ios_version,
        min_allowed_android_version: this.version.min_allowed_android_version}).subscribe((res: IResponse) => {
        console.log('RES ', res);
        this.alertService.success(res.msg || 'Version saved successfully!');
        this.getVersion();
        this.loaders.saving = false;
        this.utilService.closeModal('appVersion');
      }, error => {
        this.loaders.saving = false;
        this.alertService.error(error.error.msg || 'Unable to save latest version!');
      });
    }
  }
  public checkPrivilege(users) {
    // console.log('USERS ', this.currentUser.role, users);
    const myRole = this.currentUser.role.toLowerCase();
    return users.includes(myRole);
  }
}
