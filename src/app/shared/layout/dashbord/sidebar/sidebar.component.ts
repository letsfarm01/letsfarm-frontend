import {AfterViewInit, Component, OnInit} from '@angular/core';
import {environment as ENV} from '../../../../../environments/environment';
import {ActivatedRoute, Router} from '@angular/router';
import {UtilService} from '../../../../services/utilService/util.service';
import {UserService} from '../../../../services/api-handlers/userService/user.service';
import {AuthService} from '../../../../services/authService/auth.service';
import {EventsService} from '../../../../services/eventServices/event.service';
import {NavigatorService} from '../../../../services/navigatorService/navigator.service';
import {BootstrapNotifyService} from '../../../../services/bootstrap-notify/bootstrap-notify.service';
import {IResponse} from '../../../../interfaces/iresponse';
import {CacheService} from '../../../../services/cacheService/cache.service';

declare const $: any;
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit, AfterViewInit {
  public currentUser: any = null;
  public Roles: any = [];
  wallet_balance = 0.0;
  transactionData = null;
  show = true;
  loadingCharge = false;
  public paymentChannels: any = [];
  public wallet: any;
  public fundWallet = {
    payment_gateway: null,
    amount: 0.0,
    userId: null
  };
  selectedGateway = null;
  accountManager = {
    topic: null,
    message: null,
    userId: null
};
  loaders = {
    saving: false,
    loaders: false
  };
  loadImg = false;
  static updateActiveBar(url) {
    setTimeout(() => {
      console.info('URL : ', url);
      let oldUrl;
      url = url.split('?')[0];
      oldUrl = 'dashboard';
      $('.nav-link-special').removeClass('active');
      $(`#${url}`).addClass('active');
      if ($(`#${url}`).hasClass('active')) {
        oldUrl = url;
      } else {
        $(`#${oldUrl}`).addClass('active');
      }
    }, 500);
  }
  constructor(
    private route: Router,
    private utilService: UtilService,
    private userService: UserService,
    private authService: AuthService,
    private cacheService: CacheService,
    private alertService: BootstrapNotifyService,
    private eventService: EventsService,
    private navigatorService: NavigatorService
  ) {
    this.eventService.on('WALLET_UPDATED', () => {
      this.currentUser = this.utilService.getAuthUser();
      this.getWallet();
    });
    this.eventService.on('PROFILE_UPDATED', (user) => {
      // this.loadImg = false;
      console.log('USER NEW ', user);
      this.currentUser = user;
      this.utilService.setAuthUser(JSON.stringify(this.currentUser));
      // this.getUserProfile();
    });
  }
  ngOnInit() {
   this.setRole();
    this.currentUser = this.utilService.getAuthUser();
    this.getPaymentChannel();
    this.getWallet();
    // this.getUserProfile();
  }

  public getUserProfile() {
    this.userService.getUser(this.currentUser._id).subscribe((res: IResponse) => {
      console.log('Res FROM SIDE BAR', res.data);
      this.loadImg = true;
    }, error => {
      this.loadImg = true;
      console.log(error);
    });
  }
  forceAlignment() {
    setTimeout(() => {
      $('#dropperBody').css({'left': '-50px'});
    }, 1000);
  }
  setRole() {
    this.Roles = [];
    const user: any = JSON.parse(sessionStorage.getItem(ENV.USERTOKEN)) || null;
    this.Roles = this.utilService.setRoles();
    // this.currentUser =  user.owner || null;
  }
  public checkPrivilege(users) {
    for (const user of users) {
      if (this.Roles.includes(user)) {
        return true;
      }
    }
    return false;
  }
  ngAfterViewInit(): void {
    const routes = this.route.url.split('/');
    SidebarComponent.updateActiveBar(routes[routes.length - 1]);
    // console.log('this.route ', this.route, routes, );
    const navLink = document.querySelectorAll('.nav-link-special');
    navLink.forEach((el) => {
      // console.info('Ele ', el);
      el.addEventListener('click', (e) => {
        $('.navLinks').removeClass('active');
        $('.childNavDropDown').removeClass('in');
        const id = el['id'];
        // console.info('ellll ', el['id']);
        if (id.includes('#') || !id) {return false; }
        $(`#${id}`).addClass('active');
        $(`#${id} > ul`).addClass('in');

      });
    });
  }

  public logOut() {
    this.userService.logOut().subscribe(() => {
      this.authService.logOut();
    });
    this.authService.logOut();
    this.route.navigate(['/']);
  }
  public forceReload(data) {
    this.eventService.broadcast('RERENDER_COMPONENT', data);
  }

  public openProfile() {
    // [routerLink]="['/user/dashboard']"
    this.navigatorService.navigateUrl(`/${this.currentUser.role.toLowerCase()}/profile`);
  }
  public openAccountManager() {
    this.utilService.openModal('AccountManager');
  }
  public messageAccountManager() {
    this.accountManager.userId = this.currentUser._id;
    this.loaders.saving = true;
    this.userService.accountManager(this.accountManager)
      .subscribe((res: IResponse) => {
      this.loaders.saving = false;
      console.log({res});
      this.accountManager = {
        topic: null,
        message: null,
        userId: null
      };
      this.utilService.closeModal('AccountManager');
      this.alertService.success(res.msg || 'Message sent to account manager');
      }, error => {
      console.log({error});
      this.loaders.saving = false;
      this.alertService.error(error.error.msg || 'Unable to contact account manager!');
      });
  }

  public proceedToFund() {
    console.log('DATA ', this.fundWallet);
    const gatewaySelected = this.paymentChannels.filter(a => a._id === this.fundWallet.payment_gateway);
    if (!this.fundWallet.payment_gateway) {
      return this.alertService.warning('Select payment channel for this transaction!');
    } else if (this.fundWallet.amount < 0 || this.fundWallet.amount === 0) {
      return this.alertService.warning('Invalid Amount');
    } else if (this.fundWallet.amount < 100 ) {
      return this.alertService.warning('Amount can not be less than NGN100');
    } else if (this.fundWallet.amount > 10000000 ) {
      return this.alertService.warning('Amount exceeds maximum wallet funding at a time');
    } else {
      this.loaders.saving = true;
      console.log('URL ', window.location);
      // this.fundWallet.response_url = `${window.location.origin}/user/wallet-history`;
      this.fundWallet.userId = this.currentUser._id;
      this.userService.fundWalletInit(this.fundWallet).subscribe((res: IResponse) => {
        this.loaders.saving = false;
        console.log('Walleting ', res);
        this.utilService.closeModal('fundWallet');
        this.alertService.success(res.msg || 'Wallet funding initiated successfully');
        // this.eventService.broadcast('RELOAD_HISTORY');
        if (gatewaySelected[0].name.toLowerCase().includes('paystack')) {
          this.alertService.info('Redirecting to paystack');
          window.location.assign(res.data.authorization_url);
        } else if (gatewaySelected[0].name.toLowerCase().includes('bank transfer')) {
          this.cacheService.setStorage('BankRef', res.data.transactionRef);
          this.utilService.closeModal('fundWallet');
          this.navigatorService.navigateUrl('/user/payment/bank-transfer/' + res.data.transactionRef || '111111111' );
        } else {
          this.alertService.error('Unable to detect payment gateway', 'right');
        }
      }, error => {
        this.loaders.saving = false;
        this.alertService.error(error.error.msg || 'Unable to fund wallet at this moment, please try again later!', 'right');
      });
    }
  }
  public validateAmount() {
    if (Math.sign(this.fundWallet.amount) < 0) {
      this.fundWallet.amount = this.fundWallet.amount * -1;
    } else if (Math.sign(this.fundWallet.amount) > 10000000) {
      this.fundWallet.amount = 10000000;
    } else {
      return;
    }
  }

  getPaymentChannel() {
    this.userService.getPaymentChannels(true)
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.paymentChannels = res.data;
      }, error => {
        // this.getPaymentChannel();
      });
  }
  calculateCharge(cb) {
    this.loadingCharge = true;
    this.userService.chargeTransaction({amount: this.fundWallet.amount})
      .subscribe((res: IResponse) => {
        console.log('Response ', res);
        this.transactionData = res.data;
        this.loadingCharge = false;
        cb();
      }, (err) => {
        console.log('Error ', err);
        this.calculateCharge(cb);
      });
  }

  getWallet() {
    this.userService.getWallet(this.currentUser._id).subscribe((res: IResponse) => {
      this.wallet_balance = res.data.wallet_balance || 0.0;
    });
  }
  showCharge() {
    this.calculateCharge(() => {
      if (!this.transactionData) {
        return this.alertService.error('Unable to calculate paystack transaction charge, please use other payment method!');
      }
      this.show = false;
    });
  }
  closeShowCharge() {
    this.show = true;
    this.loadingCharge = false;
  }
  changeSelected(e) {
    this.selectedGateway = e;
    console.log(e);
  }
}
