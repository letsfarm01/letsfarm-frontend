import { NgModule } from '@angular/core';
import {SharedModules} from '../shared/shared.module';
import { superUserRouting } from '../../../app.routing';
import {NgxEditorModule} from "ngx-editor";
import {CroppieComponent} from "../../components/croppie/croppie.component";
import { EditorModule } from '@tinymce/tinymce-angular';
@NgModule({
  imports: [
    SharedModules,
    NgxEditorModule,
    EditorModule,
    superUserRouting.routes
  ],
  providers: [
    superUserRouting.providers
  ],
  entryComponents: [
    superUserRouting.entryComponent,
  ],
  declarations: [
    superUserRouting.components
  ],
  exports: [
    NgxEditorModule,
    EditorModule,
    superUserRouting.components
  ]
})
export class SuperUserModule { }
