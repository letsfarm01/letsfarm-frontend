import {environment as env, environment as ENV} from '../../../environments/environment';

export const Utils = {
  removeDuplicates(myArr, prop) {
    return myArr.filter((obj, pos, arr) => {
      return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
  },
  initDataTable(id) {
  const dataTableInstance = ($('#' + id)as any).DataTable({
    'pagingType': 'full_numbers',
    'lengthMenu': [
      [10, 25, 50, -1],
      [10, 25, 50, 'All']
    ],
    responsive: false,
    language: {
      search: '_INPUT_',
      searchPlaceholder: 'Search...',
    }
  });
},
  formatStringNumber(stringVal, decimal) {
    if (!stringVal) {
      return stringVal;
    }
    const valString = String(stringVal);
    if (valString && valString.indexOf(',') > -1) {
      return stringVal;
    }
    decimal = (decimal) ? decimal : 2;
    const val = (parseFloat(stringVal));
    const toString = String(val.toLocaleString('en-US', {minimumFractionDigits: decimal}));
    if (!toString || toString === 'NULL' || toString === 'null' || toString === '') {
      return '0';
    }
    const dotPos = toString.indexOf('.');
    const dataPath = toString.substring(dotPos);
    const len = dataPath.length;
    // console.log('len=', len);
    if (len >= 3) {
      return toString.substring(0, dotPos) + dataPath.substring(0, 3);
    }
    return toString.substring(0, dotPos) + dataPath;
  },
  downloadFormat(url, id) {
  this.downloading = true;
  const hiddenIFrameID = id;
  let iframe;
  iframe = document.getElementById(hiddenIFrameID);
  if (iframe === null) {
    iframe = document.createElement('iframe');
    iframe.id = hiddenIFrameID;
    iframe.style.display = 'none';
    document.body.appendChild(iframe);
  }
  iframe.src = url;
  setTimeout(() => {
    this.downloading = false;
  }, 2000);
},
  proceedToImport(file, url, success, error) {
  const request = new XMLHttpRequest();
  const accessKey = ENV.TOKEN;
  const token = sessionStorage.getItem(accessKey);
  const ENDPOINT = ENV.API_URL + '/' + ENV.API_VERSION + url;
  request.open('POST',
    `${ENDPOINT}`, true);
  request.onload = (response) => {
    if (request.status === 200) {
      console.log('native request::', request.response);
      const response_ = JSON.parse(request.response);
      console.log('Bulk Upload.: ', response_);
      if (response_.code === 200 && response_.status === 'SUCCESS') {
      success(response_);
      } else {
        error(response_);
      }
    } else {
      this.alertService.error('Error while trying to upload file.');
      console.log('request Failure::', request);
    }
  };
  request.setRequestHeader('Authorization', token);
  request.send(file);
}
};
