export const CONSTANTS = {
  DEFAULT_ERROR_MESSAGE: 'Could not process request',
  INVALID_FILE_FORMAT: 'Invalid file format',
  DEFAULT_SUCCESS_MESSAGE: 'Request processed successfully',
  UNAUTHENTICATED: 'UNAUTHENTICATED'
};
