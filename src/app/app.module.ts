import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import * as firebase from 'firebase/app';
import 'firebase/storage';
import 'firebase/analytics';
import { AppComponent } from './app.component';
import {landingRouting} from './app.routing';
import {CoreModule} from './shared/modules/core/core.module';
import {SharedModules} from './shared/modules/shared/shared.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { DisqusModule } from 'ngx-disqus';
import { RECAPTCHA_SETTINGS, RecaptchaSettings, RecaptchaModule
} from 'ng-recaptcha';
import {environment as ENV} from '../environments/environment';
import {
  FacebookLoginProvider, GoogleLoginProvider, SocialAuthServiceConfig,
  SocialLoginModule
} from 'angularx-social-login';
import { ResendVerifyEmailComponent } from './landing/resend-verify-email/resend-verify-email.component';
import { DisclaimerComponent } from './landing/disclaimer/disclaimer.component';
import { ManagePayoutComponent } from './admin-user/manage-payout/manage-payout.component';

firebase.initializeApp(ENV.FIREBASE);
firebase.analytics();

@NgModule({
  declarations: [
    AppComponent,
    landingRouting.components
  ],
  imports: [
    BrowserModule,
    landingRouting.routes,
    CoreModule,
    SharedModules,
    BrowserAnimationsModule,
    RecaptchaModule,
    SocialLoginModule,
    DisqusModule.forRoot('letsfarm'),


],
  providers: [
    landingRouting.providers,
    {
      provide: RECAPTCHA_SETTINGS,
      useValue: { siteKey: ENV.RECAPTCHA_SITE_KEY, size: 'invisible', badge: 'bottomleft', theme: 'light' } as RecaptchaSettings,
    }, {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '913208119439-0vq1t8ijqvd83hk8jr3t9oequrjv206h.apps.googleusercontent.com'
            ),
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider('759663901473289'),
          }
        ],
      } as SocialAuthServiceConfig,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
