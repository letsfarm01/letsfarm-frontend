import {IRouting} from './interfaces/irouting';
import {RouterModule, Routes} from '@angular/router';
import {UserRoutesComponent} from './users/user-routes/user-routes.component';
import {LoginComponent} from './landing/login/login.component';
import {GuardService, RoleService1, RoleService2} from './services/gaurdService/guard.service';
import {SuperUserRoutesComponent} from './admin-user/admin-user-routes/admin-user-routes.component';
import {AdminDashboardComponent} from './admin-user/admin-dashboard/admin-dashboard.component';
import {SSidebarComponent} from './shared/layout/dashbord/s-sidebar/s-sidebar.component';
import {ResetPasswordComponent} from './landing/reset-password/reset-password.component';
import {PageNotFoundComponent} from './shared/components/page-not-found/page-not-found.component';
import {SetPasswordComponent} from './landing/set-password/set-password.component';
import {RegisterComponent} from './landing/register/register.component';
import {SuperAdminService} from './services/api-handlers/superAdminService/super-admin.service';
import {UserDashboardComponent} from './users/user-dashboard/user-dashboard.component';
import {HomeComponent} from './landing/home/home.component';
import {FaqComponent} from './landing/faq/faq.component';
import {AboutComponent} from './landing/about/about.component';
import {BlogComponent} from './landing/blog/blog.component';
import {EducationComponent} from './landing/education/education.component';
import {FooterComponent} from './shared/layout/landing/footer/footer.component';
import {HeaderComponent} from './shared/layout/landing/header/header.component';
import {FarmShopComponent} from './landing/farm-shop/farm-shop.component';
import {AboutAppComponent} from './landing/about-app/about-app.component';
import {TeamComponent} from './landing/team/team.component';
import { ContactComponent } from './landing/contact/contact.component';
import { TermsSponsorshipComponent } from './landing/terms-sponsorship/terms-sponsorship.component';
import { TermsFarmVisitComponent } from './landing/terms-farm-visit/terms-farm-visit.component';
import { TermsUseComponent } from './landing/terms-use/terms-use.component';
import { PrivacyPolicyComponent } from './landing/privacy-policy/privacy-policy.component';
import { SelectedEducationComponent } from './landing/selected-education/selected-education.component';
import { VerifyUserEmailComponent } from './landing/verify-user-email/verify-user-email.component';
import {CategoriesComponent} from './admin-user/farm-shop/categories/categories.component';
import {StatusComponent} from './admin-user/farm-shop/status/status.component';
import {ShopsComponent} from './admin-user/farm-shop/shops/shops.component';
import {AdminFarmShopRoutesComponent} from './admin-user/farm-shop/admin-farm-shop-routes/admin-farm-shop-routes.component';
import {DeleteService} from './services/firebase-delete/delete.service';
import {FarmShopDetailsComponent} from './landing/farm-shop-details/farm-shop-details.component';
import {CartsComponent} from './users/carts/carts.component';
import {FarmUpdateComponent} from './users/farm-update/farm-update.component';
import {MyFarmsComponent} from './users/my-farms/my-farms.component';
import {TransactionComponent} from './users/transaction/transaction.component';
import {ProfileComponent} from './users/profile/profile.component';
import {FarmProductsComponent} from './users/farm-products/farm-products.component';
import {FarmProductOrdersComponent} from './users/farm-product-orders/farm-product-orders.component';
import {FarmProductSavedComponent} from './users/farm-product-saved/farm-product-saved.component';
import {CustomersComponent} from './admin-user/customers/customers.component';
import {AdminComponent} from './admin-user/admin/admin.component';
import {UserGroupPrivilegeComponent} from './admin-user/user-group-privilege/user-group-privilege.component';
import {NewsletterSubscribersComponent} from './admin-user/web-content/newsletter-subscribers/newsletter-subscribers.component';
import {BankTransferComponent} from './admin-user/bank-transfer/bank-transfer.component';
import {PaystackReferenceComponent} from './admin-user/paystack-reference/paystack-reference.component';
import {OrdersComponent} from './admin-user/orders/orders.component';
import {AllTransactionComponent} from './admin-user/transaction/transaction.component';
import {WalletTransactionsComponent} from './admin-user/wallet-transactions/wallet-transactions.component';
import {AllOrdersComponent} from './admin-user/all-orders/all-orders.component';
import {AdminProfileComponent} from './admin-user/admin-profile/admin-profile.component';
import {PaymentGatewaysComponent} from './admin-user/payment-gateways/payment-gateways.component';
import {AdminHomeComponent} from './admin-user/web-content/home/home.component';
import {AboutUsComponent} from './admin-user/web-content/about-us/about-us.component';
import {AdminEducationComponent} from './admin-user/web-content/education/education.component';
import {AdminBlogComponent} from './admin-user/web-content/blog/blog.component';
import {WebContentRoutesComponent} from './admin-user/web-content/web-content-routes/web-content-routes.component';
import {ConfirmBankTransferComponent} from './users/bank-transfer/bank-transfer.component';
import {WalletHistoryComponent} from './users/wallet-history/wallet-history.component';
import {PendingTransferComponent} from './users/pending-transfer/pending-transfer.component';
import {ManageTeamComponent} from './admin-user/web-content/manage-team/manage-team.component';
import {ReferralComponent} from './users/referral/referral.component';
import {ManageReferralsComponent} from './admin-user/referrals/referrals.component';
import {PaystackTransactionComponent} from './admin-user/paystack-transaction/paystack-transaction.component';
import {BankTransactionComponent} from './admin-user/bank-transaction/bank-transaction.component';
import {BankTransferConfirmedComponent} from './admin-user/bank-transfer-confirmed/bank-transfer-confirmed.component';
import {AccountManagersComponent} from './admin-user/account-managers/account-managers.component';
import {SuperViewUserDashboardComponent} from './admin-user/super-view-user-dashboard/super-view-user-dashboard.component';
import {DomSanitizerPipe} from './shared/pipes/DomSanitizer/dom-sanitizer.pipe';
import {SelectedBlogComponent} from './landing/selected-blog/selected-blog.component';
import {FarmLProductsComponent} from './landing/farm-products/farm-products.component';
import {FarmLProductDetailsComponent} from './landing/farm-product-details/farm-product-details.component';
import {AllFarmProductsComponent} from './admin-user/product-management/farm-products/farm-products.component';
import {FarmProductStatusComponent} from './admin-user/product-management/status/status.component';
import {FarmProductCategoriesComponent} from './admin-user/product-management/categories/categories.component';
import {AllSponsorsComponent} from './admin-user/all-sponsors/all-sponsors.component';
import {PaystackPaymentsComponent} from './admin-user/paystack-payments/paystack-payments.component';
import {CompletedSponsorsComponent} from "./admin-user/completed-sponsors/completed-sponsors.component";
import {RequestedWithdrawalComponent} from "./admin-user/requested-withdrawal/requested-withdrawal.component";
import {WithdrawalHistoryComponent} from "./admin-user/withdrawal-history/withdrawal-history.component";
import {AllArchiveSponsorsComponent} from "./admin-user/all-archive-sponsors/all-archive-sponsors.component";
import {UpdatesComponent} from "./admin-user/updates/updates.component";
import {FarmUpdatesComponent} from "./users/farm-updates/farm-updates.component";
import {ResendVerifyEmailComponent} from "./landing/resend-verify-email/resend-verify-email.component";
import {DisclaimerComponent} from "./landing/disclaimer/disclaimer.component";
import {ManagePayoutComponent} from "./admin-user/manage-payout/manage-payout.component";

const landingRoutes: Routes = [
  {path: '', component: HomeComponent, pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'about-company', component: AboutComponent},
  {path: 'about-app', component: AboutAppComponent},
  {path: 'about-team', component: TeamComponent},
  {path: 'education', component: EducationComponent},
  {path: 'education/:id/:title', component: SelectedEducationComponent},
  {path: 'faq', component: FaqComponent},
  {path: 'blog', component: BlogComponent},
  {path: 'blog/:id/:title', component: SelectedBlogComponent},
  {path: 'farm-shop', component: FarmShopComponent},
  {path: 'farm-products', component: FarmLProductsComponent},
  {path: 'farm-shop/:shopId/details', component: FarmShopDetailsComponent},
  {path: 'farm-product/:productId/details', component: FarmLProductDetailsComponent},
  {path: 'login', component: LoginComponent},
  {path: 'forgot-password', component: ResetPasswordComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'sign-up', component: RegisterComponent},
  {path: 'resend-signup-verification-link', component: ResendVerifyEmailComponent},
  {path: 'sign-up/:email', component: RegisterComponent},
  {path: 'contact', component: ContactComponent},
  {path: 'terms-of-sponsorship', component: TermsSponsorshipComponent},
  {path: 'terms-of-farm-visit', component: TermsFarmVisitComponent},
  {path: 'terms-of-use', component: TermsUseComponent},
  {path: 'privacy-policy', component: PrivacyPolicyComponent},
  {path: 'disclaimer', component: DisclaimerComponent},
  {path: 'verify-user-email/:userId/:token', component: VerifyUserEmailComponent},
  {path: 'reset-password/token/special/:token/:codeSecret', component: SetPasswordComponent},
  {path: 'user', loadChildren: './shared/modules/user/user.module#UserModule'},
  {path: 'super', loadChildren: './shared/modules/super-user/super-user.module#SuperUserModule'},

  {path: '**', component: HomeComponent},
];

export const landingRouting: IRouting = {
  routes: RouterModule.forRoot(landingRoutes, {useHash: false}),
  components: [
    LoginComponent,
    ResetPasswordComponent,
    RegisterComponent,
    SetPasswordComponent,
    HomeComponent,
    FaqComponent,
    AboutComponent,
    BlogComponent,
    EducationComponent,
    HeaderComponent,
    FooterComponent,
    FarmShopComponent,
    AboutAppComponent,
    TeamComponent,
    ContactComponent,
    PrivacyPolicyComponent,
    DisclaimerComponent,
    TermsFarmVisitComponent,
    TermsSponsorshipComponent,
    TermsUseComponent,
    VerifyUserEmailComponent,
    FarmShopDetailsComponent,
    SelectedEducationComponent,
    DomSanitizerPipe,
    SelectedBlogComponent,
    FarmLProductsComponent,
    FarmLProductDetailsComponent,
    ResendVerifyEmailComponent,
  ],
  entryComponent: [],
  providers: []
};

// , canActivate: [GuardService]
export const userRoutes: Routes = [
  {
    path: '', component: UserRoutesComponent,
    children: [
      {path: 'dashboard', component: UserDashboardComponent, data: {roles: ['user']}, canActivate: [GuardService, RoleService2]},
      {path: 'cart', component: CartsComponent, data: {roles: ['user']}, canActivate: [GuardService, RoleService2]},
      {path: 'cart/:paymentId/:transactionType', component: CartsComponent, data: {roles: ['user']},
        canActivate: [GuardService, RoleService2]},
      {path: 'my-farm', component: MyFarmsComponent, data: {roles: ['user']}, canActivate: [GuardService, RoleService2]},
      {path: 'farm-update', component: FarmUpdateComponent, data: {roles: ['user']}, canActivate: [GuardService, RoleService2]},
      {path: 'transaction-history', component: TransactionComponent, data: {roles: ['user']}, canActivate: [GuardService, RoleService2]},
      {path: 'wallet-history', component: WalletHistoryComponent, data: {roles: ['user']}, canActivate: [GuardService, RoleService2]},
      {path: 'wallet-history/:paymentId', component: WalletHistoryComponent,
        data: {roles: ['user']}, canActivate: [GuardService, RoleService2]},
      {path: 'profile', component: ProfileComponent, data: {roles: ['user']}, canActivate: [GuardService, RoleService2]},
      {path: 'profile/:shipping_address', component: ProfileComponent, data: {roles: ['user']}, canActivate: [GuardService, RoleService2]},
      {path: 'farm-products/orders', component: FarmProductOrdersComponent,
        data: {roles: ['user']}, canActivate: [GuardService, RoleService2]},
      /*{path: 'farm-products', component: FarmProductsComponent, data: {roles: ['user']}, canActivate: [GuardService, RoleService2]},
      {path: 'farm-products/saved-products', component: FarmProductSavedComponent,
        data: {roles: ['user']}, canActivate: [GuardService, RoleService2]},*/
      {path: 'payment/bank-transfer', component: ConfirmBankTransferComponent,
        data: {roles: ['user']}, canActivate: [GuardService, RoleService2]},
      {path: 'payment/pending-transfer', component: PendingTransferComponent,
        data: {roles: ['user']}, canActivate: [GuardService, RoleService2]},
      {path: 'payment/bank-transfer/:transactionRef', component: ConfirmBankTransferComponent,
        data: {roles: ['user']}, canActivate: [GuardService, RoleService2]},
      {path: 'refer-a-friend', component: ReferralComponent,
        data: {roles: ['user']}, canActivate: [GuardService, RoleService2]},
      {path: 'farm-updates-and-notifications', component: FarmUpdatesComponent,
        data: {roles: ['user']}, canActivate: [GuardService, RoleService2]}
    ]
  },
  {path: '**', component: UserRoutesComponent , redirectTo: 'dashboard'}
];


export const userRouting: IRouting = {
  routes: RouterModule.forChild(userRoutes),
  components: [
    UserRoutesComponent,
    UserDashboardComponent,
    CartsComponent,
    MyFarmsComponent,
    FarmUpdateComponent,
    ProfileComponent,
    TransactionComponent,
    FarmProductsComponent,
    FarmProductOrdersComponent,
    ConfirmBankTransferComponent,
    FarmProductSavedComponent,
    WalletHistoryComponent,
    PendingTransferComponent,
    ReferralComponent,
    FarmUpdatesComponent

  ],
  entryComponent: [],
  providers: [RoleService2]
};
export const superUserRoutes: Routes = [
  {path: '', component: SuperUserRoutesComponent, children: [
      {path: 'dashboard', component: AdminDashboardComponent, data: {roles: ['super', 'blogger', 'accountant']}, canActivate: [GuardService, RoleService1]},
      {path: 'view-user/dashboard/:userId', component: SuperViewUserDashboardComponent,
        data: {roles: ['super', 'accountant']}, canActivate: [GuardService, RoleService1]},
      {path: 'farm-shop', component: AdminFarmShopRoutesComponent, children: [
        {path: '', component: ShopsComponent, data: {roles: ['super']}, canActivate: [GuardService, RoleService1]},
        {path: 'status', component: StatusComponent, data: {roles: ['super']}, canActivate: [GuardService, RoleService1]},
        {path: 'categories', component: CategoriesComponent, data: {roles: ['super']}, canActivate: [GuardService, RoleService1]},
        {path: 'shops', component: ShopsComponent, data: {roles: ['super']}, canActivate: [GuardService, RoleService1]}
      ]},
    {path: 'manage-customers', component: CustomersComponent, data: {roles: ['super', 'accountant']}, canActivate: [GuardService, RoleService1]},
    {path: 'manage-updates', component: UpdatesComponent, data: {roles: ['super', 'accountant']}, canActivate: [GuardService, RoleService1]},
    {path: 'report/manage-referrals', component: ManageReferralsComponent,
      data: {roles: ['super', 'accountant']}, canActivate: [GuardService, RoleService1]},
    {path: 'manage-admin-users', component: AdminComponent, data: {roles: ['super']}, canActivate: [GuardService, RoleService1]},
    {path: 'manage-user-groups-and-privileges', component: UserGroupPrivilegeComponent,
      data: {roles: ['super']}, canActivate: [GuardService, RoleService1]},
    {path: 'manage-bank-transfers', component: BankTransferComponent, data: {roles: ['super', 'accountant']}, canActivate: [GuardService, RoleService1]},
    {path: 'manage-paystack-reference', component: PaystackReferenceComponent,
      data: {roles: ['super', 'accountant']}, canActivate: [GuardService, RoleService1]},
    {path: 'manage-farm-products', component: AllFarmProductsComponent,
      data: {roles: ['super']}, canActivate: [GuardService, RoleService1]},
    {path: 'farm-product-category', component: FarmProductCategoriesComponent,
      data: {roles: ['super']}, canActivate: [GuardService, RoleService1]},
    {path: 'farm-product-status', component: FarmProductStatusComponent,
      data: {roles: ['super']}, canActivate: [GuardService, RoleService1]},
    {path: 'manage-farm-products-orders', component: OrdersComponent,
      data: {roles: ['super']}, canActivate: [GuardService, RoleService1]},

    {path: 'report/manage-transactions', component: AllTransactionComponent,
      data: {roles: ['super', 'accountant']}, canActivate: [GuardService, RoleService1]},

    {path: 'report/manage-paystack-transactions', component: PaystackTransactionComponent,
      data: {roles: ['super', 'accountant']}, canActivate: [GuardService, RoleService1]},

    {path: 'report/manage-banktransfer-transactions', component: BankTransactionComponent,
      data: {roles: ['super', 'accountant']}, canActivate: [GuardService, RoleService1]},

    {path: 'manage-confirmed-bank-payment', component: BankTransferConfirmedComponent,
      data: {roles: ['super', 'accountant']}, canActivate: [GuardService, RoleService1]},

    {path: 'account-manager', component: AccountManagersComponent,
      data: {roles: ['super']}, canActivate: [GuardService, RoleService1]},

    {path: 'manage-payout', component: ManagePayoutComponent,
      data: {roles: ['super']}, canActivate: [GuardService, RoleService1]},


    {path: 'report/manage-wallet-transactions', component: WalletTransactionsComponent,
      data: {roles: ['super', 'accountant']}, canActivate: [GuardService, RoleService1]},
    {path: 'report/manage-all-orders', component: AllOrdersComponent, data: {roles: ['super', 'accountant']}, canActivate: [GuardService, RoleService1]},
    {path: 'settings', component: AdminProfileComponent, data: {roles: ['super']}, canActivate: [GuardService, RoleService1]},
    {path: 'manage-profile', component: AdminProfileComponent, data: {roles: ['super']}, canActivate: [GuardService, RoleService1]},
    {path: 'manage-payment-gateways', component: PaymentGatewaysComponent,
      data: {roles: ['super']}, canActivate: [GuardService, RoleService1]},

    {path: 'report/manage-sponsors', component: AllSponsorsComponent,
      data: {roles: ['super', 'accountant']}, canActivate: [GuardService, RoleService1]},

    {path: 'report/manage-archive-sponsors', component: AllArchiveSponsorsComponent,
      data: {roles: ['super', 'accountant']}, canActivate: [GuardService, RoleService1]},

    {path: 'report/manage-paystack-payments', component: PaystackPaymentsComponent,
      data: {roles: ['super', 'accountant']}, canActivate: [GuardService, RoleService1]},

    {path: 'web-content', component: WebContentRoutesComponent, children: [
        {path: '', component: AdminHomeComponent, data: {roles: ['super']}, canActivate: [GuardService, RoleService1]},
        {path: 'home-page', component: AdminHomeComponent, data: {roles: ['super']}, canActivate: [GuardService, RoleService1]},
        {path: 'newsletter-subscribers', component: NewsletterSubscribersComponent, data: {roles: ['super']},
          canActivate: [GuardService, RoleService1]},
        {path: 'about-us-page', component: AboutUsComponent, data: {roles: ['super']}, canActivate: [GuardService, RoleService1]},
        {path: 'manage-team', component: ManageTeamComponent, data: {roles: ['super']}, canActivate: [GuardService, RoleService1]},
        {path: 'education-management', component: AdminEducationComponent, data: {roles: ['super', 'blogger']}, canActivate:
          [GuardService, RoleService1]},
        {path: 'blog-management', component: AdminBlogComponent,
          data: {roles: ['super', 'blogger']}, canActivate: [GuardService, RoleService1]}
      ]},
    {path: 'funding/fund-customer-wallet', component: CompletedSponsorsComponent,
      data: {roles: ['super', 'accountant']}, canActivate: [GuardService, RoleService1]},

    {path: 'funding/requested-withdrawal', component: RequestedWithdrawalComponent,
      data: {roles: ['super', 'accountant']}, canActivate: [GuardService, RoleService1]},
    {path: 'funding/withdrawal-history', component: WithdrawalHistoryComponent,
      data: {roles: ['super', 'accountant']}, canActivate: [GuardService, RoleService1]},

    ]},
  {path: '**', component: SuperUserRoutesComponent , redirectTo: 'dashboard'}
];

export const superUserRouting: IRouting = {
  routes: RouterModule.forChild(superUserRoutes),
  components: [
    SuperUserRoutesComponent,
    SSidebarComponent,
    AdminDashboardComponent,
    CategoriesComponent,
    AdminFarmShopRoutesComponent,
    WebContentRoutesComponent,
    StatusComponent,
    ShopsComponent,
    CustomersComponent,
    AdminComponent,
    UserGroupPrivilegeComponent,
    NewsletterSubscribersComponent,
    BankTransferComponent,
    PaystackReferenceComponent,
    AllFarmProductsComponent,
    OrdersComponent,
    AllTransactionComponent,
    WalletTransactionsComponent,
    AllOrdersComponent,
    SuperViewUserDashboardComponent,
    AdminProfileComponent,
    PaymentGatewaysComponent,
    AdminHomeComponent,
    AboutUsComponent,
    AdminEducationComponent,
    AdminBlogComponent,
    ManageReferralsComponent,
    ManageTeamComponent,

    FarmProductCategoriesComponent,
    FarmProductStatusComponent,
    PaystackTransactionComponent,
    BankTransactionComponent,
    BankTransferConfirmedComponent,
    AccountManagersComponent,

    AllSponsorsComponent,
    PaystackPaymentsComponent,

    CompletedSponsorsComponent,
    RequestedWithdrawalComponent,
    WithdrawalHistoryComponent,
    AllArchiveSponsorsComponent,
    UpdatesComponent,
    ManagePayoutComponent

  ],
  entryComponent: [],
  providers: [SuperAdminService, DeleteService, RoleService1]
};

